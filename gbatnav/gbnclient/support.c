/*	$Id$	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file support.c
 * Funciones de support
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>

#include <gnome.h>

#include "support.h"
#include "protocol.h"
#include "cliente.h"
#include "proceso.h"
#include "g_interface.h"
#include "pantalla.h"
#include "gbnclient.h"
#include "sendmsg.h"
#include "configure.h"
#include "bnwrite.h"
#include "version.h"

/* FONTS */
#define HELVETICA_20_BFONT "-adobe-helvetica-bold-r-normal-*-20-*-*-*-*-*-*-*"
#define HELVETICA_14_BFONT "-adobe-helvetica-bold-r-normal-*-14-*-*-*-*-*-*-*"
#define HELVETICA_12_BFONT "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*"
#define HELVETICA_12_FONT  "-adobe-helvetica-medium-r-normal-*-12-*-*-*-*-*-*-*"
#define HELVETICA_10_FONT  "-adobe-helvetica-medium-r-normal-*-10-*-*-*-*-*-*-*"

#define TEG_DIALOG_X	300
#define TEG_DIALOG_Y	200
#define TEG_DIALOG_Y_NEW 45

void generic_window_set_parent (GtkWidget * dialog, GtkWindow   * parent)
{
	gint x, y, w, h, dialog_x, dialog_y;

	g_return_if_fail(dialog != NULL);
	g_return_if_fail(parent != NULL);
	g_return_if_fail(GTK_IS_WINDOW(parent));

	gtk_window_set_transient_for (GTK_WINDOW(dialog), parent);



	if ( ! GTK_WIDGET_VISIBLE(parent)) return; /* Can't get its
						  size/pos */

	/* Throw out other positioning */
	gtk_window_set_position(GTK_WINDOW(dialog),GTK_WIN_POS_NONE);

	gdk_window_get_origin (GTK_WIDGET(parent)->window, &x, &y);
	gdk_window_get_size   (GTK_WIDGET(parent)->window, &w, &h);

	/*
	 * The problem here is we don't know how big the dialog is.
	 * So "centered" isn't really true. We'll go with 
	 * "kind of more or less on top"
	 */

	dialog_x = x + w/4;
	dialog_y = y + h/4;

	gtk_widget_set_uposition(GTK_WIDGET(dialog), dialog_x, dialog_y); 
}

/**
 * @fn void teg_dialog( char* title, char* bigtitle, char* data )
 * Crea un dialogo un poco mas vistoso, onda gnome_about
 * @param title Titulo de la ventana
 * @param bittitle Titulo del dialogo
 * @param data Informacion del dialogo
 */
void teg_dialog( char* title, char* bigtitle, char* data )
{
	GtkWidget* dialog;
	GtkWidget* canvas;
	GnomeCanvasItem *item;

	dialog = gnome_dialog_new(title, 
			GNOME_STOCK_BUTTON_OK,
			NULL );
	gnome_dialog_set_parent( GNOME_DIALOG(dialog), GTK_WINDOW(window));
	gnome_dialog_set_close( GNOME_DIALOG(dialog), TRUE );
	

	canvas = gnome_canvas_new();
	gtk_widget_set_usize (canvas, TEG_DIALOG_X, TEG_DIALOG_Y );
	gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas), 0, 0, TEG_DIALOG_X, TEG_DIALOG_Y );

	item = gnome_canvas_item_new(
		gnome_canvas_root(GNOME_CANVAS(canvas)),
		gnome_canvas_rect_get_type (),
		"x1", 0.0,
		"y1", 0.0,
		"x2", (double) TEG_DIALOG_X,
		"y2", 45.0,
		"fill_color","black",
		"outline_color","black",
		NULL);

	item = gnome_canvas_item_new(
		gnome_canvas_root(GNOME_CANVAS(canvas)),
		gnome_canvas_rect_get_type (),
		"x1", 0.0,
		"y1", 45.0,
		"x2", (double) TEG_DIALOG_X,
		"y2", (double) TEG_DIALOG_Y,
		"fill_color","light green",
		"outline_color","light green",
		NULL);

	item = gnome_canvas_item_new(
		gnome_canvas_root(GNOME_CANVAS(canvas)),
		gnome_canvas_text_get_type(),
		"text",bigtitle,
		"x", (double) (TEG_DIALOG_X/2),
		"y", (double) 10,
		"x_offset", (double) -1,
		"y_offset", (double) -1,
		"font", HELVETICA_20_BFONT,
		"fill_color", "white",
		"anchor",GTK_ANCHOR_NORTH,
		NULL);

	item = gnome_canvas_item_new(
		gnome_canvas_root(GNOME_CANVAS(canvas)),
		gnome_canvas_text_get_type(),
		"text",data,
		"x", (double) 4,
		"y", (double) 60,
		"x_offset", (double) -1,
		"y_offset", (double) -1,
		"font", HELVETICA_12_FONT,
		"fill_color", "black",
		"anchor",GTK_ANCHOR_NW,
		NULL);

	gtk_box_pack_start_defaults( GTK_BOX(GNOME_DIALOG(dialog)->vbox), GTK_WIDGET(canvas));

	gtk_widget_show (canvas);
	gtk_widget_show_all(dialog);

	return;
}

/**
 * @fn GtkWidget* teg_dialog_new( char* title, char* bigtitle )
 * Crea un gnome_dialog_new un poco diferente
 */
GtkWidget* teg_dialog_new( char* title, char* bigtitle )
{
	GtkWidget* dialog;
	GtkWidget* canvas;
	GnomeCanvasItem *item;

	dialog = gnome_dialog_new(title, 
			NULL );
	gnome_dialog_set_parent( GNOME_DIALOG(dialog), GTK_WINDOW(window));

	canvas = gnome_canvas_new();
	gtk_widget_set_usize (canvas, TEG_DIALOG_X, TEG_DIALOG_Y_NEW );
	gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas), 0, 0, TEG_DIALOG_X, TEG_DIALOG_Y_NEW );

	item = gnome_canvas_item_new(
		gnome_canvas_root(GNOME_CANVAS(canvas)),
		gnome_canvas_rect_get_type (),
		"x1", 0.0,
		"y1", 0.0,
		"x2", (double) TEG_DIALOG_X,
		"y2", 45.0,
		"fill_color","black",
		"outline_color","black",
		NULL);

	item = gnome_canvas_item_new(
		gnome_canvas_root(GNOME_CANVAS(canvas)),
		gnome_canvas_text_get_type(),
		"text",bigtitle,
		"x", (double) (TEG_DIALOG_X/2),
		"y", (double) 10,
		"x_offset", (double) -1,
		"y_offset", (double) -1,
		"font", HELVETICA_20_BFONT,
		"fill_color", "white",
		"anchor",GTK_ANCHOR_NORTH,
		NULL);


	gtk_box_pack_start_defaults( GTK_BOX(GNOME_DIALOG(dialog)->vbox), GTK_WIDGET(canvas));

	gtk_widget_show (canvas);

	return dialog;
}

/* close a window */
void destroy_window( GtkWidget * widget, GtkWidget **window )
{
	if( *window != NULL) {
		gtk_widget_destroy(*window);
	}

	*window=NULL;
}


/* Brings attention to a window by raising it and giving it focus */
void raise_and_focus (GtkWidget *widget)
{
	g_assert (GTK_WIDGET_REALIZED (widget));
	gdk_window_show (widget->window);
	gtk_widget_grab_focus (widget);
}
