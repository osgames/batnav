/*	$Id$	*/
/*
	Includes de g_interface
*/


#ifndef  __BN_G_INTERFACE__
# define __BN_G_INTERFACE__

gboolean g_do_exit(GtkWidget *widget, gpointer data);

gboolean g_box_lost();

gboolean g_box_win();

void about( GtkWidget *widget, gpointer data );

void init_X();
void update_sensi();

# endif /*__BN_G_INTERFACE__*/
