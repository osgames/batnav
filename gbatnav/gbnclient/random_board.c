/*	$Id$	*/
/* 
 * Archivo que contiene los algoritmos para generar
 * una tabla aleatoria valida
 */

/* 'Users use same prandom in localhost' fix by Radovan Garabik <garabik@atlas03.dnp.fmph.uniba.sk> */
 

#include <stdlib.h>
#include <config.h>
#include <gnome.h>

#include "protocol.h"
#include "cliente.h"
#include "random_board.h"
#include "pantalla.h"

void
generar( void )
{
	gint x,y,d,t,i,j;

	if( usuario.play==PLAY || usuario.play==TURN ) { 
		/* permito que un PERDIO modifique su board */
		textfill(0,_("You can't modify your board while playing."));
		return ;

	} else if( usuario.play==BOARD ) {
		textfill(0,_("You've already sent your board to the server. You can't modify it."));
		return ;

	} else {
		usuario.random = gnome_config_get_int_with_default("/gbnclient/data/random=1995",NULL);
		srandom( usuario.random );
		usuario.random = random();		
		gnome_config_set_int   ("/gbnclient/data/random",  usuario.random);
		gnome_config_sync();

		for(i=0;i<10;i++) {
			/* clean tabla */
			for(j=0;j<10;j++)
				usuario.mitabla[i][j]=NOBARCO;
		}

		for(i=1;i<=10;i++) {	
			x = 1+(int) (10.0*rand()/(RAND_MAX+1.0));
			y = 1+(int) (10.0*rand()/(RAND_MAX+1.0));
			d = (x + y) & 1;	/* Direccion */
			t = tamano( i );	/* Tamano / size */
	
			buscar_poscicion(x,y,d,t);
		}

		for(i=0;i<10;i++) {
			for(j=0;j<10;j++)
				pmicell(i,j,usuario.mitabla[i][j]);
		}
		draw_grid_left();
	}
}

gint
posible( gint x, gint y, gint d, gint t)
{
	gint i;

	/* Por tamano, entra ? */	
	/* by size it enteres (?) */
	if( !entra( x,y,d,t) )
		return FALSE;
	
	/* Por colision, vale ? */
	/* By collision, it is worth (?) */
	if(d==0) {
		/* Horizontal */
		for( i=0; i<=t+1; i++ ) {
			if(!( vacio(x-1+i,y) && vacio( x-1+i,y-1) && vacio( x-1+i,y+1))	)
				return FALSE;
		}
		return TRUE;
	}

	else{
		/* Vertical */ 
		for( i=0; i<=t+1; i++ ) {
			if(! (vacio( x, y-1+i ) && vacio( x-1, y-1+i ) && vacio( x+1, y-1+i ))	)
				return FALSE;
		}
		return TRUE;
	}
}

/*
 * Algoritmo recursivo
 * to put boat
 */
void 
buscar_poscicion( gint x, gint y, gint d, gint t)
{
	/* Pregunta si es posible en la otra direccion */
	/* Question if it is possible in the other direction */
	if( posible( x,y, ((d^1) & 1),t )) {
		poner_barco( x,y,  ((d^1) &1), t);
		return;
	}

	/* Incrementa Y en uno y asi sucesivamente */
	/* Digamos que recorre todo - Nada de Backtracking o algo mejor */	
	/* It increases and in one and asi successively
	 * We say that Nothing of Backtracking crosses everything 
	 * - or something better
	 */
	y++;
	if(y>10) {
		y=1;
		x++;
		if(x>10)
			x=1;
	}

	if( posible( x,y,d,t) ) {
		/* to put boat */
		poner_barco( x,y,d,t);
		return ;
	}
	else
		/* lookup position */
		return buscar_poscicion( x,y,d,t);
}

/* size (of boat) */
gint
tamano( gint i )
{
	gint a;

	switch(i) {	
	case 1:
	case 3:
	case 7:
	case 8:
		a = 1;
		break;
	case 2:
	case 9:
	case 5:
		a = 2;
		break;
	case 4:
	case 10:
		a = 3;
		break;
	case 6:
		a = 4;
		break;
	}
	return a;
}

/* Pregunta si un barco entra por tamano */
/* Question if a boat enters by */
gint
entra( gint x, gint y, gint d, gint t)
{
	if(d==0) {
		/* Horizontal */
		if(x+t-1<=10)
			return TRUE;
		else
			return FALSE;
	}
	
	else {
		/* Vertical */
		if(y+t-1<=10)
			return TRUE;
		else
			return FALSE;
	}
}

/* empty */
gint
vacio( gint x, gint y)
{
	if(x<1 || x>10 || y<1 || y>10)
		return TRUE;

	if( usuario.mitabla[x-1][y-1]==NOBARCO || usuario.mitabla[x-1][y-1]==AGUA )
		return TRUE;
	else
		return FALSE;
}

/* to put boat */
void
poner_barco( gint x, gint y, gint d, gint t)
{
	gint i;

	if(d==0) {
		/* Horizontal */
		for(i=1;i<=t;i++)
			usuario.mitabla[x-2+i][y-1]=BARCO;
	}
	else {
		/* Vertical */
		for(i=1;i<=t;i++)
			usuario.mitabla[x-1][y-2+i]=BARCO;
	}
}

