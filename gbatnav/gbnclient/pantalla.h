/*	$Id$	*/

/* 
	Includes de pantalla
	Funcion que chequea el port y esas cosas
	Function that checks port and those things (?)
	Functions of screen handling and others


*/


#ifndef  __BN_PANTALLA__
# define __BN_PANTALLA__

# ifdef __cplusplus
extern "C" {
# endif /*__cplusplus*/



void 
textfill(gint, gchar *, ... );

/* Draw in my (left) screen */
void 
pmicell(int x,int y,int color);

/* Draw in enemy (right) screen */
void 
ptucell(int x,int y,int color);

void 
foot_right( char* text );

void 
foot_left( char* text );


   
# ifdef __cplusplus
}
# endif /*__cplusplus*/

# endif /*__BN_PANTALLA__*/
