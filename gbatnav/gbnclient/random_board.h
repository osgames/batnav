/*	$Id$	*/
#ifndef __BN_RANDOM__
# define __BN_RANDOM__

# ifdef __cplusplus
extern "C" {
# endif /*__cplusplus*/

void
generar( void );

gint
posible( gint x, gint y, gint d, gint t);

/* lookup position */
void
buscar_poscicion( gint x, gint y, gint d, gint t);

/* size (of boat) */
gint
tamano( gint i );

/* to put boat */
void
poner_barco( gint x, gint y, gint d, gint t);

/* it enters */
gint
entra( gint x, gint y, gint d, gint t);

/* empty */
gint
vacio( gint x, gint y);

# ifdef __cplusplus
}
# endif /*__cplusplus*/


#endif /*__BN_RANDOM__*/
