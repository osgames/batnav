/*	$Id$	*/

#ifndef __BATNAV_VERSION_H
#define __BATNAV_VERSION_H

#include <gnome.h>
#include <config.h>

#define BNVERSION	"Batalla Naval Client v"VERSION

#ifdef INET6
#define BATVER		"Gnome Client v"VERSION"+IPv6"
#else
#define BATVER		"Gnome Client v"VERSION
#endif

#ifdef INET6
#define IPVERSION	VERSION"+IPv6"
#else
#define IPVERSION	VERSION
#endif

#endif /* __BATNAV_VERSION_H */
