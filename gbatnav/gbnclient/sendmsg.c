/*	$Id$	*/
#include <gnome.h>

#include "cliente.h"
#include "pantalla.h"
#include "gbnclient.h"
#include "bnwrite.h"

void inputline_return(GtkWidget *w, gpointer data)
{
	char *theinput;

	if( usuario.play==DISCON ) {
		gtk_entry_set_text(GTK_ENTRY(w), "");
		textfill(0,_("You need to be connected"));
		return;
	}

	theinput = gtk_entry_get_text(GTK_ENTRY(w));

	if(*theinput) {
		if(theinput[0]=='/') {
			net_printf( usuario.sock, "%s\n",&theinput[1]);
		} else  {
			bnwrite(usuario.sock,BN_MESSAGE"=%s",theinput);
		}
	}
	gtk_entry_set_text(GTK_ENTRY(w), "");
}
