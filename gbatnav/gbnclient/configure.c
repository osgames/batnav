/*	$Id$	*/

#include <gnome.h> 
#include "cliente.h"
#include "pantalla.h"
#include "gbnclient.h"
#include "support.h"

void conf_view_messages( void )
{
	usuario.debug_level = !usuario.debug_level;
}

void conf_view_grid( void )
{
	int i;
	GtkWidget *widget;
	usuario.view_grid = !usuario.view_grid;

	/* left */
	if( drawing_left ) {
		gtk_widget_draw( drawing_left, NULL );
		expose_event( drawing_left, NULL );
	}

	/* right */
	i = gtk_notebook_current_page( GTK_NOTEBOOK( notebook_right ) );
	if( i >= 0 && i < MAXPLAYER ) {
		i = usuario.pages[ i ]; 
		widget = drawing_right[ i ];

		if( usuario.usrfrom >=0 && usuario.usrfrom < MAXPLAYER && drawing_right[i] ) {
			gtk_widget_draw( drawing_right[usuario.usrfrom], NULL );
			expose_event_right( drawing_right[usuario.usrfrom], NULL );
		}
	}
}
