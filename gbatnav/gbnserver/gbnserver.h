/*	$Id$	*/

#ifndef __BN_GBNSERVER_H__
#define __BN_GBNSERVER_H__ 

int eshundido( int, int, int ); /* it is sunk */
void ctable( int ); /* clean 10x10 board */
int wtable( int, char *); /* write to 10x10 board from 100 string */
void borrar_jugador( int ); /* to erase player */
void salir_bien( void ); /* to come out well */
void broadcast( char *fmt, ...);
int accept_new_player( int fd );

#endif /* __BN_GBNSERVER_H__ */
