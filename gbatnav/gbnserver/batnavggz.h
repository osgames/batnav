/*	$Id$	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file tegggz.h
 * Funcion que maneja un poco el soporte para GGZ
 */

#ifndef __BATNAVS_TEGGGZ_H
#define __BATNAVS_TEGGGZ_H

#include <config.h>

#ifdef WITH_GGZ

#include "common.h"

int batnavggz_main_loop();
BATNAV_STATUS batnavggz_find_ggzname( int fd, char *n, int len );
int batnavggz_del_fd( int fd );

#endif /* WITH_GGZ */

#endif /* __BATNAVS_TEGGGZ_H */
