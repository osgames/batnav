/*	$Id$	*/
#ifndef __BN_PLAY_H__
# define __BN_PLAY_H__

void play_batnav( gpointer , gint , GdkInputCondition );
gint quejugador( gint ); /* that player */
void token_exit( gint );
void token_start( gint );

#endif /* __BN_PLAY__ */
