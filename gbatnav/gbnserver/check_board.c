/*	$Id$	*/
/* 
 *
 * Algoritmo que chequea la validez de una tabla 
 * un poco mas descente que el anterior :)
 *
 * Algorithm that checks the validity of a table 
 * a little but descente that the previous one
 *
 */

/* 
 * Points to next pos.return TRUE . FALSE in case there are no more ships
 * this rutine uses global x,y 
 */

/* exported: algoritmo() */

#include <stdlib.h>
#include <gnome.h>
#include <config.h>
#include "check_board.h"
#include "protocol.h"
#include "server.h"

struct st_datos usuario;

/* empty (?) */
static gint
vacio( gint x, gint y, gint jugador )
{
	if(x>=10 || x<0 || y>=10 || y<0)
		return TRUE;
	if( usuario.table[jugador].p[x][y]==NOBARCO) 
		return TRUE;
	else
		return FALSE;
}

/* following_pos */
static void
siguiente_pos( gint *x, gint *y, gint jugador )
{
	if( vacio( *x, *y, jugador ) )
		(*x)++;
	else {
		while( (*x)<10  && !vacio(*x,*y,jugador) )
			(*x)++;
	}

	if( (*x) >=10 ) {
		(*x)=0;
		(*y)++;
	}
}

/* such boat (?) */
static gint
tamano_barco( gint x, gint y, gint jugador)
{
	gint b;

	b=0;

	if(!vacio(x+1,y,jugador)) { /* Barco horizontal */
		while(!vacio(x,y,jugador)) {
			b++;
			x++;
		}
		return b;
	}

	if(!vacio(x,y+1,jugador)) { /* Barco Vertical */
		while(!vacio(x,y,jugador)) {
			b++;
			y++;
		}
		return b;
	}
	return 1;	/* Barco de una unidad */
}

static gint
valid_pos( gint x, gint y, gint jugador )
{
	if( (!vacio(x,y,jugador)) && (!vacio(x+1,y+1,jugador)) )
		return FALSE;
	
	if( (!vacio(x+1,y,jugador)) && (!vacio(x,y+1,jugador)) )
		return FALSE;

	return TRUE;
}	

/* ALGORITMO_REC */
static gint
algoritmo_rec( gint *x, gint *y, int jugador, char *barcos)
{
	if(*y >= 10)
		return TRUE;
	if( (!valid_pos( *x, *y, jugador ) ) )
		return FALSE;
	if( (!vacio( *x, *y, jugador ) )  && (vacio( *x, (*y)-1, jugador)) )
		barcos[ tamano_barco(*x, *y, jugador)]++;

	siguiente_pos( x, y, jugador );
	
	return algoritmo_rec( x, y, jugador, barcos );
}


/* return TRUE if table is OK .else return FALSE  */
gint
algoritmo(gint num_jug)  
{
	gint x,y,i;

	char barcos[11];
	
	for(i=0;i<11;i++)
		barcos[i]=0;

	x=0;y=0;


	
	if(!(algoritmo_rec(&x,&y,num_jug,barcos) ))
		return FALSE;			// Por Colision

	if(	(barcos[1]==4) &&
		(barcos[2]==3) &&
		(barcos[3]==2) &&
		(barcos[4]==1) &&
		(barcos[5]==0) &&
		(barcos[6]==0) &&
		(barcos[7]==0) &&
		(barcos[8]==0) &&
		(barcos[9]==0) &&
		(barcos[10]==0)
	)
		return TRUE;
	else
		return FALSE;
}
