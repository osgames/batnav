/*	$Id$	*/
/*
 * Estructura del parser
 */

#ifndef __PARSER_H__
#define __PARSER_H__

#define PARSER_TOKEN_MAX 500
#define PARSER_VALUE_MAX 500
#define PARSER_SIG_MAX 5000
typedef struct {
	char a;
	char b;
	char c;
} DELIM, *DELIM_PTR;

typedef struct {
	char token[PARSER_TOKEN_MAX];
	char value[PARSER_TOKEN_MAX];
	char sig[PARSER_TOKEN_MAX]; 
	int status;
	DELIM_PTR igualador;
	DELIM_PTR separador;
} PARSER, *PARSER_PTR;

enum {
	PARSER_FIN,
	PARSER_SEPARADOR,
	PARSER_IGUAL,
	PARSER_DATA,
	PARSER_ERROR
};

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

/* Unica funcion publica del parser */
int parser_init( PARSER_PTR );

#endif /* __PARSER_H__ */
