/*	$Id$	*/
/*
 * Funcion bnwrite
 */

#ifndef __BNWRITE_H__
#define __BNWRITE_H__

void broadcast( char *fmt, ...);
int bnwrite(int, gchar *fmt, ...);

#endif /* __BNWRITE_H__ */
