/*	$Id$	*/
/************************************************************************
	Archivo que pertenece al batalla naval 
	Definicion del protocolo
*************************************************************************/

#ifndef __BN_PROTOCOLO_H__
#define __BN_PROTOCOLO_H__

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/


#define PROT_VER "2"
#define PROT_PLAY_VER "2"

#define PROT_MAX_LEN 5000
#define MAXNAMELEN 18

/* MAXPLAYERS - Un valor entre 2 y un numero muy grande :) */
#define MAXPLAYER 10
#define MAXSERVERNAME 50

/* NO USAR 'enum' */
#define	DISCON	0 		/* not connected */
#define	CONNEC	1		/* connected */
#define	BOARD	2		/* Board OK */
#define	PLAY	3		/* playing */
#define	TURN	4		/* playing + turn */
#define	PERDIO	5 		/* perdio (sirve para que clientes como
				el btwin sigan monitoreando el juego
				luego de perder */
				/* perdio (serves so that clients as btwin 
				 * the game after losing follows monitor eando
				 */

#define AGUA	'0'		/* water */
#define NOBARCO	'1'		/* no boat */
#define BARCO	'x'		/* boat */
#define TOCADO	'y'		/* it touches do */
#define HUNDIDO	'z'		/* sunk */

#define BN_BOARD_NOT_OK		"board_not_ok"
#define	BN_BOARD_OK		"board_ok"
#define	BN_CANT_MODIFY_BOARD	"cant_modify_board"
#define BN_CLI_VER		"client_version"
#define BN_DISCON		"disconnect"
#define BN_EXIT			"exit"
#define BN_FIRE			"fire"
#define BN_GAME_OVER		"game_over"
#define BN_HELP			"help"
#define BN_LOST			"loser"
#define BN_MESSAGE		"message"
#define BN_NAME			"name"
#define BN_NUMJUG		"number_player"
#define BN_PROTOCOL		"protocol"
#define BN_QUMM			"quiero_un_mundo_mejor"
#define BN_READ			"read"
#define BN_READY_TO_PLAY	"ready_to_play"
#define BN_REM			"*"
				/* I want to play with a robot */
#define BN_ROBOT		"quiero_jugar_con_un_robot"
#define BN_SCORES		"scores"
				/* sending my boats */
#define BN_SEND			"enviando_mis_barcos"
				/* the servant this plenty */
#define BN_SER_FULL		"el_servidor_esta_lleno"
#define BN_SER_VER		"server_version"
				/* single to ningun side */
#define BN_SOL			"solo_a_ningun_lado"
				/* to begin to play */
#define BN_START		"empezar_a_jugar"
#define BN_STATUS		"status"
#define BN_TEST			"eva_test"
				/* it is the turn of */
#define	BN_TURN			"es_el_turno_de"
				/* it hopes that there is people playing */
#define	BN_WAIT			"espera_que_hay_gente_jugando"
				/* the winner is */
#define	BN_WIN			"el_ganador_es"
#define BN_GGZ			"avoiding_perdig_effect"


# ifdef __cplusplus
}
# endif /*__cplusplus*/

#endif /* __BN_PROTOCOLO__ */
