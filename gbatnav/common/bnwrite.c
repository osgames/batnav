/*	$Id$	*/
/*
 * Funcion bnwrite
 */

#include <stdarg.h>
#include <stdio.h>

#include "protocol.h"
#include "net.h"

int
bnwrite(int fd, char *fmt, ...)
{
	int a;
	char t[1000];
	va_list args;
	va_start( args, fmt );

	if(fd==-1) return -1;

	vsnprintf(t,sizeof(t)-1,fmt,args);
	va_end(args);
	
	a = net_printf(fd,"%s\n",t);
	if(a<1) {
		fprintf(stderr,"printing (%d): %s\n",fd,t);
		perror("bnwrite error:\n");
	}
	return a;
}
