/*
 * Batalla Naval (c) 1995,96,97,98 Ricardo Quesada
 * (rquesada@dc.uba.ar)
 * 
 * Cliente X - GTK
 * Version: pre-0.60-31
 *  
 * Depende de las librerias GTK
 *
 * - Obviamente el codigo es casi el mismo que del cliente Xview, Okay ?
 *   aunque espero que sea un cacho mas estable
 * - Perdon, creo que vario muchisimo del cliente XView, a si que no es casi
 *   igual, Okay ?
 * - Bueno, esta bien.
 *
 */

/*
 * pre-0.60-1/3   xx/09/97 - Jugar con las GTK
 * pre-0.60-4     xx/09/97 - Crear pantalla
 * ... despues de los examenes ...
 * pre-0.60-5     27/12/97 - Usar pixmaps
 * pre-0.60-6     28/12/97 - Uh?
 * ... despues de las vacaciones ...
 * pre-0.60-7     13/02/98 - Un buen rato despues... connect y etc.
 * pre-0.60-8     14/02/98 - Connect y Disconnect y Status
 * pre-0.60-9     14/02/98 - Send Board y Start y vscrollbar
 * pre-0.60-10    16/02/98 - Right panel, hit and otras boludeces.
 * pre-0.60-11    19/02/98 - Mas pixmaps y otras bellas cosas (algo jugable).
 * pre-0.60-12    21/02/98 - Probe scrolled_window... pero no. Saque table
 * pre-0.60-13    23/02/98 - Parti en 2 el codigo...
 * pre-0.60-14    01/03/98 - Hacer algo mas, no ?
 * pre-0.60-15    01/03/98 - Funciona el juego joya. Espero
 * pre-0.60-16    03/03/98 - Uni de nuevo el codigo en un solo archivo.
 *                           Pude arreglar el problema de las notebooks
 *                           con las nuevas GTK 0.99.4 - Anda vieja.
 * pre-0.60-17    04/03/98 - Anda la opcion de config
 * pre-0.60-18    04/03/98 - Mensajes y esas porquerias
 * pre-0.60-19    04/03/98 - Porquerias con las notebooks
 * pre-0.60-20    05/03/98 - Tratar de arreglar las putas notebooks
 * pre-0.60-21    07/03/98 - Encontre el puto bug que no me dejaba dormir
 *                           buscar en el codigo por "puto bug"
 * pre-0.60-22    07/03/98 - ?
 * pre-0.60-23    07/03/98 - Jugable...
 * pre-0.60-24    08/03/98 - Pixmaps inside
 * pre-0.60-25    08/03/98 - Un nuevo help
 * pre-0.60-26    08/03/98 - Uso los pixmaps the GameOver y Winner
 * pre-0.60-27    08/03/98 - Tiene icono
 * pre-0.60-28    09/03/98 - No tiene mas menu y actualiza el vscrollbar
 * pre-0.60-29    10/03/98 - Algunos detalles y check button en bnsendmsg
 * pre-0.60-30    17/03/98 - Agregar el popup a las notebooks, y los tooltips
 * pre-0.60-31    18/03/98 - Otro status bar abajo ( left y right )
 */

/* ---- Includes Generales ---- */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <stdio.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>


/* ---- Includes propios de la Interface ---- */
#include <gtk/gtk.h>


/* ---- Includes propios del Juego ---- */
#include "../share/protocol.h"
#include "cliente.h"
#include "pixmaps.h"


/*
 * Funciones de manejo de pantalla y otras
 */
static void ttyfill(char msg[])
{
   gchar messg[100];
   gfloat new_value ;
   
   strncpy(messg,msg,100);
   
   gtk_text_freeze(GTK_TEXT(text));
   gtk_widget_realize(text);
   gtk_text_insert( GTK_TEXT(text),NULL,NULL,NULL,messg,-1);
   gtk_text_thaw(GTK_TEXT(text));
   
   /* tendria que encontrar la manera de que sea automatico el valor este */
   if( GTK_RANGE( vscrollbar )->adjustment->upper >= 154 )
     {
	new_value = GTK_RANGE( vscrollbar )-> adjustment->upper - 154;
	GTK_RANGE(vscrollbar)->adjustment->value = new_value;
	GTK_RANGE(vscrollbar)->timer = 0; /* para que sirver ? */
	gtk_signal_emit_by_name (GTK_OBJECT (GTK_RANGE(vscrollbar)->adjustment), "value_changed");
     }
}


static void pmicell(int x,int y,int color)
{
   if(x<10 && x>=0 && y>=0 && y<10)
     {
	switch( color )
	  {
	   case NOBARCO:
	     gdk_draw_pixmap( drawing_left->window,
			     drawing_left->style->fg_gc[GTK_WIDGET_STATE(drawing_left)],
			     fondo,
			     x*ANCHO+1,y*LARGO+1,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	   case AGUA:
	     gdk_draw_pixmap( drawing_left->window,
			     drawing_left->style->fg_gc[GTK_WIDGET_STATE(drawing_left)],
			     azul,
			     0,0,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	   case BARCO:
	     gdk_draw_pixmap( drawing_left->window,
			     drawing_left->style->fg_gc[GTK_WIDGET_STATE(drawing_left)],
			     verde,
			     0,0,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	   case TOCADO:
	     gdk_draw_pixmap( drawing_left->window,
			     drawing_left->style->fg_gc[GTK_WIDGET_STATE(drawing_left)],
			     rojo,
			     0,0,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	   case HUNDIDO:
	     gdk_draw_pixmap( drawing_left->window,
			     drawing_left->style->fg_gc[GTK_WIDGET_STATE(drawing_left)],
			     negro,
			     0,0,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	   default:
	     gdk_draw_pixmap( drawing_left->window,
			     drawing_left->style->fg_gc[GTK_WIDGET_STATE(drawing_left)],
			     negro,
			     0,0,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	  }
     }
}

static void ptucell(int x,int y,int color)
{
   gint i;
   
   if(x<10 && x>=0 && y>=0 && y<10)
     {
	i = gtk_notebook_current_page( GTK_NOTEBOOK( notebook_right ) );
	i = usuario.pages[ i ] ;  /* puto bug resuelto */
	switch( color )
	  {
	   case NOBARCO:
	     gdk_draw_pixmap( drawing_right[i]->window,
			     drawing_right[i]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[i])],
			     fondo,
			     x*ANCHO+1,y*LARGO+1,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	   case AGUA:
	     gdk_draw_pixmap( drawing_right[i]->window,
			     drawing_right[i]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[i])],
			     azul,
			     0,0,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	   case BARCO:
	     gdk_draw_pixmap( drawing_right[i]->window,
			     drawing_right[i]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[i])],
			     verde,
			     0,0,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	   case TOCADO:
	     gdk_draw_pixmap( drawing_right[i]->window,
			     drawing_right[i]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[i])],
			     rojo,
			     0,0,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	   case HUNDIDO:
	     gdk_draw_pixmap( drawing_right[i]->window,
			     drawing_right[i]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[i])],
			     negro,
			     0,0,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	   default:
	     gdk_draw_pixmap( drawing_right[i]->window,
			     drawing_right[i]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[i])],
			     negro,
			     0,0,
			     x*ANCHO+1,y*LARGO+1,
			     ANCHO-1,LARGO-1);
	     break;
	     
	  }
     }
}

static void foot_right( char* text )
{
   gtk_statusbar_pop(  GTK_STATUSBAR(statusbar_right), 1 );
   gtk_statusbar_push( GTK_STATUSBAR(statusbar_right), 1, text);
}
static void foot_left( char* text )
{
   gtk_statusbar_pop(  GTK_STATUSBAR(statusbar_left), 1 );
   gtk_statusbar_push( GTK_STATUSBAR(statusbar_left), 1, text);
}

/* Funcion que saca a un usuario */
gint sacar_usrpage( gint jugador )
{
   gint i,j,k;
   
   j=32; /* no es nada esto, pero sirve para el debug */
   
   for(i=0;i<MAXPLAYER;i++)
     {
	if (usuario.pages[i]==jugador)
	  {
	     j=i;
	     for( k=i; k < (MAXPLAYER-1); k++ )
	       {
		  usuario.pages[k]=usuario.pages[k+1];
	       }
	     usuario.pages[MAXPLAYER-1]=-32;
	  }
     }
   return j;
}

/* Funcion que busca un usuario en una page */
gint buscar_usr( gint usr )
{
   gint i,j;

   j=-20;
   for(i=0;i<MAXPLAYER;i++)
     {
	if(usuario.pages[i]==usr)
	  {
	     j=i;
	     break;
	  }
     }
   return j;
}

/* Funcion que pone el Help */

static void h2o(char messg[])
{
   gtk_text_freeze(GTK_TEXT(text_help));
   gtk_widget_realize(text_help);
   gtk_text_insert( GTK_TEXT(text_help),NULL,NULL,NULL,messg,-1);
   gtk_text_thaw(GTK_TEXT(text_help));
}

void bn_help( void )
{
   h2o("BATALLA NAVAL -\n"
       "\tHIDE,HELP & CREDITS"
       "\n\n\n" 
       "HIDE\n"
       "Use this option to hide your ships from your enemies, "
       "in case you are playing in the same room with them, of course."
       "\n\n\n"
       "HELP\n"
       "First you must fill your board. Press 'My board' in this "
       "notebook, and complete it with:\n"
       " - 4 ships of 1 unit\n"
       " - 3 ships of 2 units\n"
       " - 2 ships of 3 units\n"
       " - 1 ship of 4 units\n"
       "\n"
       "Remember that the ships must be put horizontally or vertically "
       "only, and that they must be one cell away from the others ships.\n"
       "Then 'Connect' to the server, if there is an error 'Config' your client.\n"
       "Then 'Send-your-Board' to the server and wait others players. "
       "You can view the 'Status' of the server, "
       "and when there are enough players 'Start' the game. OK?"
       "\n\n\n"
       "CREDITS\n"
       "Un-original idea by:\n"
       "Sebastian Cativa Tolosa & Ricardo Quesada\n\n"
       "Server, GTK Client, XView Client & ncurses Client by:\n"
       "Ricardo Quesada\n\n"
       "Win 16 client & bugfixes by:\n"
       "Horacio Pe�a\n\n"
       "\n\n\n"
       "e-mail's:\n"
       "Sebastian Cativa Tolosa\n(scativa@dc.uba.ar)\n\n"
       "Horacio Pe�a\n(horape@century.com.ar)\n\n"
       "Ricardo Quesada\n(rquesada@dc.uba.ar)\n\n"
       );
   
}
/****************************************************************************
 *                     FUNCIONES DE LOS EVENTOS DEL MOUSE
 ****************************************************************************/

/* Left */
static gint expose_event (GtkWidget *widget, GdkEventExpose *event)
{
   gint i,j;
   
   for(i=0;i<10;i++)
     {
	for(j=0;j<10;j++)
	  {
	     pmicell( i,j, mitabla[i][j] );
	  }
     }
   
   /* Dibuja las lineas */
   for(i=0;i<10;i++)
     {
	gdk_draw_line (widget->window,
		       widget->style->black_gc,
		       0,LARGO*i,
		       ANCHO*10,LARGO*i);
	gdk_draw_line (widget->window,
		       widget->style->black_gc,
		       ANCHO*i,0,
		       ANCHO*i,LARGO*10);
     }
   
   return FALSE;
}


// Se�al PRESS_BUTTON
static gint button_press_event (GtkWidget *widget, GdkEventButton *event)
{
   gint x,y;
   
   x=event->x / ANCHO;
   y=event->y / LARGO;
   
   
   if( usuario.play >=3 )
     {
	ttyfill("Hey, What are you trying to do ?\n");
	return TRUE;
     }
   else if( usuario.play ==2 )
     {
	ttyfill("I'm sorry but you can't modify your board right now.\n");
	return TRUE;
     }
   
   if ( event->button == 1 )
     {
	gdk_draw_pixmap( widget->window,
			widget->style->fg_gc[GTK_WIDGET_STATE(widget)],
			verde,
			0,0,
			x*ANCHO+1,y*LARGO+1,
			ANCHO-1,LARGO-1);
	mitabla[x][y]=1;
     }
   else if ( event->button == 3 )
     {
	gdk_draw_pixmap( widget->window,
			widget->style->fg_gc[GTK_WIDGET_STATE(widget)],
			fondo,
			x*ANCHO+1,y*LARGO+1,
			x*ANCHO+1,y*LARGO+1,
			ANCHO-1,LARGO-1);				  
	mitabla[x][y]=0;
     }		
   return TRUE;
}

/* Right About */
static gint expose_event_about (GtkWidget *widget, GdkEventExpose *event)
{

   gdk_draw_pixmap( widget->window,
		   widget->style->fg_gc[GTK_WIDGET_STATE(widget)],
		   about,
		   0,0,
		   0,0,
		   200,200
		   );
   return FALSE;
}

/* Right, page switch */
static gint page_switch (GtkWidget *widget, GtkNotebookPage *page, gint page_num)
{
   gchar outbuf[MSGMAXLEN];
   
   if( usuario.play>=3 )
     {
	usuario.usrfrom = usuario.pages[ page_num ] + 1;
	bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
     }
   return TRUE;
}

/* Right panel */
static gint expose_event_right (GtkWidget *widget, GdkEventExpose *event)
{
   gint i;
   
   /* Dibuja las lineas */
   switch( usuario.gano )
     {
      case 0: /* Sigue jugando */
	for(i=0;i<10;i++)
	  {
	     gdk_draw_line (widget->window,
			    widget->style->black_gc,
			    0,LARGO*i,
			    ANCHO*10,LARGO*i);
	     gdk_draw_line (widget->window,
			    widget->style->black_gc,
			    ANCHO*i,0,
			    ANCHO*i,LARGO*10);
	  }
	
	fillboard(usuario.tempclit,1); /* v0.50 */
	inteliclient(usuario.tempclit);
	break;
      case 1: /* gano el guacho */
	showboard(winner);
	break;
      case -1: /* perdio el forro */
	showboard(gameover);
	break;
      default:
	break;
     }
   return FALSE;
}

static gint button_press_event_right (GtkWidget *widget, GdkEventButton *event)
{
   gint x,y;
   gchar outbuf[MSGMAXLEN];
   
   x=event->x / ANCHO;
   y=event->y / LARGO;
   
   if(usuario.play<3)
     {
	ttyfill("Uh? Wait man. First, try to start the game\n");
	return TRUE;
     }
   if(usuario.play==3)
     {
	ttyfill("Be patient bro. Wait for your turn\n");
	return TRUE;
     }
   
   if ( event->button == 1 )
     {
	gdk_draw_pixmap( widget->window,
			widget->style->fg_gc[GTK_WIDGET_STATE(widget)],
			azul,
			0,0,
			x*ANCHO+1,y*LARGO+1,
			ANCHO-1,LARGO-1);
	
	usuario.play=3;
	outbuf[0]=x;
	outbuf[1]=y;
	bnwrite(sock,outbuf,BNHIT,0,0,usuario.numjug);
	bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
	
     }
   return TRUE;
}

static void remove_page( gint page_num )
{
//   printf("Removing page number %i\n",page_num);
   gtk_notebook_remove_page( GTK_NOTEBOOK( notebook_right ), page_num );
}


/* Dibuja la pantalla */
static void init_X()
{
   GtkTooltips *tooltips; 
   GdkColor yellow={ 4, 8, 8, 0 };
   GdkColor black={ 4, 0, 0, 0 };

   window = gtk_widget_new (gtk_window_get_type (),
			    "GtkObject::user_data", NULL,
			    "GtkWindow::type", GTK_WINDOW_TOPLEVEL,
			    "GtkWindow::title", "Batalla Naval "GBNVER,
			    "GtkWindow::allow_grow",FALSE,
			    "GtkWindow::allow_shrink",FALSE,
			    "GtkContainer::border_width",0,
			    NULL);
   gtk_signal_connect ( GTK_OBJECT( window), "destroy",
		       GTK_SIGNAL_FUNC( do_exit ),
		       GTK_OBJECT(window) );
   gtk_signal_connect ( GTK_OBJECT( window), "delete_event",
		       GTK_SIGNAL_FUNC( do_exit ),
		       GTK_OBJECT(window) );
   
   tooltips=gtk_tooltips_new();
   gtk_object_set_data (GTK_OBJECT (window), "tooltips", tooltips);
   gtk_tooltips_set_colors( tooltips, &black, &yellow);

   /* Box que contiene a box_left, box_button, box_right  */
   vbox = gtk_vbox_new ( FALSE, 0);
   gtk_container_add ( GTK_CONTAINER(window), vbox );
   gtk_container_border_width ( GTK_CONTAINER(vbox), 0);
   gtk_widget_show ( vbox ); 

   /* box horizontal */
   hbox = gtk_hbox_new ( FALSE, 0);
   gtk_container_add ( GTK_CONTAINER(vbox), hbox );
   gtk_container_border_width ( GTK_CONTAINER(hbox), 0);
   gtk_widget_show( hbox );
   
   /******************** LEFT *******************/
   notebook_left = gtk_notebook_new();
   gtk_notebook_set_tab_pos( GTK_NOTEBOOK(notebook_left),GTK_POS_TOP );
   gtk_box_pack_start( GTK_BOX(hbox),notebook_left,TRUE,TRUE,0);
   gtk_widget_show (notebook_left);
   
   // respecto al drawing_left
   drawing_left = gtk_drawing_area_new();
   gtk_drawing_area_size(GTK_DRAWING_AREA(drawing_left),200,200);
   gtk_widget_show( drawing_left );
   

   // se�ales de la drawing area
   gtk_signal_connect (GTK_OBJECT (drawing_left), "expose_event",
		       (GtkSignalFunc) expose_event, NULL);
   gtk_signal_connect (GTK_OBJECT (drawing_left), "button_press_event",
		       (GtkSignalFunc) button_press_event, NULL);
   gtk_widget_set_events (drawing_left, GDK_EXPOSURE_MASK
			  |GDK_BUTTON_PRESS_MASK);
   //		gtk_widget_set_extension_events (drawing_left, GDK_EXTENSION_EVENTS_ALL);

   
   // respecto al drawing_about
   hbox_text_help = gtk_hbox_new ( FALSE, 0);
   gtk_widget_show( hbox_text_help );
   
   text_help = gtk_text_new(NULL,NULL);
   gtk_box_pack_start( GTK_BOX(hbox_text_help), text_help, TRUE,TRUE,0);
   gtk_widget_show(text_help);
   
   vscrollbar_help = gtk_vscrollbar_new (GTK_TEXT (text_help)->vadj);
   gtk_box_pack_start( GTK_BOX(hbox_text_help), vscrollbar_help, FALSE,TRUE,0);
   gtk_widget_show (vscrollbar_help);
   
   label_left = gtk_label_new("My board");
   gtk_notebook_append_page ( GTK_NOTEBOOK(notebook_left),drawing_left,label_left);
   label_left = gtk_label_new("Hide / Help / Credits");
   gtk_notebook_append_page ( GTK_NOTEBOOK(notebook_left),hbox_text_help,label_left);

   bn_help();
      
   /* center */
   
   vbox_buttons = gtk_vbox_new ( FALSE, 10);
   gtk_container_add ( GTK_CONTAINER (hbox), vbox_buttons );
   gtk_container_border_width( GTK_CONTAINER(vbox_buttons),10);
   gtk_widget_show( vbox_buttons );
   
   button_connect = gtk_button_new_with_label("Connect");
   gtk_box_pack_start( GTK_BOX(vbox_buttons), button_connect, TRUE,TRUE,0);
   gtk_signal_connect_object( GTK_OBJECT (button_connect), "clicked",
			     GTK_SIGNAL_FUNC(init_cliente),
			     GTK_OBJECT (window) );
   gtk_widget_show(button_connect);
   gtk_tooltips_set_tip (tooltips,button_connect,
			 "Connect to the bnserver", 
			 "Connect to the bnserver");
   
   button_disconnect = gtk_button_new_with_label("Disconnect");
   gtk_box_pack_start( GTK_BOX(vbox_buttons), button_disconnect, TRUE,TRUE,0);
   gtk_signal_connect_object( GTK_OBJECT (button_disconnect), "clicked",
			     GTK_SIGNAL_FUNC(init_cliente),
			     GTK_OBJECT (window) );
   gtk_widget_hide(button_disconnect);
   gtk_tooltips_set_tip (tooltips,button_disconnect,
			 "Disconnect from the bnserver", 
			 "Disconnect from the bnserver");
   
   button_sendboard = gtk_button_new_with_label ("Send Board");
   gtk_box_pack_start( GTK_BOX(vbox_buttons), button_sendboard,TRUE,TRUE,0);
   gtk_signal_connect_object( GTK_OBJECT (button_sendboard), "clicked",
			     GTK_SIGNAL_FUNC(play),
			     GTK_OBJECT (window) );
   gtk_widget_set_sensitive(button_sendboard,FALSE);
   gtk_widget_show(button_sendboard);
   gtk_tooltips_set_tip (tooltips,button_sendboard,
			 "Send your board (ships) to the bnserver",
			 "Send your board (ships) to the bnserver");

   
   button_start = gtk_button_new_with_label("Start");
   gtk_box_pack_start( GTK_BOX(vbox_buttons), button_start, TRUE,TRUE,0);
   gtk_signal_connect_object( GTK_OBJECT (button_start), "clicked",
			     GTK_SIGNAL_FUNC(play),
			     GTK_OBJECT (window) );
   gtk_widget_hide(button_start);
   gtk_tooltips_set_tip (tooltips,button_start,
			 "Start the game",
			 "Start the game");
   
   button_sendmsg = gtk_button_new_with_label ("Send Message");
   gtk_box_pack_start( GTK_BOX(vbox_buttons), button_sendmsg,TRUE,TRUE,0);
   gtk_signal_connect_object( GTK_OBJECT (button_sendmsg), "clicked",
			     GTK_SIGNAL_FUNC(bnsendmsg),
			     GTK_OBJECT (window) );
   gtk_widget_set_sensitive(button_sendmsg,FALSE);
   gtk_widget_show(button_sendmsg);
   gtk_tooltips_set_tip (tooltips,button_sendmsg,
			 "Send a message to your enemies",
			 "Send a message to your enemies");
   
   button_status = gtk_button_new_with_label ("Status");
   gtk_box_pack_start( GTK_BOX(vbox_buttons), button_status,TRUE,TRUE,0);
   gtk_signal_connect_object( GTK_OBJECT (button_status), "clicked",
			     GTK_SIGNAL_FUNC(status),
			     GTK_OBJECT (window) );
   gtk_widget_set_sensitive(button_status,FALSE);
   gtk_widget_show(button_status);		
   gtk_tooltips_set_tip (tooltips,button_status,
			 "The status of the bnserver & your enemies",
			 "The status of the bnserver & your enemies");
   
   button_config = gtk_button_new_with_label ("Config");
   gtk_box_pack_start( GTK_BOX(vbox_buttons), button_config,TRUE,TRUE,0);
   gtk_signal_connect_object( GTK_OBJECT (button_config), "clicked",
			     GTK_SIGNAL_FUNC(configure),
			     GTK_OBJECT (window) );
   gtk_widget_show(button_config);		
   gtk_tooltips_set_tip (tooltips,button_config,
			 "Config the client",
			 "Config the client");
   
   button_quit = gtk_button_new_with_label ("Quit");
   gtk_box_pack_start( GTK_BOX(vbox_buttons), button_quit,TRUE,TRUE,0);
   gtk_signal_connect_object( GTK_OBJECT (button_quit), "clicked",
			     GTK_SIGNAL_FUNC(do_exit),
			     GTK_OBJECT (window) );
   gtk_widget_show(button_quit);
   gtk_tooltips_set_tip (tooltips,button_quit,
			 "Quit the game",
			 "Quit the game");
   /* right */
   notebook_right = gtk_notebook_new();
   gtk_signal_connect ( GTK_OBJECT ( notebook_right ), "switch_page",
		       GTK_SIGNAL_FUNC ( page_switch ), NULL );
   gtk_notebook_set_tab_pos( GTK_NOTEBOOK(notebook_right),GTK_POS_TOP );
   gtk_box_pack_start( GTK_BOX(hbox),notebook_right,TRUE,TRUE,0);
   gtk_container_border_width( GTK_CONTAINER( notebook_right), 0 );
   gtk_notebook_set_scrollable( GTK_NOTEBOOK(notebook_right) , TRUE );
   gtk_notebook_set_show_tabs( GTK_NOTEBOOK(notebook_right) , TRUE );
   gtk_notebook_popup_enable( GTK_NOTEBOOK(notebook_right) );
   gtk_widget_realize( notebook_right );
   gtk_widget_show (notebook_right);

   drawing_right_about = gtk_drawing_area_new();
   gtk_drawing_area_size(GTK_DRAWING_AREA(drawing_right_about),200,200);
   gtk_signal_connect (GTK_OBJECT (drawing_right_about), "expose_event",
		       (GtkSignalFunc) expose_event_about, NULL );
   gtk_widget_set_events (drawing_right_about, GDK_EXPOSURE_MASK );
   gtk_widget_show( drawing_right_about);
   label_right_about = gtk_label_new("Batalla Naval" );
   gtk_widget_show( drawing_right_about);
   gtk_notebook_append_page( GTK_NOTEBOOK( notebook_right), drawing_right_about, label_right_about );
   

   /* Ventana de texto de abajo */
   separator = gtk_hseparator_new ();
   gtk_box_pack_start ( GTK_BOX(vbox), separator, FALSE,TRUE, 0);

   gtk_widget_show(separator);
   
   hbox_text = gtk_hbox_new ( FALSE, 0);
   gtk_container_add ( GTK_CONTAINER(vbox), hbox_text );
   gtk_widget_show( hbox_text );
   
   text = gtk_text_new(NULL,NULL);
   gtk_box_pack_start( GTK_BOX(hbox_text), text, TRUE,TRUE,0);
   gtk_widget_show(text);
   
   vscrollbar = gtk_vscrollbar_new (GTK_TEXT (text)->vadj);
   gtk_range_set_update_policy( GTK_RANGE( vscrollbar ), GTK_UPDATE_CONTINUOUS );
   gtk_box_pack_start( GTK_BOX(hbox_text), vscrollbar, FALSE,TRUE,0);
   gtk_widget_show (vscrollbar);
   
   gtk_text_freeze(GTK_TEXT(text));
   gtk_widget_realize(text); 
   gtk_text_insert( GTK_TEXT(text),NULL,NULL,NULL,"Batalla Naval - "GBNVER" by Ricardo Quesada (c) 1995,96,97,98\n",-1);
   
   gtk_text_thaw(GTK_TEXT(text));

   
   /* StatusBar */
   hbox_status = gtk_hbox_new ( FALSE, 0);
   gtk_container_add ( GTK_CONTAINER(vbox), hbox_status );
   gtk_container_border_width ( GTK_CONTAINER(hbox_status), 0);
   gtk_widget_show ( hbox_status ); 
   
   statusbar_right = gtk_statusbar_new();
   gtk_box_pack_end( GTK_BOX( hbox_status ), statusbar_right, TRUE,TRUE, 0);
   gtk_widget_show( statusbar_right );

   statusbar_left = gtk_statusbar_new();
   gtk_box_pack_end( GTK_BOX( hbox_status ), statusbar_left, TRUE,TRUE, 0);
   gtk_widget_show( statusbar_left );

   gtk_statusbar_push( GTK_STATUSBAR( statusbar_right ), 1, "Cliente GTK");
   gtk_statusbar_push( GTK_STATUSBAR( statusbar_left ), 1, "Batalla Naval");
   
   /* Pixmaps */
   fondo = gdk_pixmap_create_from_xpm_d( window->window,
					&mask,
					&window->style->bg[GTK_STATE_NORMAL],
					fondo_xpm
					);
   rojo = gdk_pixmap_create_from_xpm_d( window->window,
				       &mask,
				       &window->style->bg[GTK_STATE_NORMAL],
				       rojo_xpm
				       );
   verde = gdk_pixmap_create_from_xpm_d( window->window,
					&mask,
					&window->style->bg[GTK_STATE_NORMAL],
					verde_xpm
					);
   azul = gdk_pixmap_create_from_xpm_d( window->window,
				       &mask,
				       &window->style->bg[GTK_STATE_NORMAL],
				       azul_xpm
				     );
   negro = gdk_pixmap_create_from_xpm_d( window->window,
					&mask,
					&window->style->bg[GTK_STATE_NORMAL],
					negro_xpm
					);
   about = gdk_pixmap_create_from_xpm_d( window->window,
					&mask,
					&window->style->bg[GTK_STATE_NORMAL],
					about_xpm
					);
   gameover = gdk_pixmap_create_from_xpm_d( window->window,
					   &mask,
					   &window->style->bg[GTK_STATE_NORMAL],
					   gameover_xpm
					   );
   winner = gdk_pixmap_create_from_xpm_d( window->window,
					 &mask,
					 &window->style->bg[GTK_STATE_NORMAL],
					 winner_xpm
					 );
   icono = gdk_pixmap_create_from_xpm_d( window->window,
					&mask,
					&window->style->bg[GTK_STATE_NORMAL],
					icono_xpm
					);
   
   gdk_window_set_icon (window->window, NULL,
			icono , mask );

   
   /* Principal */   
   gtk_widget_show( window);
}


/*************************************************************************
 * 
 * 
 *                    CODIGO GENERICO
 *                   ( mentira !!!!! )
 * 
 *************************************************************************/
static size_t bnwrite(int fd,char *buf,char tip0,char tip1,char tip2,char jugyo )
{
   gint i;
   
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   proto.bnptip2=tip2;
   proto.jugador=jugyo;
   for(i=0;i<MSGMAXLEN;i++)
     proto.bnpmsg[i]=buf[i];
   strcpy(proto.bnphead,BNPHEAD);
   proto.bnpver=BNPVER;
   return( write(fd,&proto,MAXLEN) ) ;
}

/* convierte un char[10][10] a un char[100] */
void iwtable( char *dest)
{
   gint i,x,y;
   x=0;y=0;
   for(i=0;i<100;i++) 
     {
	dest[i]=mitabla[x][y];
	x++;
	if(x>=10) 
	  {
	     x=0;y++;
	  }
     }
}

/* pone por default la misma tabla que jugue antes */
void filtermiboard()  
{
   gint x,y;
   
   for(x=0;x<10;x++) 
     {
	for(y=0;y<10;y++) 
	  {
	     if( mitabla[x][y] >= BARCO ) 
	       mitabla[x][y]=BARCO;
	     else if( mitabla[x][y] <= NOBARCO ) 
	       mitabla[x][y]=NOBARCO;
	     
	  }
     }
}


/*
 * Pone en un temporal datos ( Usados ppalmente por Cli GTK )
 */
void putintemp( char *table )
{
   gint i;
   for(i=0;i<100;i++)
     usuario.tempclit[i]=table[i];
}

void showboard( GdkPixmap *pixmap )
{
   gint i;
   
   i = gtk_notebook_current_page( GTK_NOTEBOOK( notebook_right ) );
   i = usuario.pages[ i ] ;  /* puto bug resuelto */
   
   gdk_draw_pixmap( drawing_right[i]->window,
		   drawing_right[i]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[i])],
		   pixmap,
		   0,0,
		   0,0,
		   200,200
		   );
}

/* funcion que rellena los tableros
 * IN: char * - relleno
 * IN: int - 0 - miboard
 *         - 1 - tuboard
 */
void fillboard( char *filltable, int a )
{
   gint i,j;
   gint k;
   
   i=0;j=0;
   
   for(k=0;k<100;k++)
     {
	
	if(a==0)
	  pmicell( i,j, filltable[k]);
	if(a==1)
	  ptucell( i,j, filltable[k]);
	
	i++;
	if (i==10)
	  {
	     j++;i=0;
	  }
     }
}

void inteliclient( char *table)
{
   gint i,x,y;
   
   x=0;
   y=0;
   
   for(i=0;i<100;i++)
     {
	switch(table[i])
	  {
	   case HUNDIDO:
	   case TOCADO:
	     ptucell(x-1,y-1,AGUA);
	     ptucell(x-1,y+1,AGUA);
	     ptucell(x+1,y-1,AGUA);							
	     ptucell(x+1,y+1,AGUA);
	     break;
	   case NOBARCO:
	     if(x<9 && table[i+1]==HUNDIDO)
	       ptucell(x,y,AGUA);
	     
	     if(x>0 && table[i-1]==HUNDIDO)
	       ptucell(x,y,AGUA);
	     
	     if(y<9 && table[i+10]==HUNDIDO)
	       ptucell(x,y,AGUA);
	     
	     if(y>0 && table[i-10]==HUNDIDO)
	       ptucell(x,y,AGUA);
	     break;
	   default:
	     break;
	  }
	x++;
	if(x==10)
	  {
	     x=0;
	     y++;
	  }
     }
}


/***************************************************************************
 *                       FUNCIONES DE LOS BOTONES
 ***************************************************************************/
/* Dis/Connect esta en las funciones INIT con el nombre de init_cliente */

/* quit */
void destroy_window( GtkWidget *widget, GtkWidget **window )
{
   *window = NULL;
}

void do_exit()
{
   gtk_exit(0);
}

/* Boton SendBoard y Start */
int play( void )
{
   gchar outbuf[MSGMAXLEN];
   gchar temptable[100]; /* ver 0.50 */
   usuario.gano=0;
   
   /* Send Board */
   if( usuario.play==1)  
     {
	filtermiboard();
	iwtable(temptable);
	fillboard(temptable,0); /* v0.50 */
	
	iwtable( outbuf );
	bnwrite(sock,outbuf,BNWRI,usuario.numjug,0,usuario.numjug);
	return( TRUE );
     }
   
   /* Start */
   else if(usuario.play==2) 
     {
	bnwrite(sock,outbuf,BNSTR,usuario.numjug,0,usuario.numjug);
     }
   
   return(TRUE);
}

/* Boton Status */
void status( )
{
   gchar tempbuf[100];
   bnwrite(sock,tempbuf,BNSTS,0,0,usuario.numjug);
}


/* Boton Config */
static gint configure_apply( GtkWidget *widget, GtkWidget *window )
{
      
   if( strncmp( usuario.nombre, gtk_entry_get_text( GTK_ENTRY( entry_name)),
	       MAXNAMELEN) != 0)
     {
	if( usuario.play>=1 )
	  ttyfill("You must disconnect & then reconnect to public your new name\n");
	
	strncpy( usuario.nombre,
		gtk_entry_get_text( GTK_ENTRY( entry_name )),
		MAXNAMELEN
		);
     }
   
   strncpy( usuario.server_name,
	   gtk_entry_get_text( GTK_ENTRY( entry_server )),
	   50
	   );
   usuario.port = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner_port));
   
   return TRUE;
}

static gint configure_ok( GtkWidget *widget, GtkWidget *window )
{
   configure_apply( widget, window );
   gtk_widget_destroy( window );
   return TRUE;
}

void configure( void )
{
   static GtkWidget *win_configure=NULL;
   GtkWidget *main_vbox;
   GtkWidget *vbox;
   GtkWidget *hbox1;
   GtkWidget *hbox2;
   GtkWidget *hbox3;
   GtkWidget *hbox4;
   GtkWidget *frame;
   GtkWidget *label1;
   GtkWidget *label2;
   GtkWidget *label3;
   GtkWidget *separator;
   GtkWidget *button_ok;
   GtkWidget *button_apply;
   GtkWidget *button_cancel;
   GtkAdjustment *adj;

   if(!win_configure)
     {
	win_configure = gtk_widget_new (gtk_window_get_type (),
					"GtkWindow::type", GTK_WINDOW_TOPLEVEL,
					"GtkWindow::title", "Batalla Naval - Config",
					"GtkContainer::border_width",0,
					NULL);
	gtk_signal_connect ( GTK_OBJECT( win_configure), "destroy",
			    GTK_SIGNAL_FUNC( destroy_window ),
			    &win_configure );
	gtk_signal_connect ( GTK_OBJECT( win_configure), "delete_event",
			    GTK_SIGNAL_FUNC( destroy_window),
			    &win_configure );
	
	main_vbox = gtk_vbox_new( FALSE, 0);
	gtk_container_add ( GTK_CONTAINER(win_configure), main_vbox );
	
	frame = gtk_frame_new ("Configuration");
	gtk_box_pack_start( GTK_BOX( main_vbox ), frame, TRUE, TRUE, 0);
	
	vbox = gtk_vbox_new( FALSE, 0);
	gtk_container_add ( GTK_CONTAINER(frame), vbox );
	
	hbox1 = gtk_hbox_new( FALSE, 0);
	gtk_container_add( GTK_CONTAINER( vbox ), hbox1 );
	label1 = gtk_label_new("Port:");
	gtk_box_pack_start( GTK_BOX( hbox1), label1, TRUE, TRUE, 0);
	adj = (GtkAdjustment * ) gtk_adjustment_new( usuario.port, 1.0, 65536.0, 1.0, 5.0, 1.0 );
	spinner_port = gtk_spin_button_new( adj, 0, 0);
	gtk_box_pack_start( GTK_BOX( hbox1), spinner_port, TRUE, TRUE, 0);
	
	hbox2 = gtk_hbox_new( FALSE, 0);
	gtk_container_add( GTK_CONTAINER( vbox ), hbox2 );
	label2 = gtk_label_new("Server name:");
	gtk_box_pack_start( GTK_BOX( hbox2), label2, TRUE, TRUE, 0);
	entry_server = gtk_entry_new( );
	gtk_entry_set_text( GTK_ENTRY( entry_server ), usuario.server_name);
	gtk_box_pack_start( GTK_BOX( hbox2), entry_server, TRUE, TRUE, 0);
	
	hbox3 = gtk_hbox_new( FALSE, 0);
	gtk_container_add( GTK_CONTAINER( vbox ), hbox3 );
	label3 = gtk_label_new("User name:");
	gtk_box_pack_start( GTK_BOX( hbox3), label3, TRUE, TRUE, 0);
	entry_name = gtk_entry_new( );
	gtk_entry_set_text( GTK_ENTRY( entry_name ), usuario.nombre);
	gtk_box_pack_start( GTK_BOX( hbox3), entry_name, TRUE, TRUE, 0);
	
	separator = gtk_hseparator_new ();
	gtk_box_pack_start (GTK_BOX( main_vbox), separator, FALSE, TRUE, 0);
	
	hbox4 = gtk_hbox_new (FALSE, 10);
	gtk_box_pack_start (GTK_BOX (main_vbox), hbox4, FALSE, TRUE, 0);
	
	button_ok = gtk_button_new_with_label ("OK");
	gtk_signal_connect_object (GTK_OBJECT (button_ok), "clicked",
				   GTK_SIGNAL_FUNC(configure_ok),
				   GTK_OBJECT(win_configure) );
	gtk_box_pack_start (GTK_BOX (hbox4), button_ok, TRUE, TRUE, 0);
	
	button_apply = gtk_button_new_with_label ("Apply");
	gtk_signal_connect_object (GTK_OBJECT (button_apply), "clicked",
				   GTK_SIGNAL_FUNC(configure_apply),
				   GTK_OBJECT( win_configure ));
	gtk_box_pack_start (GTK_BOX (hbox4), button_apply, TRUE, TRUE, 0);

	button_cancel = gtk_button_new_with_label ("Cancel");
	gtk_signal_connect_object (GTK_OBJECT (button_cancel), "clicked",
				   GTK_SIGNAL_FUNC(gtk_widget_destroy),
				   GTK_OBJECT(win_configure) );
	gtk_box_pack_start (GTK_BOX (hbox4), button_cancel, TRUE, TRUE, 0);
	
	GTK_WIDGET_SET_FLAGS (button_ok, GTK_CAN_DEFAULT);
	gtk_widget_grab_default (button_ok);
     }

   if (!GTK_WIDGET_VISIBLE (win_configure))
     gtk_widget_show_all (win_configure);
   else
     gtk_widget_destroy (win_configure);
}

/* Boton SendMsg */
static gint sendmsg_ok( GtkWidget *widget, GtkWidget *window )
{
   strncpy( usuario.tomsg,
	   gtk_entry_get_text( GTK_ENTRY( entry_message )),
	   MSGMAXLEN
	   );
   usuario.towho = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner_towho));
   usuario.lecho = GTK_CHECK_BUTTON( toggle_button )->toggle_button.active ;

   if(usuario.play>=1)
     bnwrite(sock,usuario.tomsg,BNMSG,usuario.towho,usuario.lecho,usuario.numjug);
   
   gtk_widget_destroy( window );
   return TRUE;
}

void bnsendmsg( void )
{
   static GtkWidget *win_sendmsg=NULL;
   GtkWidget *main_vbox;
   GtkWidget *vbox;
   GtkWidget *hbox1;
   GtkWidget *hbox2;
   GtkWidget *hbox4;
   GtkWidget *frame;
   GtkWidget *label1;
   GtkWidget *label2;
   GtkWidget *separator;
   GtkWidget *button_ok;
   GtkWidget *button_cancel;
   GtkAdjustment *adj;
   
   if(!win_sendmsg)
     {
	win_sendmsg = gtk_widget_new (gtk_window_get_type (),
				      "GtkWindow::type", GTK_WINDOW_TOPLEVEL,
				      "GtkWindow::title", "Batalla Naval - Send Message",
				      "GtkContainer::border_width",0,
				      NULL);
	gtk_signal_connect ( GTK_OBJECT( win_sendmsg ), "destroy",
			    GTK_SIGNAL_FUNC( destroy_window ),
			    &win_sendmsg );
	gtk_signal_connect ( GTK_OBJECT( win_sendmsg ), "delete_event",
			    GTK_SIGNAL_FUNC( destroy_window),
			    &win_sendmsg );
	
	main_vbox = gtk_vbox_new( FALSE, 0);
	gtk_container_add ( GTK_CONTAINER(win_sendmsg), main_vbox );
	
	frame = gtk_frame_new ("Send Message");
	gtk_box_pack_start( GTK_BOX( main_vbox ), frame, TRUE, TRUE, 0);
	
	vbox = gtk_vbox_new( FALSE, 0);
	gtk_container_add ( GTK_CONTAINER(frame), vbox );
	
	hbox1 = gtk_hbox_new( FALSE, 0);
	gtk_container_add( GTK_CONTAINER( vbox ), hbox1 );
	label1 = gtk_label_new("To number of player (0=broadcast):");
	gtk_box_pack_start( GTK_BOX( hbox1), label1, TRUE, TRUE, 0);
	adj = (GtkAdjustment * ) gtk_adjustment_new( usuario.towho, 0.0, MAXPLAYER, 1.0, 1.0, 1.0 );
	spinner_towho = gtk_spin_button_new( adj, 0, 0);
	gtk_box_pack_start( GTK_BOX( hbox1), spinner_towho, TRUE, TRUE, 0);
	
	hbox2 = gtk_hbox_new( FALSE, 0);
	gtk_container_add( GTK_CONTAINER( vbox ), hbox2 );
	label2 = gtk_label_new("Message:");
	gtk_box_pack_start( GTK_BOX( hbox2), label2, TRUE, TRUE, 0);
	entry_message = gtk_entry_new( );
	gtk_entry_set_text( GTK_ENTRY( entry_message ), "");
	gtk_box_pack_start( GTK_BOX( hbox2), entry_message, TRUE, TRUE, 0);
	
	
	toggle_button  = gtk_check_button_new_with_label("Local echo");
	gtk_box_pack_start( GTK_BOX( vbox ), toggle_button, TRUE, TRUE, 0);
	
	separator = gtk_hseparator_new ();
	gtk_box_pack_start (GTK_BOX( main_vbox), separator, FALSE, TRUE, 0);
	
	hbox4 = gtk_hbox_new (FALSE, 10);
	gtk_box_pack_start (GTK_BOX (main_vbox), hbox4, FALSE, TRUE, 0);
	
	button_ok = gtk_button_new_with_label ("OK");
	gtk_signal_connect_object (GTK_OBJECT (button_ok), "clicked",
				   GTK_SIGNAL_FUNC(sendmsg_ok),
				   GTK_OBJECT(win_sendmsg) );
	gtk_box_pack_start (GTK_BOX (hbox4), button_ok, TRUE, TRUE, 0);

	button_cancel = gtk_button_new_with_label ("Cancel");
	gtk_signal_connect_object (GTK_OBJECT (button_cancel), "clicked",
				   GTK_SIGNAL_FUNC(gtk_widget_destroy),
				   GTK_OBJECT(win_sendmsg) );
	gtk_box_pack_start (GTK_BOX (hbox4), button_cancel, TRUE, TRUE, 0);
	
	GTK_WIDGET_SET_FLAGS (button_ok, GTK_CAN_DEFAULT);
	gtk_widget_grab_default (button_ok);
     }

   if(!GTK_WIDGET_VISIBLE( win_sendmsg))
     gtk_widget_show_all( win_sendmsg );
   else
     gtk_widget_destroy( win_sendmsg );
}


/****************************************************************************
 *                        FUNCION QUE LEE EL SOCKET
 ****************************************************************************/
static void proceso( gpointer data, gint sock, GdkInputCondition GDK_INPUT_READ )
{
   struct protocolo proto;
   gchar outbuf[MSGMAXLEN];
   gchar outbufi[MSGMAXLEN];
   gchar temptable[100];     /* v0.50 */
   gint i,j;
   
   
   if( read(sock,&proto,MAXLEN) == MAXLEN)
     {
	switch(proto.bnptip0) 
	  {
	   case BNTST:
	     break;
	   case BNWRI:
	     if(proto.bnpmsg[0]==1) 
	       {
		  ttyfill("The board's OK. Press 'Start' and enjoy the game.\n");
		  usuario.play=2;
		  
		  gtk_widget_hide( button_sendboard );
		  gtk_widget_show( button_start );
		  gtk_widget_set_sensitive(button_start,TRUE);
	       }
	     else if(proto.bnpmsg[0]==0) 
	       {
		  ttyfill("ERROR: The board's not OK. Check it, please.\n");
		  usuario.play=1;
	       }
	     break;
	   case BNJUG:
	     usuario.numjug=proto.bnptip1;

	     sprintf(outbuf,"Connected to bnserver:%s. You are player number:%i",proto.bnpmsg,usuario.numjug);
	     foot_right(outbuf);
	     strcat(outbuf,".\n");
	     ttyfill(outbuf);

	     usuario.play = 1;
	     
	     gtk_widget_hide( button_connect );
	     gtk_widget_show( button_disconnect );
	     
	     gtk_widget_set_sensitive(button_sendboard,TRUE);
	     gtk_widget_set_sensitive(button_status,TRUE);
	     gtk_widget_set_sensitive(button_sendmsg,TRUE);
	     
	     
	     bnwrite(sock,outbuf,BNVER,usuario.usrfrom,bnsup,usuario.numjug);
	     
	     break;
	   case BNVER:
	     sprintf(outbuf,"bnserver version:%i.%i\n",proto.bnpmsg[0],proto.bnpmsg[1]);
	     ttyfill(outbuf);
	     break;
	   case BNMSG:
	     strcpy(outbuf,proto.bnpmsg);
	     strcat(outbuf,"\n");
	     ttyfill(outbuf);
	     break;
	   case BNALL:
	     ttyfill("bnserver is full. Try later, if you are so kind.\n");
	     close(sock);
	     break;
	   case BNWAI:
	     ttyfill("You can't start a game while people are playing.Try later\n");
	     break;
	   case BNHIT:
	     if(usuario.play>=3) 
	       {
		  mitabla[(int)proto.bnpmsg[0]][(int)proto.bnpmsg[1]]=(int)proto.bnpmsg[2];
		  if(usuario.hide==FALSE)
		    {
		       iwtable(temptable);
		       fillboard(temptable,0); /* v0.50 */
		    }
	       }
	     bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
	     break;
	   case BNREA:
	     if(usuario.play>=3)
	       {
		  putintemp(proto.bnpmsg);   /* v0.60 */
		  fillboard(proto.bnpmsg,1); /* v0.50 */
		  inteliclient(proto.bnpmsg);
	       }
	     
	     break;
	   case BNSTR:
	     ttyfill("Great! Please fasten your seatbelt and wait your turn.\n"); 
	     sprintf(outbuf,"Available players:");
	     
	     for(i=0;i<MAXPLAYER;i++)
	       usuario.pages[i]=i;
	     
	     for(i=0;i<100;i++)
	       usuario.tempclit[i]=NOBARCO;

	     j = g_list_length( GTK_NOTEBOOK(notebook_right)->children);
	     for (i = 0; i < j; i++)
	       gtk_notebook_remove_page ( GTK_NOTEBOOK( notebook_right), 0);
	     
	     for(i=0;i<MAXPLAYER;i++)
	       {
		  if(proto.bnpmsg[i]>=2)
		    {
		       sprintf(outbufi," %i",i+1);
		       strcat(outbuf,outbufi);
		       strcpy(usuario.names[i],&proto.bnpmsg[5+i*MAXNAMELEN]);
		       
		       label_right[i] = gtk_label_new( usuario.names[i] );
		       label_right2[i] = gtk_label_new( usuario.names[i] );		       
		       drawing_right[i] = gtk_drawing_area_new();
		       gtk_drawing_area_size(GTK_DRAWING_AREA(drawing_right[i]),200,200);
		       gtk_signal_connect (GTK_OBJECT (drawing_right[i]), "expose_event",
					   (GtkSignalFunc) expose_event_right, NULL);
		       gtk_signal_connect (GTK_OBJECT (drawing_right[i]), "button_press_event",
					   (GtkSignalFunc) button_press_event_right, NULL);
		       gtk_widget_set_events (drawing_right[i], GDK_EXPOSURE_MASK
					      |GDK_BUTTON_PRESS_MASK);

		       gtk_widget_show( label_right[i] );
		       gtk_widget_show( label_right2[i] );
		       gtk_widget_show( drawing_right[i] );
		       gtk_notebook_append_page_menu ( GTK_NOTEBOOK(notebook_right),
						      drawing_right[i],
						      label_right[i],
						      label_right2[i] );
		       
		    }
		  else
		    {
		       sacar_usrpage(i);
		    }
	       }
	     
	     strcat(outbuf,".\n");
	     ttyfill(outbuf);
	     
	     strcpy(outbuf,"You are ");
	     strncat(outbuf,usuario.names[usuario.numjug-1],MAXNAMELEN);
	     foot_left(outbuf);

	     if(usuario.play==2) 
	       {
		  usuario.play=3;
		  gtk_widget_set_sensitive(button_start,FALSE);
	       }
	     
	     /*
	      * pone automaticamente al proximo jugador 
	      */
	     i = buscar_usr( usuario.numjug - 1 );
	     gtk_notebook_set_page( GTK_NOTEBOOK( notebook_right ),i );
	     gtk_notebook_next_page( GTK_NOTEBOOK(notebook_right));
	     i = gtk_notebook_current_page( GTK_NOTEBOOK( notebook_right ));
	     usuario.usrfrom = usuario.pages[ i ] +1 ;
	     
	     break;
	   case BNCON:
	     sprintf(outbuf,"Player %i is ready to play\n",proto.bnptip1);
	     ttyfill(outbuf);
	     break;
	   case BNWHO:          
	     sprintf(outbuf,"It's %s turn.",usuario.names[proto.bnptip1-1]);
	     foot_right(outbuf);
	     strcat(outbuf,"\n");
	     ttyfill(outbuf);
	     break;
	   case BNTRN:
	     usuario.play=4;                    /* oh, it is my turn */
	     ttyfill("It's your turn.\n");
	     foot_right("Cool, It's my turn");
	     break;
	   case BNEXT:
	     i=proto.bnptip1-1;

	     remove_page( sacar_usrpage(i) );
	     
	     usuario.names[i][0]=0;
	     strcpy(outbuf,proto.bnpmsg);
	     strcat(outbuf,"\n");
	     ttyfill(outbuf);
	     
	     for(i=0;i<MAXPLAYER;i++) 
	       {       
		  if(usuario.numjug-1!=i) 
		    {
		       if(strcmp(usuario.names[i],"")!=0) 
			 usuario.usrfrom=i+1;
		    }
	       }
//	     if(usuario.play>=3)
//	       bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
	     break;
	   case BNLOS:
	     usuario.gano=-1;
	     usuario.play=1;                    /* oh, i lost the game */
	     ttyfill("It's a pity, but the game is over for you\n");

	     showboard(gameover);
	     for(i=0;i<MAXPLAYER;i++)
	       usuario.names[i][0]=0;
	     
	     gtk_widget_show( button_sendboard );
	     gtk_widget_hide( button_start );
	     
	     usuario.firsthit=0;            /* new on v0.41 */
	     //	strcpy(outbuf,"My board");
	     //	dname(outbuf,0);
	     //	strcpy(outbuf,"Enemy's board");
	     //	dname(outbuf,1);
	     //	strcpy(outbuf,"Batalla Naval");
	     //	foot_right(outbuf);
	     break;
	   case BNWIN:
	     usuario.gano=1;
	     usuario.play=1;                    /* oh, I won the game */
	     showboard(winner);
	     for(i=0;i<MAXPLAYER;i++)
	       usuario.names[i][0]=0;       /* clear all names */
	     
	     ttyfill("Cool, you are the winner.\n");

	     
	     gtk_widget_show( button_sendboard );
	     gtk_widget_hide( button_start );
	     
	     usuario.firsthit=0;            /* new on v0.41 */
	     //							strcpy(outbuf,"My board");
	     //							dname(outbuf,0); 
	     //							strcpy(outbuf,"Enemy's board");
	     //							dname(outbuf,1);
	     //							strcpy(outbuf,"Batalla Naval");
	     //							foot_right(outbuf);
	     break;
	   case BNSOL:
	     ttyfill("If you want to play alone, make the robot, OK?\n");
	     break;
	   case BNSTS:         
	     for(i=0;i<MAXPLAYER;i++) 
	       {
		  sprintf(outbuf,"Player %i (%s), name='%s'\n"
			  ,i+1,usuario.status[(int)proto.bnpmsg[i]],&proto.bnpmsg[5+i*MAXNAMELEN]);
		  ttyfill(outbuf);
	       }
	     break;
	   default:
	     sprintf(outbuf,"BNSERVER sending code %X. Ummm... update the client please.\n",proto.bnptip0);
	     ttyfill(outbuf);
	     break;
	  }
     }
}

/***************************************************************************
 *                               FUNCIONES INIT
 ***************************************************************************/
void init_datos( void )
{
   gint i,j;
   
   usuario.play = 0;
   usuario.usrfrom=1;
   usuario.lecho=1;
   usuario.gano=0;
   
   for(i=0;i<10;i++) 
     {          /* clean tabla */
	for(j=0;j<10;j++)
	  mitabla[i][j]=0;
     } 
   usuario.firsthit=0;
   for(i=0;i<MAXPLAYER;i++)
     {
	usuario.names[i][0]=0;       /* clear all names */
	usuario.pages[i]=i;
     }
   strncpy(usuario.status[DISCON],"not connected",16);
   strncpy(usuario.status[CONNEC],"  connected  ",16);
   strncpy(usuario.status[BOARD], "ready to play",16);
   strncpy(usuario.status[PLAY],  "   playing   ",16);
   strncpy(usuario.status[TURN],  "  playing *  ",16);
   usuario.hide=FALSE;
}

int init_cliente( void )
{
   gchar outbuf[MSGMAXLEN];
   gint i;
   
   if(usuario.play==0) 
     {
	/* Create socket */
	sock=socket(AF_INET,SOCK_STREAM,0);
	if(sock <0) 
	  {
	     ttyfill("ERROR: creating stream socket\n");
	     return(1);
	  }
	host_info = gethostbyname(usuario.server_name);
	if(host_info==NULL) 
	  {
	     close(sock);       
	     ttyfill("ERROR: Unknown host. Press 'Config'\n");
	     return(2);
	  }
	server.sin_family=host_info->h_addrtype;
	memcpy( (char*) &server.sin_addr, host_info->h_addr,host_info->h_length);
	server.sin_port=htons(usuario.port);
	
	
	if(connect(sock,(struct sockaddr *)&server,sizeof(server))< 0) 
	  {
	     close(sock);          
	     ttyfill("ERROR: Press 'Config' or start the bnserver. Ok? \n");
	     return(2);
	  }
	
	/* Propio de las GTK, OK ?, perdon de las GDK */
	usuario.tag = gdk_input_add( sock, GDK_INPUT_READ, proceso, temp );
	
	
	strncpy(outbuf,usuario.nombre,MAXNAMELEN);
	bnwrite(sock,outbuf,BNJUG,GCLI,GBNVERH,GBNVERL);       /* WHO AM I */
	return(0);
     }
   else if(usuario.play!=0) 
     {
	gtk_widget_show( button_connect );
	gtk_widget_hide( button_disconnect );
	gtk_widget_show( button_sendboard );
	gtk_widget_hide( button_start );
	
	gtk_widget_set_sensitive(button_sendboard,FALSE);
	gtk_widget_set_sensitive(button_sendmsg,FALSE);
	gtk_widget_set_sensitive(button_status,FALSE);
	gtk_widget_set_sensitive(button_sendmsg,FALSE);
	
	ttyfill("Press 'Connect' again to join a game.\n");
	foot_right("Press 'Connect' again to join a game");
	foot_left("Batalla Naval");
	usuario.firsthit=0;             
	for(i=0;i<MAXPLAYER;i++)
	  {
	     usuario.names[i][0]=0;
	     usuario.pages[i]=i;
	  }
	usuario.play=0;
	bnwrite(sock,outbuf,BNEXT,0,0,usuario.numjug);				  
	close(sock);
	
	gdk_input_remove( usuario.tag );
     }
   return(0);
}

void help()
{
   printf("\n\nBatalla Naval "GBNVER"  (c) 1995,96,97,98 Ricardo Quesada\n");
   printf("\nsyntax:");
   printf("\ngbnclient [-s host_name] [-p server_port] [-u username]");
   printf("\n\t-s host_name:   The name of the host where bnserver is running");
   printf("\n\t                default is: localhost");
   printf("\n\t-p server_port: The number of the port that bnserver is listening to");
   printf("\n\t                default is: 1995");
   printf("\n\t-u username:    Your name or alias");
   printf("\n\t                default is your login name");
   printf("\n\nSend bugs,doubts,fixes to rquesada@dc.uba.ar\n");
   exit(-1);
}


/****************************************************************************
 *                           MAIN * MAIN * MAIN
 ****************************************************************************/

int main (int argc, char *argv[])
{
   gint i;
   
   gtk_set_locale();         /* no se que hace */
   gtk_init (&argc, &argv);
   gtk_rc_parse("gbnclientrc");
   
   strcpy( usuario.server_name,"localhost") ;
   usuario.port=1995 ;
   strncpy( usuario.nombre, getenv("LOGNAME"), MAXNAMELEN) ;
   
   for(i=1;i<argc;i++) 
     {
	if(argv[i][0]=='-') 
	  {
	     switch(argv[i][1]) 
	       {
		case 'x':
		  bnsup=argv[i][2];
		  break;
		case 's': case 'S':
		  if(argc>i+1) 
		    strncpy( usuario.server_name,argv[i+1],50);
		  else 
		    help();
		  break;
		case 'u': case 'U':
		  if(argc>i+1) 
		    strncpy( usuario.nombre,argv[i+1],MAXNAMELEN);
		  else 
		    help();
		  break;
		case 'p': case 'P':
		  if(argc>i+1) 
		    usuario.port=atoi(argv[i+1]);
		  else 
		    help();
		  break;
		case 'h': case 'H':
		case '?': case '-':
		default:
		  help();
		  break;
	       }
	  }
	else 
	  {
	     if(argv[i-1][0]!='-')
	       help();
	  }
     }
   
   
   init_datos();                  /* inicializa variables */
   init_X();              /* inicializa entorno y juego */
   init_cliente();
   
   //		fillboard(Bboard,0);
   //		fillboard(Nboard,1); 
   
   gtk_main ();
   return 0;
}
