/*
 * Batalla Naval (c) 1995,96,97,98 Ricardo Quesada
 * (rquesada@dc.uba.ar)
 * 
 * Cliente X 
 * version: 0.60
 *  
 * Depende de las librerias xview
 */

/*
 *  New in v0.50
 * --------------
 * - Tiene un cliente 'inteligente'
 * 
 * - No usa las librerias SlingShot: Estas librerias no las use porque no las 
 * tenia en Debian, y en vez de portalas decidi modificar el juego.
 * 
 * - Esta version igual depende en las 'buggy' librerias xview. Tiene algunos 
 * bugs corregidos, aunque creo que todavia quedan algunos sueltos por ahi.
 *
 */

/*
 * Includes de X y de las xview 
 */
#include <X11/Xlib.h> 
#include <xview/xview.h> 
#include <xview/window.h> 
#include <xview/openmenu.h> 
#include <xview/panel.h>
#include <xview/canvas.h>
#include <xview/xv_xrect.h>
#include <xview/frame.h>
#include <xview/cms.h>
#include <xview/notify.h>
#include <xview/svrimage.h>     
#include <xview/icon.h>         
#include <xview/ttysw.h>        

/* 
 * includes para sockets y otras yerbas 
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <stdio.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>
#include "../share/protocol.h"           /* definicion del protocolo */
#include "../share/tablas.h"             /* boards for win,lost,etc. new on v0.30 */
#include "batnav.icon.h"        /* fucking icono */

/*
 * Defines
 */ 
#define  WHITE  0  
#define  BLACK  1
#define  BLUE   2
#define  GREEN  3
#define  LBLUE  4
#define  YELLOW 5
#define  RED    6

#define MAXPLAYER 5
#define BN_XVER "0.60"
#define BN_XVERH 0
#define BN_XVERL 60

#define LARGO 20
#define ANCHO 20

#define RAPID 10                       /* the lower, more time for select() */

/* 
 *                   VARIABLES GLOBALES
 */

int sock;
struct sockaddr_in server;
struct hostent *host_info;
char mitabla[10][10];          /* mi tabla de barcos */
char barquitos[4];             /* tama;os de los barcos 1 de 4, 2 de 3, etc. */
short int x,y;                 /* punteros de la tabla mia */


Frame  frame,           /* frame principal que contiene todo */
       subframe1,       /* ventana about */
       subframe2,       /* ventana de config */
       subframe3;       /* ventana de sendmsg */

Panel  panel,           /* contenedor de los botones */
       panelcito,       /* connect/disconnect button */
       panelinfo,       /* config button */
       panelcit2,       /* send board/start button */
       panel3,          /* contenedor de sendmsg */
       panelcit3,       /* players button */
       panelcit4,       /* sendmsg button */
       panelcit5;       /* status button */

Canvas canvas,           /* del about */
       canvas_mib,       /* de miboard ya que no uso las sspkg */
       canvas_tub,       /* de tuboard - v0.50 */
       canvas_min,       /* nombre de mi board */
       canvas_tun;       /* nombre de tu board */

Menu menu;
Menu_item mi[MAXPLAYER];                 /* new on v0.36 */

/* 
 * I think that these variables could be put locally .
 * solo sera cuestion de probar ya que este codigo tiene
 * mas de 2 a�os de antiguedad
 */
Cms cms;                                 /* Colores */
Server_image icon_image;                 /* Icono v0.31 */
Icon icon;                               /* Icono v0.31 */
Tty  tty;                                /* Mensajes abajo v0.32 */
GC gc;                                   
XGCValues gc_val;
Display *display;                        /* */
XID xid; 
fd_set readfds;                           /* used by proceso() */
struct itimerval timer;                   /* used by proceso() */
char bnsup;                               /* usado por bnwrite */

#ifndef __linux
struct timeval timeout=  { 0,250000 };
#else
struct timeval timeout;
#endif

unsigned long *colors;

static Xv_singlecolor cms_colors[]= 
{
     { 255,255,255 },              /* white */
     {   0,  0,  0 },              /* black */
     {   0,  0, 255},              /* blue  INVERTED in v0.29 */
     {   0,255,  0 },              /* green INVERTED in v0.29 */
     {  30,230, 250},              /* light blue */
     { 255,255,  0 },              /* yellow */
     { 255,  0,  0 },              /* red */
     {   0,  0,  0 },              /* color por defecto */
};


/*
 * Estructura principal 
 */
struct usuario 
{
   char server_name[50];             /* server name */
   int port;                         /* server port */
   char nombre[MAXNAMELEN];          /* user name */
   char names[MAXPLAYER][MAXNAMELEN]; /* other players's name */
   
   int firsthit;                     /* para borrar BN new on 0.30 */
   int play;                         /* already playing ? */
   int numjug;                       /* number of player */
   int usrfrom;                      /* player who is owner of enemy window */
   
   char tomsg[MSGMAXLEN];            /* usuadas para mandar mensajes */
   int  towho;
   int  lecho;                       /* local echo */
   
   char status[5][16];               /* new on v0.43 */
   int  hide;                        /* hide my board new on v0.44 */
} usuario;

char fijo01[MSGMAXLEN];


/*  
 * Funciones
 */

/*
 * Funcion pricicipal de comunicaciones
 */
size_t bnwrite(int fd,char *buf,char tip0,char tip1,char tip2,char jugyo )
{
   int i;
   
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   proto.bnptip2=tip2;
   proto.jugador=jugyo;
   for(i=0;i<MSGMAXLEN;i++)
     proto.bnpmsg[i]=buf[i];
   strcpy(proto.bnphead,BNPHEAD);
   proto.bnpver=BNPVER;
   return( write(fd,&proto,MAXLEN) ) ;
}

/*
 * mandar mensajes a la teletype (tty)
 */
void ttyfill(char msg[]) 
{
   char messg[100];
   strncpy(messg,msg,100);
   ttysw_output(tty,messg,strlen(messg));
}

/* 2 funciones para escribir mensajes abajo */
void foot_right( char msg[] ) 
{
   char tempbuf[100];
   strncpy(tempbuf,msg,100);
   xv_set(frame,
	  FRAME_RIGHT_FOOTER,tempbuf,
	  NULL);
}
void foot_left( char msg[] ) 
{
   char tempbuf[100];
   strncpy(tempbuf,msg,100);
   xv_set(frame,
	  FRAME_LEFT_FOOTER,tempbuf,
	  NULL);
}

/* Draw  name */
void dname(char * nombre, int a) 
{
   Xv_Window xv_w;
   GC gc;
   Display *display;
   Window xwin;
   int largo;
   
   if(a==0)
     xv_w = canvas_paint_window(canvas_min);
   else if(a==1)
     xv_w = canvas_paint_window(canvas_tun);			
   
   xwin = xv_get(xv_w,XV_XID);
   display = (Display *) xv_get(canvas_mib,XV_DISPLAY);
   gc = DefaultGC(display,DefaultScreen(display));
   
   
   XSetForeground(display,gc,colors[WHITE]);
   XFillRectangle(display, xwin, gc,0,0,ANCHO*10, LARGO*2);		
   
   XSetForeground(display,gc,colors[BLACK]);
   largo=strlen(nombre);		
   XDrawString(display,xwin,gc,50,25,nombre,largo);
   
}

/* paint mi cell */
void pmicell(int x,int y,int color) 
{
   Xv_Window xv_w;
   GC gc;
   Display *display;
   Window xwin;
   
   if(x<10 && x>=0 && y>=0 && y<10)
     {
	xv_w = canvas_paint_window(canvas_mib);
	xwin = xv_get(xv_w,XV_XID);
	display = (Display *) xv_get(canvas_mib,XV_DISPLAY);
	gc = DefaultGC(display,DefaultScreen(display));
	
	XSetForeground(display,gc,colors[color]);
	XFillRectangle(display, xwin, gc,
		       x*ANCHO+1,y*LARGO+1,
		       ANCHO-1, LARGO-1);		
     }
}

/* Otro uso en v0.50 - usada por inteliclient */
void ptucell(int x,int y,int color)
{
   Xv_Window xv_w;
   GC gc;
   Display *display;
   Window xwin;
   
   if(x<10 && x>=0 && y>=0 && y<10)
     {
	xv_w = canvas_paint_window(canvas_tub);
	xwin = xv_get(xv_w,XV_XID);
	display = (Display *) xv_get(canvas_mib,XV_DISPLAY);
	gc = DefaultGC(display,DefaultScreen(display));
	
	XSetForeground(display,gc,colors[color]);
	XFillRectangle(display, xwin, gc, 
		       x*ANCHO+1,y*LARGO+1,
		       ANCHO-1, LARGO-1);
     }
}

/* v0.50 */
void inteliclient( char *table)
{
   int i,x,y;
   
   x=0;
   y=0;
   
   for(i=0;i<100;i++)
     {
	switch(table[i])
	  {
	   case HUNDIDO:
	   case TOCADO:
	     ptucell(x-1,y-1,BLUE);
	     ptucell(x-1,y+1,BLUE);
	     ptucell(x+1,y-1,BLUE);							
	     ptucell(x+1,y+1,BLUE);
	     break;
	   case NOBARCO:
	     if(x<9 && table[i+1]==HUNDIDO)
	       ptucell(x,y,BLUE);
	     
	     if(x>0 && table[i-1]==HUNDIDO)
	       ptucell(x,y,BLUE);
	     
	     if(y<9 && table[i+10]==HUNDIDO)
	       ptucell(x,y,BLUE);
	     
	     if(y>0 && table[i-10]==HUNDIDO)
	       ptucell(x,y,BLUE);
	     break;
	   default:
	     break;
	  }
	x++;
	if(x==10)
	  {
	     x=0;
	     y++;
	  }
     }
}
/* funcion que rellena los tableros
 * IN: char * - relleno
 * IN: int - 0 - miboard
 *         - 1 - tuboard
 */
void fillboard( char *filltable, int a )
{
   int i,j;
   Xv_Window xv_w;
   GC gc;
   Display *display;
   Window xwin;
   
   int k;
   
   /* board mio */
   if(a==0)
     {
	xv_w = canvas_paint_window(canvas_mib);
	xwin = xv_get(xv_w,XV_XID);
	display = (Display *) xv_get(canvas_mib,XV_DISPLAY);
	gc = DefaultGC(display,DefaultScreen(display)); 
     }
   /* board tuyo */
   else if(a==1)
     {
	xv_w = canvas_paint_window(canvas_tub);
	xwin = xv_get(xv_w,XV_XID);
	display = (Display *) xv_get(canvas_mib,XV_DISPLAY);
	gc = DefaultGC(display,DefaultScreen(display)); 
     }
   
   i=0;
   j=0;
   for(k=0;k<100;k++)
     {
	switch( filltable[k] )
	  {
	   case BARCO: /* 1 */
	     XSetForeground(display,gc,colors[GREEN]);
	     XFillRectangle(display, xwin, gc, 
			    i*ANCHO+1,j*LARGO+1,
			    ANCHO-1, LARGO-1);
	     break;
	   case NOBARCO: /* 0 */
	     XSetForeground(display,gc,colors[WHITE]);
	     XFillRectangle(display, xwin, gc, 
			    i*ANCHO+1,j*LARGO+1,
			    ANCHO-1, LARGO-1);
	     break;
	   case AGUA: /* -1 */
	     XSetForeground(display,gc,colors[BLUE]);
	     XFillRectangle(display, xwin, gc, 
			    i*ANCHO+1,j*LARGO+1,
			    ANCHO-1, LARGO-1);
	     break;
	   case TOCADO: /* 2 */
	     XSetForeground(display,gc,colors[RED]);
	     XFillRectangle(display, xwin, gc, 
			    i*ANCHO+1,j*LARGO+1,
			    ANCHO-1, LARGO-1);
	     break;
	   case HUNDIDO: /* 3 */
	     XSetForeground(display,gc,colors[BLACK]);
	     XFillRectangle(display, xwin, gc, 
			    i*ANCHO+1,j*LARGO+1,
			    ANCHO-1, LARGO-1);
	     break;
	   default:
	     XSetForeground(display,gc,colors[LBLUE]);
	     XFillRectangle(display, xwin, gc, 
			    i*ANCHO+1,j*LARGO+1,
			    ANCHO-1, LARGO-1);
	     break;
	  }
	i++;
	if (i==10)
	  {
	     j++;
	     i=0;
	  }
     }
}

/* void fill0( int b,int a ) -eliminada en v0.50 */

/* void repaintmi( void ) -eliminada en v0.50 */

/* void repainttu( char outbuf[] ) -eliminada en v0.50 */


/* convierte un char[10][10] a un char[100] */
void iwtable( char *dest) 
{
   int i,x,y;
   x=0;y=0;
   for(i=0;i<100;i++) 
     {
	dest[i]=mitabla[x][y];
	x++;
	if(x>=10) 
	  {
	     x=0;y++;
	  }
     }
}

/* pone por default la misma tabla que jugue antes */
void filtermiboard()  
{
   int x,y;
   
   for(x=0;x<10;x++) 
     {
	for(y=0;y<10;y++) 
	  {
	     if( mitabla[x][y] >= BARCO ) 
	       mitabla[x][y]=BARCO;
	     else if( mitabla[x][y] <= NOBARCO ) 
	       mitabla[x][y]=NOBARCO;
	     
	  }
     }
}


/*
 * Funciones que dibujan mi tabla y la tuya
 */
void dibujar_mi_nombre(Canvas canvas,        /* unused */
		       Xv_Window paint_window, /* unused */
		       Display *dpy,
		       Window xwin,
		       Xv_xrectlist *xrects ) /* unused */
{
   GC gc;
   
   gc = DefaultGC(dpy,DefaultScreen(dpy));
   XSetForeground(dpy,gc,colors[BLACK]);
   XDrawString(dpy,xwin,gc,60,25,"My board",8);
   
}
void dibujar_tu_nombre(Canvas canvas,        /* unused */
		       Xv_Window paint_window, /* unused */
		       Display *dpy,
		       Window xwin,
		       Xv_xrectlist *xrects ) /* unused */
{
   GC gc;
   
   gc = DefaultGC(dpy,DefaultScreen(dpy));
   XSetForeground(dpy,gc,colors[BLACK]);
   XDrawString(dpy,xwin,gc,50,25,"Enemy's board",13);
   
}



void dibujar_mi_tabla(Canvas canvas,        /* unused */
		      Xv_Window paint_window, /* unused */
		      Display *dpy,
		      Window xwin,
		      Xv_xrectlist *xrects ) /* unused */
{
   GC gc;
   int  i;
   gc = DefaultGC(dpy,DefaultScreen(dpy));
   XSetForeground(dpy,gc,colors[BLACK]);
   for(i=0;i<10;i++)
     {
	XDrawLine( dpy, xwin, gc, LARGO*i, 0, LARGO*i, LARGO*10);
	XDrawLine( dpy, xwin, gc, 0, ANCHO*i, ANCHO*10, ANCHO*i);
     }
   
}

void dibujar_tu_tabla(Canvas canvas,        /* unused */
		      Xv_Window paint_window, /* unused */
		      Display *dpy,
		      Window xwin,
		      Xv_xrectlist *xrects ) /* unused */
{
   GC gc;
   int i;
   gc = DefaultGC(dpy,DefaultScreen(dpy));
   XSetForeground(dpy,gc,colors[BLACK]);
   for(i=0;i<10;i++)
     {
	XDrawLine( dpy, xwin, gc, LARGO*i, 0, LARGO*i, LARGO*10);
	XDrawLine( dpy, xwin, gc, 0, ANCHO*i, ANCHO*10, ANCHO*i);
     }
   
}
/* 
 * Eventos de mi tabla y tu tabla
 */
/* mi tabla - eventos */
void event_mib( Xv_Window window, Event *event )
{
   char temptable[100];
   int x,y;
   
   
   switch( event_action(event) )
     {
	/* Boton Izquierdo */
      case ACTION_SELECT:
      case MS_LEFT:
	if(!event_is_down(event))
	  {
	     /* Es primer disparo y no estoy jugando */
	     if( (usuario.firsthit==0) && (usuario.play<2))
	       {
		  for(x=0;x<10;x++)
		    {
		       for(y=0;y<10;y++)
			 mitabla[x][y]=NOBARCO;
		    }
		  iwtable(temptable);
		  fillboard(temptable,0);
		  fillboard(temptable,1);
		  usuario.firsthit=1;
	       }
	     if( usuario.play >=3 )
	       {
		  ttyfill("You cant modify the board while playing!\n\r");
		  break;
		  /*							return; */
	       }
	     if( usuario.play ==2 )
	       {
		  ttyfill("The board is OK, so you cant modify it\n\r");
		  break;
		  /*							return; */
	       }
	     x = event_x(event) / ANCHO;
	     y = event_y(event) / LARGO;
	     mitabla[x][y]=BARCO;
	     pmicell(x,y,GREEN);
	     break;
	  }
	
	/* BOTON DERECHO */
      case ACTION_MENU:
      case MS_RIGHT:
	if(!event_is_down(event))
	  {
	     if( (usuario.firsthit==0) && (usuario.play<2))
	       {
		  for(x=0;x<10;x++)
		    {
		       for(y=0;y<10;y++)
			 mitabla[x][y]=NOBARCO;
		    }
		  iwtable(temptable);
		  fillboard(temptable,0);
		  fillboard(temptable,1);
		  usuario.firsthit=1;
	       }
	     if( usuario.play >=3 )
	       {
		  ttyfill("You cant modify the board while playing!\n\r");
		  break;
	       }
	     if( usuario.play ==2 )
	       {
		  ttyfill("The board is OK, so you cant modify it\n\r");
		  break;
	       }
	     
	     x = event_x(event) / ANCHO;
	     y = event_y(event) / LARGO;
	     mitabla[x][y]=NOBARCO;
	     pmicell(x,y,WHITE);
	     break;
	  }
      default:
	break;
     }
}

/* Tu tabla -eventos */
void event_tub( Xv_Window window, Event *event )
{
   int x;
   int y;
   char outbuf[MSGMAXLEN];
   char temptable[100];
   
   switch( event_action(event) )
     {
      case ACTION_SELECT:
      case MS_LEFT:
	if(!event_is_down(event))
	  {
	     x = event_x(event) / ANCHO;
	     y = event_y(event) / LARGO;
	     
	     if( (usuario.firsthit==0) && (usuario.play<2))
	       {
		  for(x=0;x<10;x++)
		    {
		       for(y=0;y<10;y++)
			 mitabla[x][y]=NOBARCO;
		    }
		  iwtable(temptable);
		  fillboard(temptable,0);
		  fillboard(temptable,1);
		  usuario.firsthit=1;
	       }
	     if(usuario.play<3)
	       {
		  ttyfill("You cant HIT now. First start a game\n\r");
		  break;
	       }
	     if(usuario.play==3)
	       {
		  ttyfill("It is not your turn!\n\r");
		  break;
	       }
	     usuario.play=3;
	     outbuf[0]=x;
	     outbuf[1]=y;
	     bnwrite(sock,outbuf,BNHIT,0,0,usuario.numjug);
	     bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
	  }
      default:
	break;
     }
}

/*
 *             FUNCIONES DE COMUNICACIONES
 */


static 
Notify_value proceso( )
{
   struct protocolo proto;
   char outbuf[MSGMAXLEN];
   char outbufi[MSGMAXLEN];
   char temptable[100];     /* v0.50 */
   int i;
   
   FD_SET(sock,&readfds);
   if(select(FD_SETSIZE,&readfds,NULL,NULL,&timeout)) 
     {
	/* == MAXLEN modified in 0.50 - Big bug ?*/
	if( read(sock,&proto,MAXLEN) == MAXLEN)
	  {
	     switch(proto.bnptip0) 
	       {
		case BNTST:
		  break;
		case BNWRI:
		  if(proto.bnpmsg[0]==1) 
		    {
		       xv_set(panelcit2,
			      PANEL_LABEL_STRING,"     Start     ",
			      NULL); 
		       ttyfill("The board is OK. Press START to start the game.\n\r");
		       usuario.play=2;
		    }
		  else if(proto.bnpmsg[0]==0) 
		    {
		       ttyfill("Check board: Bnserver reports an error.\n\r");
		       usuario.play=1;
		    }
		  break;
		case BNJUG:
		  usuario.numjug=proto.bnptip1;
		  sprintf(outbuf,"Connected to bnserver:%s",proto.bnpmsg);
		  foot_right(outbuf);
		  sprintf(outbuf,"Player %i",usuario.numjug);
		  foot_left(outbuf);
		  sprintf(outbuf,"Connected to bnserver:%s. You are player number:%i.\n\r",proto.bnpmsg,usuario.numjug);
		  ttyfill(outbuf);
		  
		  usuario.play = 1;
		  xv_set(panelcito,
			 PANEL_LABEL_STRING,"Disconnect",
			 NULL); 
		  xv_set(panelcit2,
			 PANEL_BUSY,FALSE,
			 NULL);
		  xv_set(panelcit4,
			 PANEL_BUSY,FALSE,
			 NULL);
		  xv_set(panelcit5,              /* status */
			 PANEL_BUSY,FALSE,
			 NULL);
		  
		  
		  bnwrite(sock,outbuf,BNVER,usuario.usrfrom,bnsup,usuario.numjug);
		  
		  break;
		case BNVER:
		  sprintf(outbuf,"bnserver version:%i.%i\n\r",proto.bnpmsg[0],proto.bnpmsg[1]);
		  ttyfill(outbuf);
		  break;
		case BNMSG:
		  strcpy(outbuf,proto.bnpmsg);
		  strcat(outbuf,"\n\r");
		  ttyfill(outbuf);
		  break;
		case BNALL:
		  ttyfill("bnserver is full. Using all the connections.Try later\n\r");
		  close(sock);
		  break;
		case BNWAI:
		  ttyfill("You cant start a game while people are playing.Try later\n\r");
		  break;
		case BNHIT:
		  if(usuario.play>=3) 
		    {
		       mitabla[(int)proto.bnpmsg[0]][(int)proto.bnpmsg[1]]=(int)proto.bnpmsg[2]; /* new on v0.44 */
		       if(usuario.hide==FALSE)
			 {
			    iwtable(temptable);
			    fillboard(temptable,0); /* v0.50 */
			 }
		    }
		  bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
		  break;
		case BNREA:
		  if(usuario.play>=3)
		    {
		       fillboard(proto.bnpmsg,1); /* v0.50 */
		       inteliclient(proto.bnpmsg);
		    }
		  
		  break;
		case BNSTR:
		  foot_right("Please,wait your turn");
		  ttyfill("The game starts now! Please,wait your turn.\n\r"); /* new on v0.32 */
		  ttyfill("At the bottom-right corner you can see if its your turn.\n\r");
		  sprintf(outbuf,"Available players:");
		  
		  for(i=0;i<MAXPLAYER;i++) 
		    {
		       if(proto.bnpmsg[i]>=2) 
			 {
			    sprintf(outbufi," %i",i+1);
			    strcat(outbuf,outbufi);
			    strcpy(usuario.names[i],&proto.bnpmsg[5+i*MAXNAMELEN]);
			    xv_set(mi[i],MENU_STRING,usuario.names[i],NULL);
			 }
		    }
		  strcat(outbuf,".\n\r");
		  ttyfill(outbuf);
		  
		  strcpy(outbuf,"Your board:");
		  strcat(outbuf,usuario.names[usuario.numjug-1]);
		  dname(outbuf,0);
		  foot_left(usuario.names[usuario.numjug-1]); /* new on v0.36 */
		  
		  if(usuario.play==2) 
		    {
		       usuario.play=3;
		       xv_set(panelcit2,
			      PANEL_BUSY,TRUE,
			      NULL);
		       xv_set(panelcit3,
			      PANEL_BUSY,FALSE,
			      NULL);
		    }
		  for(i=0;i<MAXPLAYER;i++) 
		    {          /* new on v0.36 */
		       if(usuario.numjug-1!=i) 
			 {
			    if(strcmp(usuario.names[i],"")!=0) {
			       usuario.usrfrom=i+1;
			       sprintf(outbuf,"%s's board",usuario.names[i]);
			       dname(outbuf,1);
			    }
			 }
		    }
		  break;
		case BNCON:
		  sprintf(outbuf,"Player %i is ready to play\n\r",proto.bnptip1);
		  ttyfill(outbuf);
		  break;
		case BNWHO:                          /* new on v0.32 */
		  sprintf(outbuf,"It's %s turn.",usuario.names[proto.bnptip1-1]);
		  foot_right(outbuf);
		  break;
		case BNTRN:
		  usuario.play=4;                    /* oh, it is my turn */
		  foot_right("It's my turn.");
		  break;
		case BNEXT:                          /* new on v0.36 */
		  i=proto.bnptip1-1;
		  usuario.names[i][0]=0;
		  xv_set(mi[i],MENU_STRING,usuario.names[i]);
		  strcpy(outbuf,proto.bnpmsg);
		  strcat(outbuf,"\n\r");
		  ttyfill(outbuf);
		  
		  for(i=0;i<MAXPLAYER;i++) 
		    {          /* new on v0.36 */
		       if(usuario.numjug-1!=i) 
			 {
			    if(strcmp(usuario.names[i],"")!=0) {
			       usuario.usrfrom=i+1;
			       sprintf(outbuf,"%s's board",usuario.names[i]);
														  }
			 }
		    }
		  if(usuario.play>=3) 
		    {
		       bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
		    }
		  break;
		case BNLOS:
									  usuario.play=1;                    /* oh, i lost the game */
		  for(i=0;i<MAXPLAYER;i++)
		    usuario.names[i][0]=0;
		  ttyfill("You were killed. To start again wait until the game is over.\n\r");
		  fillboard(looseboard,1); 
		  xv_set(panelcit2,
			 PANEL_LABEL_STRING,"Send Board",
			 PANEL_BUSY,FALSE,
											 NULL);
		  xv_set(panelcit3,
			 PANEL_BUSY,TRUE,
			 NULL);
		  usuario.firsthit=0;            /* new on v0.41 */
		  strcpy(outbuf,"My board");
		  dname(outbuf,0);
		  strcpy(outbuf,"Enemy's board");
		  dname(outbuf,1);
		  strcpy(outbuf,"Batalla Naval");
		  foot_right(outbuf);
		  strcpy(outbuf,"Game Over");
									  foot_left(outbuf);
		  break;
		case BNWIN:
		  usuario.play=1;                    /* oh, i lost the game */
		  for(i=0;i<MAXPLAYER;i++)
		    usuario.names[i][0]=0;
		  ttyfill("CONGRATULATIONS: You are the winner.\n\r");
		  fillboard(winboard,1); 
		  xv_set(panelcit2,
			 PANEL_LABEL_STRING,"Send Board",
			 PANEL_BUSY,FALSE,
			 NULL); 
		  xv_set(panelcit3,
			 PANEL_BUSY,TRUE,
			 NULL);
		  usuario.firsthit=0;            /* new on v0.41 */
		  strcpy(outbuf,"My board");
		  dname(outbuf,0);
		  strcpy(outbuf,"Enemy's board");
		  dname(outbuf,1);
		  strcpy(outbuf,"Batalla Naval");
		  foot_right(outbuf);
		  strcpy(outbuf,"Game Over");
		  foot_left(outbuf);
		  break;
		case BNSOL:
		  ttyfill("You are the only one who sent the board.Wait another player.\n\r");
		  break;
		case BNSTS:                    /* new on v0.43 */
		  for(i=0;i<MAXPLAYER;i++) 
		    {
		       sprintf(outbuf,"Player %i (%s), name='%s'\n\r"
			       ,i+1,usuario.status[(int)proto.bnpmsg[i]],&proto.bnpmsg[5+i*MAXNAMELEN]);
		       ttyfill(outbuf);
		    }
		  break;
		default:
		  sprintf(outbuf,"BNSERVER sending code %X.  Not available in this xbnclient.\n\r",proto.bnptip0);
		  ttyfill(outbuf);
									  break;
	       }
	  }
     }
   return NOTIFY_DONE;                    /* new on v0.30 */
}

int init_cliente( void ) 
{
   char outbuf[MSGMAXLEN];
   int i;
   
   if(usuario.play==0) {                  /* estado de un boton */
      /* Create socket */
      sock=socket(AF_INET,SOCK_STREAM,0);
      if(sock <0) 
	{
	   ttyfill("ERROR: creating stream socket\n\r");
	   return(1);
	}
      host_info = gethostbyname(usuario.server_name);
      if(host_info==NULL) 
	{
	   close(sock);           /* new on v0.38 */
	   ttyfill("ERROR: Unknown host.Please press 'Config'\n\r");
	   return(2);
	}
      server.sin_family=host_info->h_addrtype;
      memcpy( (char*) &server.sin_addr, host_info->h_addr,host_info->h_length);
      server.sin_port=htons(usuario.port);
      
      
      if(connect(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
	 close(sock);            /* new on v0.38 */
	 ttyfill("ERROR:Cant establish connection with server .Press 'Config'.\n\r");
						return(2);
      }
      
      timer.it_value.tv_usec = RAPID * 1000;
      timer.it_interval.tv_usec = RAPID * 1000;
      notify_set_itimer_func(frame, proceso,ITIMER_REAL, &timer, NULL);
      
      strncpy(outbuf,usuario.nombre,MAXNAMELEN);
      bnwrite(sock,outbuf,BNJUG,XCLI,BN_XVERH,BN_XVERL);       /* WHO AM I */
      return(0);
   }
   else if(usuario.play!=0) 
     {
	foot_right("(c) Ricardo Quesada");
	foot_left("Batalla Naval");
	xv_set(panelcito,
	       PANEL_LABEL_STRING,"  Connect  ",
	       NULL); 
	xv_set(panelcit2,
	       PANEL_LABEL_STRING,"Send Board",
	       PANEL_BUSY,TRUE,
	       NULL);
	xv_set(panelcit3,
	       PANEL_BUSY,TRUE,
	       NULL);
	xv_set(panelcit4,
	       PANEL_BUSY,TRUE,
	       NULL);
	xv_set(panelcit5,
	       PANEL_BUSY,TRUE,
	       NULL);
	
	usuario.firsthit=0;              /* new on v0.41 */
	for(i=0;i<MAXPLAYER;i++)
	  usuario.names[i][0]=0;
	usuario.play=0;
	bnwrite(sock,outbuf,BNEXT,0,0,usuario.numjug);
	close(sock);
     }
   return(0);
}
/*
 *                            FUNCIONES X  y otras...
 */

/* void nada( void ) - no hace nada */

void menuses( Menu menu,Menu_item menu_item) 
{
   char outbuf[MSGMAXLEN];
   int i;
   
   for(i=0;i<MAXPLAYER;i++) 
     {
	if(!strcmp((char *)xv_get(menu_item,MENU_STRING),usuario.names[i])) 
	  {
	     if(strcmp(usuario.names[i],"")!=0) 
	       {   
		  sprintf(fijo01,"%s's board",usuario.names[i]);
		  usuario.usrfrom=i+1;
		  dname(fijo01,1);
	       }
	  }
     }
   if(usuario.play>=3) 
     {
	bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
     }
}

void about( Frame item, Event *event ) 
{
   xv_set( subframe1, XV_SHOW, TRUE, NULL );
}

void send_msg( Frame item, Event *event ) 
{
   xv_set( subframe3, XV_SHOW, TRUE, NULL);
}

void status() 
{
   char tempbuf[100];
   if(usuario.play>=1) 
     {
	bnwrite(sock,tempbuf,BNSTS,0,0,usuario.numjug);
     }
}

void about_proc( Canvas c, Xv_window pw, Display *display, Window xid, Xv_xrectlist *xrects ) 
{
   int w,h;
   w = xv_get(pw,XV_WIDTH);
   h = xv_get(pw,XV_HEIGHT);
   
   XSetForeground(display,gc,colors[LBLUE]);
   XFillRectangle(display, xid, gc, 0, 0, w, h/3 );
   XSetForeground(display,gc,colors[WHITE]);
   XFillRectangle(display, xid, gc, 0, h/3, w, h/3*2 );
   XSetForeground(display,gc,colors[LBLUE]);
   XFillRectangle(display, xid, gc, 0, h/3*2, w,h );
   XSetForeground(display,gc,colors[YELLOW]);
   XFillArc(display, xid, gc, w/3 + w/3 * .15, h/3 + h/3 *.15, h/3 * .7, h/3 * .7, 0, 360*64 );
   XSetForeground(display,gc,colors[BLACK]);
   XDrawString(display,xid,gc,90, 40,"Batalla Naval v"BN_XVER,19);
   XDrawString(display,xid,gc,126, 80,"X client",8);
   XDrawString(display,xid,gc,30,100 ,"by Ricardo Quesada (rquesada@dc.uba.ar)",39);
   XDrawString(display,xid,gc,124,140,"bnserver",8);
   XDrawString(display,xid,gc,90,160,"by Ricardo Quesada",18);
   XDrawString(display,xid,gc,107,204,"ncurses client",14);
   XDrawString(display,xid,gc,90,224,"by Ricardo Quesada",18);
   XDrawString(display,xid,gc,50,260,"Hecho en Buenos Aires, Argentina",32);
}

void info( Frame item, Event *event ) 
{
   xv_set( subframe2, XV_SHOW, TRUE, NULL );
}
int server_port( Panel_item item, Event *event ) 
{
   usuario.port=(int)xv_get(item,PANEL_VALUE);
   return PANEL_NEXT;
}
int server_name( Panel_item item, Event *event ) 
{
   strcpy(usuario.server_name,(char *)xv_get(item,PANEL_VALUE));
   return PANEL_NEXT;
}
int your_name( Panel_item item, Event *event ) 
{
   strncpy(usuario.nombre,(char *)xv_get(item,PANEL_VALUE),MAXNAMELEN);
   return PANEL_NEXT;
}

int localecho( Panel_item item,unsigned value, Event *event ) 
{
   usuario.lecho=value;
   return XV_OK;
}
int to_player( Panel_item item, Event *event ) 
{
   usuario.towho=(int)xv_get(item,PANEL_VALUE);
   return PANEL_NEXT;
}
int to_message( Panel_item item, Event *event ) 
{
   strcpy(usuario.tomsg,(char *)xv_get(item,PANEL_VALUE));
   return PANEL_NEXT;
}
void real_message( void ) 
{         /* new on v0.34 - this send the message */
   if(usuario.play>=1)
     bnwrite(sock,usuario.tomsg,BNMSG,usuario.towho,usuario.lecho,usuario.numjug);
   else
     ttyfill("First establish a connection with 'Connect'.\n\r");
   return;
}

/*                            ALGORITMO
 *                  CHEQUEAR SI LOS BARCOS ESTAN BIEN
 */

/* points to next pos.return TRUE . FALSE in case there are no more ships */
/* this rutine uses global x,y */
int nextbarco() 
{
   if(x==9) 
     { 
	x=0; y++;
     }
   else x++;
   if( y==10 ) return FALSE;
   else return TRUE;
}    	
/* return TRUE if in (x,y) hay barco. else FALSE */
/* this rutine uses local x,y */
int haybarco( int x, int y) 
{
   if( (x < 0) || ( x > 9 ) || (y < 0) || (y >9 ) ) 
     return FALSE;
   if( mitabla[x][y]==1 ) 
     return TRUE;
   if( mitabla[x][y] == 0 ) 
     return FALSE;
   ttyfill("ABNORMAL ERROR in haybarco(): Please contact the author.\n\r");
   return FALSE;
}

/* return TRUE if board is OK .else return FALSE  */
int algoritmo( void )  
{
   int i,xx,yy,barcos,subarco;
   char temporal[100];
   
   for(i=0;i<4;i++) barquitos[i]=0;
   
   /* global x,y */
   x=0;y=0;
   barcos=0;
   
   for(;;) 
     {
	if(haybarco(x,y)==FALSE) 
	  {
	     if(nextbarco()==FALSE) 
	       {
		  if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) 
		    {
		       return TRUE;         /* tabla bien puesta */
		    }
		  else 
		    {
		       sprintf(temporal,"Check board: All numbers must be equal to 0. [4]=%i,[3]=%i,[2]=%i,[1]=%i\n\r",1-barquitos[3],2-barquitos[2],3-barquitos[1],4-barquitos[0]);
		       ttyfill(temporal);
		       return FALSE;
		    }
	       }
	  }
	else 
	  { 
	     if( haybarco(x,y)==TRUE )  
	       {
		  if( (haybarco(x,y-1)==FALSE) && (haybarco(x-1,y)==FALSE) ) 
		    {
		       /* this is the first time i check this */
		       subarco=1;
		       barcos++;
		       xx=x;yy=y;
		       
		       for(;;) 
			 {
			    if( (haybarco(x-1,y+1)==TRUE) || (haybarco(x+1,y+1)==TRUE) ) 
			      {
				 sprintf(temporal,"Check board: Collision in (x,y)=%i,%i\n\r",x+1,y+1);
				 ttyfill(temporal);
				 pmicell(x,y,RED);
				 return FALSE;
			      }
			    if( (haybarco(x+1,y)==FALSE) && (haybarco(x,y+1)==FALSE) ) 
			      {
				 x=xx;y=yy;          /* restauro pos */
				 barquitos[subarco-1]++; /* update cantidad de tama;os de barcos */
				 break;
			      }
			    else if( haybarco(x+1,y) == TRUE ) 
			      {
				 subarco++;
				 barcos++;
				 x++;
			      }          
			    else if( haybarco(x,y+1) == TRUE ) 
			      {
				 y++;
				 barcos++;
				 subarco++;
			      }
			    if( subarco > 4) 
			      {
				 sprintf(temporal,"Check board: Ship longer than 4 units in (x,y)=%i,%i\n\r",x+1,y+1);
				 ttyfill(temporal);
				 pmicell(x,y,RED);
				 return FALSE;
			      }
			 } /* for(;;) */
		    } /* if(haybarco(x,y-1)==FALSE) */
	       } /* if(haybarco(x,y)==TRUE) */
	     if(nextbarco()==FALSE) 
	       {
		  if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) 
		    {
												return TRUE;         /* tabla bien puesta */
		    }
		  else 
		    {
		       sprintf(temporal,"Check board: All numbers must be equal to 0. [4]=%i,[3]=%i,[2]=%i,[1]=%i\n\r",1-barquitos[3],2-barquitos[2],3-barquitos[1],4-barquitos[0]);
		       ttyfill(temporal);
		       return FALSE;
		    }
	       } /* if nextbarco()==FALSE) */
	  } /* else */
     } /* for(;;) */
} /* void algoritomo() */

/*
 *                     EMPEZAR LA PARTIDA
 */
int play( void ) 
{
   char outbuf[MSGMAXLEN];
   char temptable[100]; /* ver 0.50 */
   
   if(usuario.play==0) 
     {         /* estado del boton SEND BOARD */
	filtermiboard();
	iwtable(temptable);
	fillboard(temptable,0); /* v0.50 */
	if(algoritmo()==TRUE)  
	  {
	     ttyfill("First establish a connection.Press 'Connect'\n\r");
	  }
	return(FALSE);
     }
   else if( usuario.play==1)  
     {
	filtermiboard();
	iwtable(temptable);
	fillboard(temptable,0); /* v0.50 */
	
	if( algoritmo()==FALSE) 
	  {
	     return(FALSE);
	  }
	else if( algoritmo()==TRUE) 
	  {
	     iwtable( outbuf );
	     bnwrite(sock,outbuf,BNWRI,usuario.numjug,0,usuario.numjug);
	     return( TRUE );
	  } /* close algorito==ok */
     }
   else if(usuario.play==2) 
     {
	bnwrite(sock,outbuf,BNSTR,usuario.numjug,0,usuario.numjug);
     }
   else if(usuario.play>=3) 
     {
	ttyfill("Que muchacho?.This mean that you are playing!\n\r");
     }
   return(TRUE);
}
/*
 *                          QUIT
 */
void quit( ) 
{
   char outbuf[MSGMAXLEN];
   if(usuario.play!=0)
     {             /* if connected then kill child */
	bnwrite(sock,outbuf,BNEXT,0,0,usuario.numjug);
	close(sock);
     }
   xv_destroy_safe( frame );
}


/* int mitabla_fn( ) - eliminada en v0.50 */

/* Funcion que pone la H para que no vean mi tabla */
void mitabla_fn2( )
{
   char temptable[100];
   switch(usuario.hide) {
    case TRUE:
      usuario.hide=FALSE;
      iwtable(temptable);
      fillboard(temptable,0);
      break;
    case FALSE:
      usuario.hide=TRUE;
      fillboard(Hboard,0);
      break;
   }
}

/* int tutabla_fn( ) - eliminada en v0.50 */


/*
 *                               INICIALIZAR VARIABLES
 */
void init_datos( void ) 
{
   int i,j;
   
   usuario.play = 0;
   usuario.usrfrom=1;
   usuario.lecho=1;
   for(i=0;i<10;i++) 
     {          /* clean tabla */
	for(j=0;j<10;j++)
	  mitabla[i][j]=0;
     } 
   usuario.firsthit=0;
   for(i=0;i<MAXPLAYER;i++)
     usuario.names[i][0]=0;       /* clear all names */
   
   strncpy(usuario.status[DISCON],"not connected",16);
   strncpy(usuario.status[CONNEC],"  connected  ",16);
   strncpy(usuario.status[BOARD], "ready to play",16);
   strncpy(usuario.status[PLAY],  "   playing   ",16);
   strncpy(usuario.status[TURN],  "  playing *  ",16);
   usuario.hide=FALSE;
}

/*
 *                                 INICIALIZAR X
 */
void init_X( void ) 
{
   
   int i;
   
   /* crea los colores */
   cms = (Cms) xv_create( XV_NULL,CMS,
			 CMS_SIZE,8,
			 CMS_TYPE, XV_STATIC_CMS,
			 CMS_COLORS,cms_colors,
			 NULL );
   /* ventana principal */
   frame = (Frame) xv_create(XV_NULL, FRAME,
			     XV_HEIGHT,240,
			     XV_WIDTH,500,
			     FRAME_LABEL,"Batalla Naval X Client v"BN_XVER,
			     FRAME_SHOW_FOOTER,TRUE,
			     FRAME_LEFT_FOOTER,"Batalla Naval",
			     FRAME_RIGHT_FOOTER,"(c) Ricardo Quesada",
			     FRAME_SHOW_RESIZE_CORNER,FALSE,
			     NULL);
   
   
   /*
    * Todo lo referente a mi board
    */
   
   /* nombre de mi board */
   canvas_min = (Canvas) xv_create( frame,CANVAS,
				   XV_Y,0,
				   XV_X,0,
				   XV_HEIGHT,LARGO * 2 ,
				   XV_WIDTH,10 * ANCHO,
				   WIN_CMS,cms, 
				   CANVAS_X_PAINT_WINDOW,TRUE,
				   CANVAS_REPAINT_PROC, dibujar_mi_nombre,
				   NULL );
   
   canvas_mib = (Canvas) xv_create( frame,CANVAS,
				   WIN_BELOW,canvas_min,
				   XV_X,0,
				   XV_HEIGHT,10 * LARGO ,
				   XV_WIDTH,10 * ANCHO,
				   WIN_CMS,cms, 
				   CANVAS_X_PAINT_WINDOW,TRUE,
				   CANVAS_REPAINT_PROC, dibujar_mi_tabla,
				   NULL );
   xv_set(canvas_paint_window(canvas_mib),
	  WIN_EVENT_PROC, event_mib,
	  WIN_CONSUME_EVENTS,WIN_MOUSE_BUTTONS,NULL,
	  NULL);
   
   /* 
    * Todo lo referente a los botones principales: connect,about,config,etc.
    */
   panel = (Panel) xv_create( frame,PANEL,
			     XV_Y,0,
			     PANEL_LAYOUT,PANEL_VERTICAL,
			     XV_WIDTH, 100,
			     NULL );
   
   panelcito = (Panel) xv_create(panel,PANEL_BUTTON,
				 PANEL_LABEL_STRING," Connect  ",
				 PANEL_NOTIFY_PROC,init_cliente,
				 NULL);
   panelcit2 = (Panel) xv_create(panel,PANEL_BUTTON,
				 PANEL_BUSY,TRUE,
				 PANEL_LABEL_STRING,"Send Board",
				 PANEL_NOTIFY_PROC,play,
				 NULL);
   
   panelcit5 = (Panel) xv_create(panel,PANEL_BUTTON,
				 PANEL_LABEL_STRING,"    Status    ",
				 PANEL_NOTIFY_PROC,status,
				 PANEL_BUSY,TRUE,
				 NULL);
   
   for(i=0;i<MAXPLAYER;i++) 
     {
	mi[i] = (Menu_item) xv_create(XV_NULL,MENUITEM,
				      MENU_STRING,usuario.names[i],
				      MENU_NOTIFY_PROC,menuses,
				      NULL);
     }
   
   menu = (Menu) xv_create (XV_NULL, MENU,
			    MENU_GEN_PIN_WINDOW, frame, "Players",
			    NULL);
   for(i=0;i<MAXPLAYER;i++)
     xv_set(menu,MENU_APPEND_ITEM,mi[i],NULL);
   
   panelcit3 = (Panel) xv_create(panel, PANEL_BUTTON,
				 PANEL_BUSY,TRUE,
				 PANEL_LABEL_STRING," Players  ",
				 PANEL_NOTIFY_PROC,NULL,
				 PANEL_ITEM_MENU,menu,
				 NULL);
   panelcit4 = (Panel) xv_create(panel,PANEL_BUTTON,        /* new on v0.34 */
				 PANEL_BUSY,TRUE,
				 PANEL_LABEL_STRING," Send Msg  ",
				 PANEL_NOTIFY_PROC,send_msg,
				 NULL);
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"    Config   ",
	     PANEL_NOTIFY_PROC,info,
	     NULL); 
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"     About   ",
	     PANEL_NOTIFY_PROC,about,
	     NULL);
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"     Quit     ",
	     PANEL_NOTIFY_PROC,quit,
	     NULL);
   
   /*
    * Todo lo referente al board del enemigo 
    */
   /* donde va el nombre del board del enemigo */
   canvas_tun = (Canvas) xv_create( frame,CANVAS,
				   XV_Y,0,
				   XV_HEIGHT,LARGO * 2 ,
				   XV_WIDTH,10 * ANCHO,
				   WIN_CMS,cms, 
				   CANVAS_X_PAINT_WINDOW,TRUE,
				   CANVAS_REPAINT_PROC, dibujar_tu_nombre,
				   NULL );
   canvas_tub = (Canvas) xv_create( frame,CANVAS,
				   WIN_BELOW,canvas_tun,
				   XV_X,ANCHO*10+100,
				   XV_HEIGHT,10 * LARGO,
				   XV_WIDTH,10 * ANCHO,
				   CANVAS_X_PAINT_WINDOW,TRUE,
				   CANVAS_REPAINT_PROC, dibujar_tu_tabla,
				   NULL );
   xv_set(canvas_paint_window(canvas_tub),
	  WIN_EVENT_PROC, event_tub,
	  WIN_CONSUME_EVENTS,WIN_MOUSE_BUTTONS,NULL,
	  NULL);
   
   /* 
    * todo lo referente a about
    */
   subframe1=(Frame) xv_create(frame,FRAME,
			       XV_HEIGHT,300,
			       XV_WIDTH,300,
			       FRAME_SHOW_FOOTER,TRUE,
			       FRAME_LEFT_FOOTER,"Batalla Naval",
			       FRAME_RIGHT_FOOTER,"(c) Ricardo Quesada",
			       FRAME_LABEL, "About",
			       FRAME_SHOW_RESIZE_CORNER,FALSE,
			       NULL);
   canvas = (Canvas) xv_create( subframe1,CANVAS,
			       CANVAS_X_PAINT_WINDOW,TRUE,
			       XV_VISUAL_CLASS, PseudoColor,
			       WIN_CMS,cms, 
			       CANVAS_REPAINT_PROC,about_proc,
			       NULL );
   display = (Display *) xv_get( subframe1, XV_DISPLAY); 
   xid = (XID) xv_get(  canvas_paint_window( canvas ) ,XV_XID );
   gc = (GC) XCreateGC( display, xid, XV_NULL , &gc_val );
   colors = (unsigned long*) xv_get( canvas,WIN_X_COLOR_INDICES);
   
   /*
    * todo lo referente a Config
    */
   subframe2=(Frame) xv_create(frame,FRAME_CMD,
			       XV_HEIGHT,200,
			       XV_WIDTH,400,
			       FRAME_SHOW_FOOTER,TRUE,
			       FRAME_LEFT_FOOTER,"How to: After entering the name press ENTER, then press OK",
			       FRAME_LABEL, "Configuration",
			       NULL);
   panelinfo = (Panel) xv_get( subframe2,FRAME_CMD_PANEL);
   
   
   xv_create( panelinfo, PANEL_BUTTON,
	     XV_X,190,
	     XV_Y,150,
	     PANEL_LABEL_STRING,"OK",
	     PANEL_NOTIFY_PROC,NULL,
	     NULL); 
   xv_create( panelinfo, PANEL_TEXT,
	     XV_X,70,
	     XV_Y,45,
	     PANEL_VALUE,usuario.server_name,
	     PANEL_LABEL_STRING,"Host name:",
	     PANEL_LAYOUT,PANEL_HORIZONTAL,
	     PANEL_NOTIFY_PROC, server_name,
	     PANEL_CLIENT_DATA, frame,
	     PANEL_VALUE_DISPLAY_LENGTH,20,
	     NULL);
   xv_create( panelinfo, PANEL_NUMERIC_TEXT,
	     XV_X,70,
	     XV_Y,70,
	     PANEL_LABEL_STRING,"Server port:",
	     PANEL_VALUE,usuario.port,
	     PANEL_MAX_VALUE,65535,
	     PANEL_MIN_VALUE,0,
	     PANEL_NOTIFY_PROC, server_port,
	     PANEL_CLIENT_DATA, frame,
	     PANEL_LAYOUT,PANEL_HORIZONTAL,
	     NULL);
   xv_create( panelinfo, PANEL_TEXT,
	     XV_X,70,
	     XV_Y,95,
	     PANEL_VALUE,usuario.nombre,
	     PANEL_LABEL_STRING,"Your name:",
	     PANEL_LAYOUT,PANEL_HORIZONTAL,
	     PANEL_VALUE_DISPLAY_LENGTH,20,
	     PANEL_NOTIFY_PROC, your_name,
	     PANEL_CLIENT_DATA, frame,
	     NULL);
   xv_create(panelinfo, PANEL_CHOICE,
	     XV_X,160,
	     XV_Y,120,
	     PANEL_FEEDBACK,PANEL_MARKED,
	     PANEL_CHOOSE_ONE,FALSE,
	     PANEL_LABEL_STRING,"Hide board:",
	     PANEL_VALUE, usuario.hide,
	     PANEL_CHOICE_STRINGS,"ON", NULL,
	     PANEL_NOTIFY_PROC,mitabla_fn2,
	     PANEL_CLIENT_DATA,frame,
	     NULL);
   /*
    * todo lo referente a SEND MESSAGE
    */
   subframe3 = (Frame) xv_create(frame,FRAME_CMD,
				 XV_HEIGHT,135,
				 XV_WIDTH,420,
				 FRAME_SHOW_FOOTER,TRUE,
				 FRAME_LEFT_FOOTER,"How to: After entering the message press ENTER, then press OK",
				 FRAME_LABEL, "Send Message",
				 FRAME_CMD_PIN_STATE,TRUE,
				 NULL);
   panel3 = (Panel) xv_get( subframe3,FRAME_CMD_PANEL);
   
   xv_create( panel3, PANEL_TEXT,
	     XV_X,20,
	     XV_Y,20,
	     PANEL_VALUE,"",
	     PANEL_LABEL_STRING,"Message:",
	     PANEL_LAYOUT,PANEL_HORIZONTAL,
	     PANEL_NOTIFY_PROC, to_message,
	     PANEL_CLIENT_DATA, frame,
	     PANEL_VALUE_DISPLAY_LENGTH,60,
	     NULL);
   xv_create( panel3, PANEL_NUMERIC_TEXT,
	     XV_X,20,
	     XV_Y,45,
	     PANEL_LABEL_STRING,"To Player (0=broadcast):",
	     PANEL_MAX_VALUE,MAXPLAYER,
	     PANEL_MIN_VALUE,0,
	     PANEL_NOTIFY_PROC, to_player,
	     PANEL_CLIENT_DATA, frame,
	     PANEL_LAYOUT,PANEL_HORIZONTAL,
	     NULL);
   xv_create(panel3, PANEL_CHOICE,
	     XV_X,160,
	     XV_Y,70,
	     PANEL_FEEDBACK,PANEL_MARKED,
	     PANEL_CHOOSE_ONE,FALSE,
	     PANEL_LABEL_STRING,"Local Echo:",
	     PANEL_VALUE, usuario.lecho,
	     PANEL_CHOICE_STRINGS,"ON", NULL,
	     PANEL_NOTIFY_PROC,localecho,
	     PANEL_CLIENT_DATA,frame,
	     NULL);
   
   xv_create( panel3, PANEL_BUTTON,
	     XV_X,200,
	     XV_Y,100,
	     PANEL_LABEL_STRING,"OK",
	     PANEL_NOTIFY_PROC,real_message,
	     NULL); 
   
   /* 
    * Todo lo referente al icono 
    */
   icon = (Icon) xv_create(XV_NULL, ICON,
			   ICON_IMAGE, xv_create(XV_NULL, SERVER_IMAGE,
						 XV_WIDTH, baticono_width,
						 XV_HEIGHT, baticono_height,
						 SERVER_IMAGE_X_BITS, baticono_bits,
						 NULL),
			   NULL);
   xv_set(frame, FRAME_ICON, icon, NULL);
   
   /*
    * Crea el panel donde van a ir a parar todos los mensajes.
    */
   tty = (Tty) xv_create( frame,TTY,
			 XV_X,0,
			 WIN_BELOW,panel,
			 XV_HEIGHT,80,
			 XV_WIDTH,500,
			 TTY_ARGV,TTY_ARGV_DO_NOT_FORK,
			 NULL);
   
   /*
    * startup x, and run
    */
   window_fit( frame ); 
   
}

/*
 *                        main()
 */
void help() 
{
   printf("\n\nBatalla Naval XView client v"BN_XVER"  (c) 1995,96,97,98 Ricardo Quesada\n");
   printf("\nsyntax:");
   printf("\nxbnclient [-s host_name] [-p server_port] [-u username]");
   printf("\n\t-s host_name:   The name of the host where bnserver is running");
   printf("\n\t                default is: localhost");
   printf("\n\t-p server_port: The number of the port that bnserver is listening to");
   printf("\n\t                default is: 1995");
   printf("\n\t-u username:    Your name or alias");
   printf("\n\t                default is your login name");
   printf("\n\nSend bugs,doubts,fixes to rquesada@dc.uba.ar\n");
   exit(-1);
}

void main( int argc, char *argv[] )
{
   int i;
   xv_init( XV_INIT_ARGC_PTR_ARGV, &argc, argv, NULL);
   
   strcpy( usuario.server_name,"localhost");
   usuario.port=1995;     
   strncpy( usuario.nombre,getenv("LOGNAME"),MAXNAMELEN);
   
   for(i=1;i<argc;i++) 
     {
	if(argv[i][0]=='-') 
	  {
	     switch(argv[i][1]) 
	       {
		case 'x':
		  bnsup=argv[i][2];
		  break;
		case 's': case 'S':
		  if(argc>i+1) 
		    strncpy( usuario.server_name,argv[i+1],50);
		  else 
		    help();
		  break;
		case 'u': case 'U':
		  if(argc>i+1) 
		    strncpy( usuario.nombre,argv[i+1],MAXNAMELEN);
		  else 
		    help();
		  break;
		case 'p': case 'P':
		  if(argc>i+1) 
		    usuario.port=atoi(argv[i+1]);
		  else 
		    help();
		  break;
		case 'h': case 'H':
		case '?': case '-':
		default:
		  help();
		  break;
	       }
	  }
	else 
	  {
	     if(argv[i-1][0]!='-')
	       help();
	  }
     }
   
   
   init_datos();                  /* inicializa variables */
   init_X();              /* inicializa entorno y juego */
   ttyfill("Batalla Naval XView Client v"BN_XVER" (c) 1995,98 Ricardo Quesada.\n\r");
   ttyfill("rquesada@dc.uba.ar - 8 Mar 98 - Buenos Aires, Argentina.\n\r");
   ttyfill("To start fill your board with the ships then press 'Send Board'.\n\r");
   init_cliente();
   fillboard(Bboard,0);
   fillboard(Nboard,1); 
   xv_main_loop(frame);
}
