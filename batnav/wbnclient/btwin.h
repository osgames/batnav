#include <windows.h>
#include <winsock.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "protocol.h"

/*funciones de di�logo*/
BOOL CALLBACK DConectar(HWND,UINT,WPARAM,LPARAM);
BOOL CALLBACK DMensaje (HWND,UINT,WPARAM,LPARAM);
BOOL CALLBACK DMandtab (HWND,UINT,WPARAM,LPARAM);
/* controles de los di�logos */
#define IDC_SERV	   103
#define IDC_JUGADOR  101
#define IDC_PORT	   102

#define IDC_DESTINO	101
#define IDC_MENSAJE	102

/* estados */
#define 	BTW_NO_CONECTADO 		0x00
#define  BTW_BUSCANDO_SERVER 	0x01
#define  BTW_CONECTANDO_TCP	0x02
#define  BTW_CONECTANDO_BNP	0x03
#define  BTW_CONECTADO		   0x10
#define	BTW_TABLERO_MANDADO	0x11
#define  BTW_LISTO				0x20
#define  BTW_EMPEZANDO			0x21
#define  BTW_JUGANDO				0x30
#define	BTW_MI_TURNO			0x31

#define	BN_WVERH					0x0
#define	BN_WVERL					48 /* 0.3 */

#define 	ID_Accion				0 /* switch(estado)
								BTW_NO_CONECTADO: 	CONECTAR
								BTW_BUSCANDO_SERVER: IMPOSIBLE
								BTW_CONECTANDO_TCP:	IMPOSIBLE
								BTW_CONECTANDO_BNP:	IMPOSIBLE
								BTW_CONECTADO: 		MANDAR TABLERO
								BTW_TABLERO_MANDADO:	IMPOSIBLE
								BTW_LISTO: 				EMPEZAR
								BTW_JUGANDO: 			IMPOSIBLE
								BTW_GANE||BTW_PERDI: NUEVO JUEGO */
#define 	ID_Mensaje				1
#define 	ID_Salir					2
#define  ID_Txtmsj				3
#define  ID_basetab				4
#define  ID_tab					0x10
#define	ID_nom    				0x20
#define	ID_sta    				0x30

int bnwrite(SOCKET fd,char *buf,char tip0,char tip1,
														char tip2,char jugyo );

LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL tabProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL staProc(HWND,UINT,WPARAM,LPARAM);
void arrtablero(char *);

#define	WM_DNS		WM_USER+1
#define	WM_CONNECT	WM_USER+2
#define	WM_READ		WM_USER+3
#define	WM_TABLA		WM_USER+4

#define  B1				 4
#define  B2H_1			 5
#define	B2H_2			 6
#define	B3H_1			 7
#define	B3H_2			 8
#define	B3H_3			 9
#define	B4H_1			10
#define	B4H_2			11
#define	B4H_3			12
#define	B4H_4			13
#define  B2V_1			14
#define	B2V_2			15
#define	B3V_1			16
#define	B3V_2			17
#define	B3V_3			18
#define	B4V_1			19
#define	B4V_2			20
#define	B4V_3			21
#define	B4V_4			22

