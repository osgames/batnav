/*
 * Batalla Naval - Cliente para Win16
 * por Horacio Pe�a
 */

#include "btwin.h"

struct tagstatus {
		char	Bar4,Bar3,Bar2,Bar1;
		} ;
typedef struct tagstatus status;
typedef char 	tablero[100];
typedef char   nomjug[22];
void inline		Linea(HDC hdc,int orgx,int orgy,int desx,int desy) {MoveTo(hdc,orgx,orgy);LineTo(hdc,desx,desy);}
/* Variables globales*/
char 					szProgName[]="Batalla Naval para Win16";
char					tab 		 []="Control para tableros";
char					stat 		 []="Control para status";
WSADATA				Wsadata;
SOCKET				sock;
HINSTANCE 			hInstancia;
int		   		estado=BTW_NO_CONECTADO;
HWND					VentPrinc;
char 					servidor[200];
int					puerto;
nomjug				jugador;
char					tmp_gethost[MAXGETHOSTSTRUCT];
char 					Error[250];
char					jug;
struct protocolo	proto;
tablero				tableros[6];
nomjug				jugadores[5];
status				sta[5];

#pragma argsused
int PASCAL WinMain(HINSTANCE hInst,HINSTANCE hPreInst,
						 LPSTR lpszCmdLine,int nCmdShow)
{
  MSG  lpMsg;
  WNDCLASS wcApp;
  int tmp,tmp2;

  hInstancia=hInst;
  for(tmp2=0;tmp2<6;tmp2++)
		for(tmp=0;tmp<100;tmp++)
				tableros[tmp2][tmp]=0;
  for(tmp=0;tmp<5;tmp++)
		{sta[tmp].Bar4=-1;sta[tmp].Bar3=-1,sta[tmp].Bar2=-1,sta[tmp].Bar1=-1;}

  if (!hPreInst) {
		wcApp.lpszClassName=szProgName;
		wcApp.hInstance    =hInst;
		wcApp.lpfnWndProc  =WndProc;
		wcApp.hCursor      =LoadCursor(NULL,IDC_ARROW);
		wcApp.hIcon        =LoadIcon(hInst,"1");
		wcApp.lpszMenuName =NULL;
		wcApp.hbrBackground=GetStockObject(WHITE_BRUSH);
		wcApp.style        =CS_HREDRAW|CS_VREDRAW;
		wcApp.cbClsExtra   =0;
		wcApp.cbWndExtra   =0;
		if (!RegisterClass (&wcApp)) {
				sprintf(Error,"Error registrando clase %s",wcApp.lpszClassName);
				MessageBox(NULL,Error,szProgName,MB_ICONSTOP);
				return FALSE;
				}
		wcApp.lpszClassName=tab;
		wcApp.hInstance    =hInst;
		wcApp.lpfnWndProc  =tabProc;
		wcApp.hCursor      =LoadCursor(NULL,IDC_ARROW);
		wcApp.hIcon        =NULL;
		wcApp.lpszMenuName =NULL;
		wcApp.hbrBackground=GetStockObject(WHITE_BRUSH);
		wcApp.style        =CS_HREDRAW|CS_VREDRAW;
		wcApp.cbClsExtra   =0;
		wcApp.cbWndExtra   =sizeof(LONG)*2;
		if (!RegisterClass (&wcApp)) {
				sprintf(Error,"Error registrando %s",wcApp.lpszClassName);
				MessageBox(NULL,Error,szProgName,MB_ICONSTOP);
				return FALSE;
				}
		wcApp.lpszClassName=stat;
		wcApp.hInstance    =hInst;
		wcApp.lpfnWndProc  =staProc;
		wcApp.hCursor      =LoadCursor(NULL,IDC_ARROW);
		wcApp.hIcon        =NULL;
		wcApp.lpszMenuName =NULL;
		wcApp.hbrBackground=GetStockObject(WHITE_BRUSH);
		wcApp.style        =CS_HREDRAW|CS_VREDRAW;
		wcApp.cbClsExtra   =0;
		wcApp.cbWndExtra   =sizeof(LONG)*2;
		if (!RegisterClass (&wcApp)) {
				sprintf(Error,"Error registrando %s",wcApp.lpszClassName);
				MessageBox(NULL,Error,szProgName,MB_ICONSTOP);
				return FALSE;
				}
		}
  tmp=WSAStartup(0x101,&Wsadata);
  if(tmp!=0) {
		sprintf(Error,"Error en WSAStartup: %d",tmp);
		MessageBox(NULL,Error,szProgName,MB_ICONSTOP);
		return 0;
		}
  sock=0;
  sock=socket(PF_INET,SOCK_STREAM,0);
  if(sock==INVALID_SOCKET) {
		sprintf(Error,"Error en socket: %d",WSAGetLastError());
		MessageBox(NULL,Error,szProgName,MB_ICONSTOP);
		WSACleanup();
		return 0;
		}

  VentPrinc=CreateWindow(szProgName,szProgName,
						  WS_CAPTION|WS_CLIPCHILDREN|WS_MAXIMIZE|
						  WS_MINIMIZEBOX|WS_POPUPWINDOW|WS_VISIBLE,
						  0,0,640,480,
						  (HWND)NULL,(HMENU)NULL,
						  (HANDLE)hInstancia,(LPSTR)NULL);
  ShowWindow(VentPrinc,nCmdShow);
  UpdateWindow(VentPrinc);
  while (GetMessage(&lpMsg,NULL,NULL,NULL)) {
	 TranslateMessage(&lpMsg);
	 DispatchMessage(&lpMsg);
  }

  if(estado>=BTW_CONECTADO)
		bnwrite(sock,"",BNEXT,0,0,jug);
  closesocket(sock);
  WSACleanup();
  return(lpMsg.wParam);
}

LONG FAR PASCAL WndProc(HWND hWnd,UINT messg,
								WPARAM wParam,LPARAM lP)
	{
	DLGPROC			dlgproc;
	PAINTSTRUCT 	ps;
	HDC 				hdc;
	/* para conectar */
	u_long			addr;
	sockaddr_in		ca;
	int				i,j;

	switch (messg)
			{
			case WM_CREATE:
					CreateWindow("button","Conectar",WS_VISIBLE|BS_PUSHBUTTON|
							WS_CHILD, 40,420,160,35,hWnd,(HMENU)ID_Accion,
							hInstancia, NULL);
					CreateWindow("button","Enviar Mensaje",WS_VISIBLE|BS_PUSHBUTTON|
							WS_CHILD|WS_DISABLED,240,420,160,35,hWnd,
							(HMENU)ID_Mensaje, hInstancia, NULL);
					CreateWindow("button","Salir",WS_VISIBLE|BS_PUSHBUTTON|
							WS_CHILD,440,420,160,35,hWnd,(HMENU)ID_Salir,
							hInstancia, NULL);
					CreateWindow("static","No conectado",WS_VISIBLE|WS_BORDER|
							WS_CHILD,30,380,580,20,hWnd,(HMENU)ID_Txtmsj,
							hInstancia, NULL);

					CreateWindow(tab,"",WS_VISIBLE|WS_CHILD,
							 39, 24,161,161,hWnd,(HMENU)ID_tab+1,
							hInstancia, (void*) MAKELONG(1,ID_tab+1));
					CreateWindow("static","",WS_VISIBLE|WS_CHILD|SS_RIGHT,
							 39,  6,161, 17,hWnd,(HMENU)ID_nom+1,
							hInstancia, NULL);
					CreateWindow(tab,"",WS_VISIBLE|WS_CHILD,
							239, 24,161,161,hWnd,(HMENU)ID_tab+2,
							hInstancia, (void*) MAKELONG(2,ID_tab+2));
					CreateWindow("static","",WS_VISIBLE|WS_CHILD|SS_RIGHT,
							239,  6,161, 17,hWnd,(HMENU)ID_nom+2,
							hInstancia, NULL);
					CreateWindow(tab,"",WS_VISIBLE|WS_CHILD,
							440, 24,161,161,hWnd,(HMENU)ID_tab+3,
							hInstancia, (void*) MAKELONG(3,ID_tab+3));
					CreateWindow("static","",WS_VISIBLE|WS_CHILD|SS_RIGHT,
							440,  6,161, 17,hWnd,(HMENU)ID_nom+3,
							hInstancia, NULL);
					CreateWindow(tab,"",WS_VISIBLE|WS_CHILD,
							 39,209,161,161,hWnd,(HMENU)ID_tab+4,
							hInstancia, (void*) MAKELONG(4,ID_tab+4));
					CreateWindow("static","",WS_VISIBLE|WS_CHILD|SS_RIGHT,
							 39,191,161, 14,hWnd,(HMENU)ID_nom+4,
							hInstancia, NULL);
					CreateWindow(tab,"",WS_VISIBLE|WS_CHILD,
							239,209,161,161,hWnd,(HMENU)ID_tab+5,
							hInstancia, (void*) MAKELONG(5,ID_tab+5));
					CreateWindow("static","",WS_VISIBLE|WS_CHILD|SS_RIGHT,
							239,191,161, 14,hWnd,(HMENU)ID_nom+5,
							hInstancia, NULL);
					CreateWindow(stat,"",WS_VISIBLE|WS_CHILD,
							440,209,161,161,hWnd,(HMENU)ID_sta,
							hInstancia, (void*) MAKELONG(0,ID_sta));
					break;
			case WM_PAINT:
					hdc=BeginPaint(hWnd,&ps);
					MoveTo(hdc,  0,410);
					LineTo(hdc,640,410);
					ValidateRect(hWnd,NULL);
					EndPaint(hWnd,&ps);
					break;
			case WM_DESTROY:
					PostQuitMessage(0);
					break;
			case WM_COMMAND:
					switch( wParam ) {
							case ID_Accion:
									switch( estado ) {
											case BTW_NO_CONECTADO: /* Conectar */
													dlgproc= (DLGPROC) MakeProcInstance(
																		(FARPROC) DConectar,
																		hInstancia);
													if(DialogBox(hInstancia,"CONECTAR_A",
																		hWnd,dlgproc)) {
															EnableWindow(GetDlgItem(hWnd,ID_Accion),
																		FALSE);
															estado=BTW_BUSCANDO_SERVER;
															sprintf(Error,"Haciendo busqueda en DNS");
															SetDlgItemText(hWnd,ID_Txtmsj,Error);
															} /* if(Dialog) */
													break;
											case BTW_CONECTADO: /* Mandar tablero */
													dlgproc= (DLGPROC) MakeProcInstance(
																		(FARPROC) DMandtab,
																		hInstancia);
													if(DialogBox(hInstancia,"MANDARTAB",
																		hWnd,dlgproc)) {
															EnableWindow(GetDlgItem(hWnd,ID_Accion),
																		FALSE);
															estado=BTW_TABLERO_MANDADO;
															sprintf(Error,"Tablero mandado. Esperando respuesta.");
															SetDlgItemText(hWnd,ID_Txtmsj,Error);
															} /* if(Dialog) */
																												break;
											case BTW_LISTO:
													bnwrite(sock,"",BNSTR,0,0,jug);
													EnableWindow(GetDlgItem(hWnd,ID_Accion),
															FALSE);
													estado=BTW_EMPEZANDO;
													sprintf(Error,"Vamos a jugar.");
													SetDlgItemText(hWnd,ID_Txtmsj,Error);
													break;
											} /* switch(estado) */
									break; /* case ID_Accion */
							case ID_Mensaje:
									dlgproc= (DLGPROC) MakeProcInstance(
														(FARPROC) DMensaje,
														hInstancia);
									DialogBox(hInstancia,"MANDARMSJ",
														NULL,dlgproc);
									break;
							case ID_Salir:
									DestroyWindow(hWnd);
									break;
							case ID_tab+1:
							case ID_tab+2:
							case ID_tab+3:
							case ID_tab+4:
							case ID_tab+5:
									if(estado!=BTW_MI_TURNO) {
											if(estado==BTW_JUGANDO)
												sprintf(Error,"���Espera a tu turno!!!");
											else
												sprintf(Error,"Hay que estar jugando para disparar");
											SetDlgItemText(hWnd,ID_Txtmsj,Error);
											break;
											}
									if((wParam-ID_tab)==jug) {
											sprintf(Error,"���No te pod�s tirar a vos mismo!!!");
											SetDlgItemText(hWnd,ID_Txtmsj,Error);
											break;
											}
									Error[0]=lP%10;
									Error[1]=lP/10;
									estado=BTW_JUGANDO;
									if(bnwrite(sock,Error,BNHIT,0,0,jug)
													==SOCKET_ERROR) {
											MessageBox(NULL,"Error en BNWRITE",
															szProgName,MB_OK);
											break;
											}
									break;
					} /* switch(wParam) */
					break; /* case WM_COMMAND */
			case WM_DNS: /* gethostbyname */
					if(estado!=BTW_BUSCANDO_SERVER) {
							MessageBox(NULL,"Respuesta a gethostbyname fuera de tiempo",
											szProgName,MB_OK);
							break;
							}
					if(WSAGETASYNCERROR(lP)!=0) {
							MessageBox(NULL,"Error en respuesta a gethostbyname.",
											szProgName,MB_OK);
							break;
							}

					strcpy(servidor,((struct hostent *) tmp_gethost)->h_name);
					addr = (*(u_long FAR *) ((struct hostent *) tmp_gethost)->h_addr_list[0]);
					ca.sin_family			=	AF_INET;
					ca.sin_port				=	htons(puerto);
					ca.sin_addr.s_addr	=	addr;
					WSAAsyncSelect(sock,VentPrinc,WM_CONNECT,FD_CONNECT);

					connect(sock, (LPSOCKADDR) &ca, sizeof(sockaddr_in));

					estado=BTW_CONECTANDO_TCP;
					sprintf(Error,"Conectando a %s (TCP)",servidor);
					SetDlgItemText(hWnd,ID_Txtmsj,Error);
					break;
			case WM_CONNECT:
					if(estado!=BTW_CONECTANDO_TCP) {
							MessageBox(NULL,"Respuesta a connect fuera de tiempo",
											szProgName,MB_OK);
							break;
							}
					if(WSAGETSELECTERROR(lP)!=0) {
							MessageBox(NULL,"Error en respuesta a connect.",
											szProgName,MB_OK);
							break;
							}
					if(WSAGETSELECTEVENT(lP)!=FD_CONNECT) {
							MessageBox(NULL,"Error en algun WSASelect "
										"(esperando CONNECT, recibiendo otra cosa)",
											szProgName,MB_OK);
							break;
							}
					WSAAsyncSelect(sock,VentPrinc,WM_READ,FD_READ);

					if(bnwrite(sock,jugador,BNJUG,WCLI,BN_WVERH,BN_WVERL)
						==SOCKET_ERROR) {
							MessageBox(NULL,"Error en BNWRITE",
											szProgName,MB_OK);
							break;
							}

					estado=BTW_CONECTANDO_BNP;
					sprintf(Error,"Conectando a %s via BNP ",servidor);
					SetDlgItemText(hWnd,ID_Txtmsj,Error);
					break;
			 case WM_READ:
					if(WSAGETSELECTEVENT(lP)!=FD_READ){
							MessageBox(NULL,"Error en algun WSASelect "
										"(esperando READ, recibiendo otra cosa)",
											szProgName,MB_OK);
							break;
							}

					recv(sock, (char FAR *) &proto, MAXLEN, 0);
					if((proto.bnphead[0]!='B')||(proto.bnphead[1]!='N')||
								(proto.bnphead[2]!='P')){
							MessageBox(NULL,"Paquete no BNP",
											szProgName,MB_OK);
							break;
							}
					switch( proto.bnptip0 ) {
							case BNJUG:
									if(estado!=BTW_CONECTANDO_BNP) {
											MessageBox(NULL,"BNJUG fuera de tiempo",
																	szProgName,MB_OK);
											break;
											}
									jug=proto.bnptip1;
									strcpy(servidor,proto.bnpmsg);
									if(bnwrite(sock,"",BNVER,0,0,jug)
													==SOCKET_ERROR) {
											MessageBox(NULL,"Error en BNWRITE",
													szProgName,MB_OK);
											break;
											}
									estado=BTW_CONECTADO;
									sprintf(Error,"Conectado");
									SetDlgItemText(hWnd,ID_Txtmsj,Error);
									sprintf(Error,"Mandar tablero");
									SetDlgItemText(hWnd,ID_Accion,Error);
									EnableWindow(GetDlgItem(hWnd,ID_Accion),
														TRUE);
									EnableWindow(GetDlgItem(hWnd,ID_Mensaje),
														TRUE);
									break;
							case BNMSG:
									SetDlgItemText(hWnd,ID_Txtmsj,proto.bnpmsg);
									break;
							case BNVER:
									sprintf(Error,"Servidor %2d.%2d",proto.bnpmsg[0],
														proto.bnpmsg[1]);
									SetDlgItemText(hWnd,ID_Txtmsj,Error);
									break;
							case BNWRI:
									if(estado!=BTW_TABLERO_MANDADO) {
											MessageBox(NULL,"BNWRI fuera de tiempo",
																	szProgName,MB_OK);
											break;
											}
									if(proto.bnpmsg[0]) {
											estado=BTW_LISTO;
											sprintf(Error,"Tablero aceptado");
											SetDlgItemText(hWnd,ID_Txtmsj,Error);
											sprintf(Error,"Empezar juego");
											SetDlgItemText(hWnd,ID_Accion,Error);
											EnableWindow(GetDlgItem(hWnd,ID_Accion),
																TRUE);
											}
									else {
											estado=BTW_CONECTADO;
											sprintf(Error,"Tablero denegado: hay que "
												"hay que volver a enviarlo");
											SetDlgItemText(hWnd,ID_Txtmsj,Error);
											sprintf(Error,"Mandar tablero");
											SetDlgItemText(hWnd,ID_Accion,Error);
											EnableWindow(GetDlgItem(hWnd,ID_Accion),
																TRUE);
											}
									break;
							case BNALL:
									if(estado!=BTW_CONECTANDO_BNP) {
											MessageBox(NULL,"BNALL fuera de tiempo",
																	szProgName,MB_OK);
											break;
											}
									estado=BTW_NO_CONECTADO;
									sprintf(Error,"Mucha gente en el servidor. Pruebe despues");
									SetDlgItemText(hWnd,ID_Txtmsj,Error);
									EnableWindow(GetDlgItem(hWnd,ID_Accion),
														TRUE);
									break;
							case BNWAI:
									if(estado!=BTW_TABLERO_MANDADO) {
											MessageBox(NULL,"BNWAI fuera de tiempo",
																	szProgName,MB_OK);
											break;
											}
									estado=BTW_CONECTADO;
									sprintf(Error,"Tablero denegado. Estan jugando.");
									SetDlgItemText(hWnd,ID_Txtmsj,Error);
									EnableWindow(GetDlgItem(hWnd,ID_Accion),
														TRUE);
									break;
							case BNSOL:
									if(estado!=BTW_EMPEZANDO) {
											MessageBox(NULL,"BNSOL fuera de tiempo",
																	szProgName,MB_OK);
											break;
											}
									estado=BTW_LISTO;
									sprintf(Error,"No se puede jugar solo. Espere.");
									SetDlgItemText(hWnd,ID_Txtmsj,Error);
									EnableWindow(GetDlgItem(hWnd,ID_Accion),
														TRUE);
									break;
							case BNSTR:
									for(i=0,j=0;i<5;i++) {
											strcpy(jugadores[i],&proto.bnpmsg[5+i*MAXNAMELEN]);
											if(i==jug-1) strcat(jugadores[i],"(yo)");
											SetDlgItemText(hWnd,ID_nom+i+1,jugadores[i]);
											if(proto.bnpmsg[i]>=2) {
													j++;
													sta[i].Bar4=1;
													sta[i].Bar3=2;
													sta[i].Bar2=3;
													sta[i].Bar1=4;
													}
											}
									InvalidateRect(GetDlgItem(hWnd,ID_sta)						,NULL,FALSE);
									estado=BTW_JUGANDO;
									sprintf(Error,"���Empieza la batalla con %d barcos!!!",j*10);
									SetDlgItemText(hWnd,ID_Txtmsj,Error);
									EnableWindow(GetDlgItem(hWnd,ID_Accion),
														TRUE);
									break;
							case BNTRN:
									if(estado!=BTW_JUGANDO) {
											MessageBox(NULL,"BNTRN fuera de tiempo",
																	szProgName,MB_OK);
											break;
											}
									for(i=0;i<5;i++)
											if(bnwrite(sock,"",BNREA,i,0,jug)
															==SOCKET_ERROR) {
													MessageBox(NULL,"Error en BNWRITE",
															szProgName,MB_OK);
													break;
													}
									estado=BTW_MI_TURNO;
									sprintf(Error,"Me toca jugar.");
									SetDlgItemText(hWnd,ID_Txtmsj,Error);
									break;
							case BNWHO:
									if(estado!=BTW_JUGANDO) {
											MessageBox(NULL,"BNWHO fuera de tiempo",
																	szProgName,MB_OK);
											break;
											}
									for(i=0;i<5;i++)
											if(bnwrite(sock,"",BNREA,i,0,jug)
															==SOCKET_ERROR) {
													MessageBox(NULL,"Error en BNWRITE",
															szProgName,MB_OK);
													break;
													}
									sprintf(Error,"Juega %s.",jugadores[proto.bnptip1-1]);
									SetDlgItemText(hWnd,ID_Txtmsj,Error);
									break;
							case BNEXT:
									switch(proto.bnptip2) {
											case 0: /* el jugador se va (decisi�n
														* propia) */
														sprintf(Error,"%s se retir� del combate",
																	jugadores[proto.bnptip1-1]);
														SetDlgItemText(hWnd,ID_Txtmsj,Error);
														break;
											case 1: /* al jugador lo van ;) */
														sprintf(Error,"%s ha sido eliminado",
																jugadores[proto.bnptip1-1]);
														SetDlgItemText(hWnd,ID_Txtmsj,Error);
														break;
											case 2: /* se acaba el partido */
														estado=BTW_CONECTADO;
														sprintf(Error,"Mandar tablero");
														SetDlgItemText(hWnd,ID_Accion,Error);
														EnableWindow(GetDlgItem(hWnd,ID_Accion),
																		TRUE);
														sprintf(Error,"%s es el vencedor",
																jugadores[proto.bnptip1-1]);
														MessageBox(NULL,Error,szProgName,MB_OK);
														for(i=1;i<6;i++) {
																for(j=0;j<100;j++)
																		tableros[i][j]=0;
																InvalidateRect(GetDlgItem(hWnd,ID_tab+i),NULL,TRUE);
																}
														break;
											}
									break;
							case BNCON:
									sprintf(Error,"El jugador %d est� listo para jugar",proto.bnptip1);
									SetDlgItemText(hWnd,ID_Txtmsj,Error);
									break;
							case BNREA:
									if(!(estado==BTW_JUGANDO||estado==BTW_MI_TURNO)) {
											MessageBox(NULL,"BNREA fuera de tiempo",
																	szProgName,MB_OK);
											break;
											}
									memcpy(tableros[proto.bnptip1+1],proto.bnpmsg,100);
									arrtablero(tableros[proto.bnptip1+1]);
									sta[proto.bnptip1].Bar4=1;sta[proto.bnptip1].Bar3=2;sta[proto.bnptip1].Bar2=3;sta[proto.bnptip1].Bar1=4;
									for(i=0;i<100;i++)
											switch(tableros[proto.bnptip1+1][i]) {
													case B4H_1:case B4V_1:sta[proto.bnptip1].Bar4--;break;
													case B3H_1:case B3V_1:sta[proto.bnptip1].Bar3--;break;
													case B2H_1:case B2V_1:sta[proto.bnptip1].Bar2--;break;
													case B1:              sta[proto.bnptip1].Bar1--;break;
													}
									InvalidateRect(GetDlgItem(hWnd,ID_tab+proto.bnptip1+1),NULL,FALSE);
									InvalidateRect(GetDlgItem(hWnd,ID_sta)						,NULL,FALSE);
									break;
							case BNHIT:
									switch(proto.bnpmsg[2]) {
											case TOCADO: sprintf("%s nos peg� ;( (%d,%d)",
																	jugadores[proto.bnptip2],proto.bnpmsg[0],
																	proto.bnpmsg[1]);break;
											case AGUA:   sprintf("%s le err� :) (%d,%d)",
																	jugadores[proto.bnptip2],proto.bnpmsg[0],
																	proto.bnpmsg[1]);break;
											}
									break;
							case BNLOS:
									MessageBox(NULL,"Perdimos :(",szProgName,MB_OK);
									break;
							case BNWIN:
									estado=BTW_CONECTADO;
									sprintf(Error,"Mandar tablero");
									SetDlgItemText(hWnd,ID_Accion,Error);
									EnableWindow(GetDlgItem(hWnd,ID_Accion),
														TRUE);
									sprintf(Error,"���Ganamos!!!");
									SetDlgItemText(hWnd,ID_Txtmsj,Error);
									MessageBox(NULL,Error,szProgName,MB_OK);
									for(i=1;i<6;i++) {
										for(j=0;j<100;j++)
											tableros[i][j]=0;
										InvalidateRect(GetDlgItem(hWnd,ID_tab+i),NULL,TRUE);
										}
									break;
							default:
									sprintf(Error,"Mensaje extra�o %x",proto.bnptip0);
									SetDlgItemText(hWnd,ID_Txtmsj,Error);
									break;
							}
					break;
	 default:
		return(DefWindowProc(hWnd,messg,wParam,lP));
  } /* switch(messg) */
  return(0L);
}

#pragma argsused
BOOL CALLBACK DConectar(HWND hWnd,UINT msg,WPARAM wP,LPARAM lP)
	{
	char tmp[5];

	switch(msg) {
			case WM_INITDIALOG:
					break;
			case WM_COMMAND:
					switch(wP) {
							case IDOK: /* Lo �nico que hace es pedir
											  la direccion del server*/
									/* Primero toma (y valida) los datos
										del cuadro de di�logo */
									GetWindowText(GetDlgItem(hWnd,IDC_PORT   ), tmp     ,   5);
									puerto=atoi(tmp);
									if(!puerto) {
											MessageBox(NULL,"Error en n�mero de puerto",szProgName,MB_ICONSTOP);
											return 0;
											}
									GetWindowText(GetDlgItem(hWnd,IDC_SERV   ), servidor, 200);
									GetWindowText(GetDlgItem(hWnd,IDC_JUGADOR), jugador ,  17);

									WSAAsyncGetHostByName(VentPrinc, WM_DNS,
											servidor, tmp_gethost,
											MAXGETHOSTSTRUCT);
									EndDialog(hWnd, 1);
									break;
							case IDCANCEL:
									EndDialog(hWnd, 0);
									break;
							}
					break;
			}
	return (0);
	}

/* Funcion para comunicarse con el server. (riq) */
int bnwrite(SOCKET fd,char *buf,char tip0,char tip1,
												char tip2,char jugyo )
	{
	int i;

	struct protocolo proto;
	proto.bnptip0=tip0;
	proto.bnptip1=tip1;
	proto.bnptip2=tip2;
	proto.jugador=jugyo;
	for(i=0;i<MSGMAXLEN;i++)
	  proto.bnpmsg[i]=buf[i];
	strcpy(proto.bnphead,BNPHEAD);
	proto.bnpver=BNPVER;
	return( send(fd,(char FAR *)&proto,MAXLEN,0) ) ;
	}

#pragma argsused
BOOL CALLBACK DMensaje(HWND hWnd,UINT msg,WPARAM wP,LPARAM lP)
	{
	char Destino[2];

	switch(msg) {
			case WM_INITDIALOG:
					break;
			case WM_COMMAND:
					switch(wP) {
							case IDOK: /* Toma los datos y manda el BNMSG corresp.*/
									GetWindowText(GetDlgItem(hWnd,IDC_MENSAJE), Error  , 100);
									GetWindowText(GetDlgItem(hWnd,IDC_JUGADOR), Destino,   2);
									if(Destino[0]<'0'||Destino[0]>'5') {
											MessageBox(NULL,"Error en destinatario.",szProgName,MB_ICONSTOP);
											break;
											}
									if(bnwrite(sock,Error,BNMSG,Destino[0]-'0',1,jug)
													==SOCKET_ERROR) {
											MessageBox(NULL,"Error en BNWRITE",
													szProgName,MB_OK);
											break;
											}

									EndDialog(hWnd, 1);
									break;
							case IDCANCEL:
									EndDialog(hWnd, 0);
									break;
							}
					break;
			}
	return (0);
	}

#pragma argsused
BOOL CALLBACK DMandtab(HWND hWnd,UINT msg,WPARAM wP,LPARAM lP)
	{
	int   i,j;

	switch(msg) {
			case WM_INITDIALOG:
					CreateWindow(tab,"",WS_VISIBLE|WS_CHILD,
							109,22,161,161,hWnd,(HMENU)ID_tab,
							hInstancia, (void*) MAKELONG(0,ID_tab));
					for(i=0,j=0;i<100;i++)
							if(tableros[0][i]==BARCO) j++;
					EnableWindow(GetDlgItem(hWnd,IDOK),j==20);
					break;
			case WM_COMMAND:
					switch(wP) {
							case IDOK: /* Toma los datos y manda el BNMSG corresp.*/
									if(bnwrite(sock,tableros[0],BNWRI,0,0,jug)
													==SOCKET_ERROR) {
											MessageBox(NULL,"Error en BNWRITE",
													szProgName,MB_OK);
											break;
											}
									EndDialog(hWnd, 1);
									break;
							case IDCANCEL:
									EndDialog(hWnd, 0);
									break;
							case ID_tab:
									tableros[0][lP]=!tableros[0][lP];
									for(i=0,j=0;i<100;i++)
											if(tableros[0][i]==BARCO) j++;
									EnableWindow(GetDlgItem(hWnd,IDOK),j==20);
									InvalidateRect(GetDlgItem(hWnd,ID_tab),NULL,FALSE);
									break;
							}
					break;
			}
	return (0);
	}

LONG FAR PASCAL tabProc(HWND hWnd,UINT messg,
								WPARAM wParam,LPARAM lP)
	{
	PAINTSTRUCT 	ps;
	HDC 				hdc,memdc;
	int				i;
	HBITMAP			graficos[25];
	HWND				padre=(HWND)GetWindowLong(hWnd,0);
	int				tabla=LOWORD(GetWindowLong(hWnd,4)),
						id   =HIWORD(GetWindowLong(hWnd,4));

	switch (messg)
			{
			case WM_CREATE:
					SetWindowLong(hWnd,0,(LONG) ((CREATESTRUCT*)
							lP)->hwndParent);
					SetWindowLong(hWnd,4,(LONG) (LONG) ((CREATESTRUCT*)
							lP)->lpCreateParams);
					break;
			case WM_PAINT:
					for(i=0;i<24;i++)
							graficos[i]=LoadBitmap(hInstancia,MAKEINTRESOURCE(i+1));

					hdc=BeginPaint(hWnd,&ps);
					memdc=CreateCompatibleDC(hdc);
					ValidateRect(hWnd,NULL);
					for(i=0;i<11;i++) {
							MoveTo(hdc,  0,16*i);
							LineTo(hdc,161,16*i);
							}
					for(i=0;i<11;i++) {
							MoveTo(hdc,16*i,  0);
							LineTo(hdc,16*i,161);
							}
					for(i=0;i<100;i++) {
							SelectObject(memdc,graficos[tableros[tabla][i]+1]);
							BitBlt(hdc,(i%10)*16+1,(i/10)*16+1,15,15,
														memdc,0,0,SRCCOPY);
							}
					EndPaint(hWnd,&ps);
					DeleteDC(memdc);

					for(i=0;i<24;i++)
							DeleteObject(graficos[i]);

					break;
			case WM_LBUTTONDOWN:
					if(LOWORD(lP)>159||HIWORD(lP)>159)
							break;
					SendMessage(padre,WM_COMMAND,id,10*(HIWORD(lP)/16)
									+(LOWORD(lP)/16));
					break;
	 default:
		return(DefWindowProc(hWnd,messg,wParam,lP));
  } /* switch(messg) */
return(0L);
}

void arrtablero(char *tablero)
	{
	int i,j;

	/* primera fase: ubica los aguas */
	for(i=0;i<10;i++)
			for(j=0;j<10;j++) {
					switch(tablero[10*i+j]) {
							case TOCADO:
									if(i> 0&&j> 0) tablero[10*(i-1)+(j-1)]=AGUA;
									if(i> 0&&j< 9) tablero[10*(i-1)+(j+1)]=AGUA;
									if(i< 9&&j> 0) tablero[10*(i+1)+(j-1)]=AGUA;
									if(i< 9&&j< 9) tablero[10*(i+1)+(j+1)]=AGUA;
									break;
							case HUNDIDO:
									if(i> 0&&j> 0&&tablero[10*(i-1)+(j-1)]==NOBARCO)tablero[10*(i-1)+(j-1)]=AGUA;
									if(i> 0      &&tablero[10*(i-1)+(j  )]==NOBARCO)tablero[10*(i-1)+(j  )]=AGUA;
									if(i> 0&&j< 9&&tablero[10*(i-1)+(j+1)]==NOBARCO)tablero[10*(i-1)+(j+1)]=AGUA;
									if(      j> 0&&tablero[10*(i  )+(j-1)]==NOBARCO)tablero[10*(i  )+(j-1)]=AGUA;
									if(      j< 9&&tablero[10*(i  )+(j+1)]==NOBARCO)tablero[10*(i  )+(j+1)]=AGUA;
									if(i<10&&j> 0&&tablero[10*(i+1)+(j-1)]==NOBARCO)tablero[10*(i+1)+(j-1)]=AGUA;
									if(i<10      &&tablero[10*(i+1)+(j  )]==NOBARCO)tablero[10*(i+1)+(j  )]=AGUA;
									if(i<10&&j< 9&&tablero[10*(i+1)+(j+1)]==NOBARCO)tablero[10*(i+1)+(j+1)]=AGUA;
									break;
							}
					}
	/* segunda fase: transformar hundidos en barcos... otro d�a */
	for(i=0;i<10;i++)
			for(j=0;j<10;j++) {
					if(tablero[10*i+j]!=HUNDIDO) continue;
					if( ( (j==9)||(tablero[10*(i)+(j+1)]!=HUNDIDO) ) &&
						 (	(i==9)||(tablero[10*(i+1)+(j)]!=HUNDIDO) ) ) {tablero[10*i+j]=B1;continue;}
					if((j<9&&tablero[10*(i)+(j+1)]==HUNDIDO)) { /* Horizontal */
							if((j==8||tablero[10*(i)+(j+2)]!=HUNDIDO)){tablero[10*i+j]=B2H_1;tablero[10*(i)+(j+1)]=B2H_2;continue;}
							if((j==7||tablero[10*(i)+(j+3)]!=HUNDIDO)){tablero[10*i+j]=B3H_1;tablero[10*(i)+(j+1)]=B3H_2;tablero[10*(i)+(j+2)]=B3H_3;continue;}
							tablero[10*i+j]=B4H_1;tablero[10*(i)+(j+1)]=B4H_2;tablero[10*(i)+(j+2)]=B4H_3;tablero[10*(i)+(j+3)]=B4H_4;continue;
							}
					else { /* vertical */
							if((i==8||tablero[10*(i+2)+(j)]!=HUNDIDO)){tablero[10*i+j]=B2V_1;tablero[10*(i+1)+(j)]=B2V_2;continue;}
							if((i==7||tablero[10*(i+3)+(j)]!=HUNDIDO)){tablero[10*i+j]=B3V_1;tablero[10*(i+1)+(j)]=B3V_2;tablero[10*(i+2)+(j)]=B3V_3;continue;}
							tablero[10*i+j]=B4V_1;tablero[10*(i+1)+(j)]=B4V_2;tablero[10*(i+2)+(j)]=B4V_3;tablero[10*(i+3)+(j)]=B4V_4;continue;
							}
					}
	}

LONG FAR PASCAL staProc(HWND hWnd,UINT messg,
								WPARAM wParam,LPARAM lP)
	{
	PAINTSTRUCT 	ps;
	HDC 				hdc,memdc;
	RECT				r;
	int				i;
	HBITMAP			BARCO4,BARCO3,BARCO2,BARCO1;
	HWND				padre=(HWND)GetWindowLong(hWnd,0);
	int				id   =HIWORD(GetWindowLong(hWnd,4));

	switch (messg)
			{
			case WM_CREATE:
					SetWindowLong(hWnd,0,(LONG) ((CREATESTRUCT*)
							lP)->hwndParent);
					SetWindowLong(hWnd,4,(LONG) (LONG) ((CREATESTRUCT*)
							lP)->lpCreateParams);
					break;
			case WM_PAINT:
					BARCO4=LoadBitmap(hInstancia,"BARCO4");
					BARCO3=LoadBitmap(hInstancia,"BARCO3");
					BARCO2=LoadBitmap(hInstancia,"BARCO2");
					BARCO1=LoadBitmap(hInstancia,MAKEINTRESOURCE(B1+2));

					hdc=BeginPaint(hWnd,&ps);
					memdc=CreateCompatibleDC(hdc);
					ValidateRect(hWnd,NULL);
					SetTextColor(hdc,RGB(0,0,0));

					Linea(hdc,  0,  0,160,  0);
					Linea(hdc,  0, 16,160, 16);
					Linea(hdc,  0, 32,160, 32);
					Linea(hdc,  0, 48,160, 48);
					Linea(hdc,  0, 64,160, 64);
					Linea(hdc,  0, 80,160, 80);
					Linea(hdc,  0, 96,160, 96);
					Linea(hdc,  0,160,160,160);
					Linea(hdc,  0,  0,  0,160);
					Linea(hdc,160,  0,160,160);
					Linea(hdc, 80, 16, 80, 96);
					Linea(hdc, 96, 16, 96, 96);
					Linea(hdc,112, 16,112, 96);
					Linea(hdc,128, 16,128, 96);
					Linea(hdc,144, 16,144, 96);
					r.left=1;r.right=79;
					r.top=17;r.bottom=31;
					DrawText(hdc,"Jugador",7,&r,DT_LEFT);

					SelectObject(memdc,BARCO1);
					BitBlt(hdc,  1, 33, 15, 15,
								memdc,0,0,SRCCOPY);
					SelectObject(memdc,BARCO2);
					BitBlt(hdc,  1, 49, 31, 15,
								memdc,0,0,SRCCOPY);
					SelectObject(memdc,BARCO3);
					BitBlt(hdc,  1, 65, 47, 15,
								memdc,0,0,SRCCOPY);
					SelectObject(memdc,BARCO4);
					BitBlt(hdc,  1, 81, 63, 15,
								memdc,0,0,SRCCOPY);

					for(i=0;i<5;i++){
						SetTextColor(hdc,RGB(  0,  0,  0));
						r.left=(i+5)*16+1;r.right=r.left+15;
						sprintf(Error,"%c",i+'1');
						r.top=17;r.bottom=31;
						DrawText(hdc,Error,1,&r,DT_CENTER);

						sprintf(Error,"%c",sta[i].Bar1+'0');
						r.top=33;r.bottom=47;
						switch(sta[i].Bar1) {
								case -1: SetTextColor(hdc,RGB( 85, 85, 85));sprintf(Error,"%c",'0');break;
								case  0: SetTextColor(hdc,RGB(255,  0,  0));break;
								default: SetTextColor(hdc,RGB(  0,  0,255));break;
								}
						DrawText(hdc,Error,1,&r,DT_CENTER);
						sprintf(Error,"%c",sta[i].Bar2+'0');
						r.top=49;r.bottom=63;
						switch(sta[i].Bar2) {
								case -1: SetTextColor(hdc,RGB( 85, 85, 85));sprintf(Error,"%c",'0');break;
								case  0: SetTextColor(hdc,RGB(255,  0,  0));break;
								default: SetTextColor(hdc,RGB(  0,  0,255));break;
								}
						DrawText(hdc,Error,1,&r,DT_CENTER);
						sprintf(Error,"%c",sta[i].Bar3+'0');
						r.top=65;r.bottom=79;
						switch(sta[i].Bar3) {
								case -1: SetTextColor(hdc,RGB( 85, 85, 85));sprintf(Error,"%c",'0');break;
								case  0: SetTextColor(hdc,RGB(255,  0,  0));break;
								default: SetTextColor(hdc,RGB(  0,  0,255));break;
								}
						DrawText(hdc,Error,1,&r,DT_CENTER);
						sprintf(Error,"%c",sta[i].Bar4+'0');
						r.top=81;r.bottom=95;
						switch(sta[i].Bar4) {
								case -1: SetTextColor(hdc,RGB( 85, 85, 85));sprintf(Error,"%c",'0');break;
								case  0: SetTextColor(hdc,RGB(255,  0,  0));break;
								default: SetTextColor(hdc,RGB(  0,  0,255));break;
								}
						DrawText(hdc,Error,1,&r,DT_CENTER);
						}

					EndPaint(hWnd,&ps);
					DeleteDC(memdc);

					DeleteObject(BARCO1);
					DeleteObject(BARCO2);
					DeleteObject(BARCO3);
					DeleteObject(BARCO4);

					break;
			case WM_LBUTTONDOWN:
					if(LOWORD(lP)>159||HIWORD(lP)>159)
							break;
					SendMessage(padre,WM_COMMAND,id,10*(HIWORD(lP)/16)
									+(LOWORD(lP)/16));
					break;
	 default:
		return(DefWindowProc(hWnd,messg,wParam,lP));
  } /* switch(messg) */
return(0L);
}


