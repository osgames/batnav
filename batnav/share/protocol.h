/******************************************
 * Archivo que pertenece al batalla naval *
 *                                        * 
 *    definicion del protocolo v0.11      *
 *                                        *
 ******************************************/

#ifndef __BN_PROT__
# define __BN_PROT__
struct protocolo  {
		char bnphead[3];                       /* bnp head */
		char bnpver;                           /* protocol version */
		char jugador;                          /* nro de cliente */
		char bnptip0;                          /* token */
		char bnptip1;                          /* value de token */
		char bnptip2;                          /* user o root */
		char bnpmsg[100];                      /* any message o table */
};


# define MSGMAXLEN 100
# define MAXNAMELEN 16
# define MAXLEN sizeof(struct protocolo)
# define HEADLEN MAXLEN - MSGMAXLEN

# define BNPHEAD "BNP"
# define BNPVER   0x11

# define DISCON   0x00         /* not connected */
# define CONNEC   0x01         /* yes connected */
# define BOARD    0x02         /* send board */
# define PLAY     0x03         /* playing */
# define TURN     0x04         /* his turn */
# define PERDIO   0x05         /* perdio (sirve para que clientes como
                                * el btwin sigan monitoreando el juego
				* luego de perder */

# define AGUA   -1
# define NOBARCO 0
# define BARCO   1
# define TOCADO  2
# define HUNDIDO 3

# define XCLI     0x10         /* playing with a X client ( Xview )*/
# define GCLI     0x11         /* playing with a X client ( GTK ) */
# define NCLI     0x20         /* playing with a ncurses client */
# define WCLI     0x30         /* jugando con un cliente Win16 */

# define BNUSR    0x38
# define BNSUP    0x39          

# define BNMSG    0x41          /* es un msg */               

# define BNEXT    0x43          /* si lo manda el server: que se vaya el cli */
# define BNJUG    0x44          /* te dice que jugador sos */

# define BNALL    0x46          /* sorry, pero ya hay muchos logueados */
# define BNWAI    0x47          /* Wait, people are already playing */

# define BNREA    0x57          /* read board */
# define BNWRI    0x58          /* send board */
# define BNHIT    0x59          /* fire at (x,y) */
# define BNCON    0x5A          /* Player x is connected */
# define BNSOL    0x5B          /* You cant play alone */
# define BNSTR    0x5C          /* Start the game */
# define BNTRN    0x5D          /* Your turn */
# define BNLOS    0x5E          /* you lost */
# define BNWIN    0x5F          /* you win */
# define BNVER    0x60          /* version */
# define BNTST    0x61          /* this is for test only */
# define BNWHO    0x62          /* whose turn ? */
# define BNSTS    0x63          /* status of players */

#endif __BN_PROT__
