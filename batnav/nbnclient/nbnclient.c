/*
 * Batalla Naval (c) 1995,96,97,98 Ricardo Quesada
 * (rquesada@dc.uba.ar)
 * 
 * Cliente ncurses
 * version: 0.60
 *  
 * Varios bugs resueltos por:
 * Horacio Pe�a ( horape@balug.org.ar )
 * 
 */

/*
 *                includes
 */

#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include "../share/protocol.h"
#include "../share/tablas.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h> 
#include <syslog.h>
#include <errno.h>
#include <string.h>

/*
 *                DEFINES
 */ 
#define BN_NVER "0.60"
#define BN_NVERH 0
#define BN_NVERL 60

#define MAXPLAYER 5

#define  WHITE  0
#define  BLACK  1
#define  BLUE   2
#define  GREEN  3
#define  GOOD   4
#define  VGOOD  5
#define  RED    6
#define  NORMAL 7
#define  BLUCKY 8
#define  LUCKY  9


#define  MITABLA 0
#define  BOTONES 1
#define  TUTABLA 2
#define  SENDMSG 3
#define  THECONF 4

#define  MAXLUGARC 6                 /* fucking cantidad de botones */

#define RAPID 25                     /* similar to x client */

void globaltab(void);
/*
 *                VARIABLES GLOBALES
 */ 
int    sock;
struct sockaddr_in server;
struct hostent *host_info;
char   mitabla[10][10];          /* mi tabla de barcos */
char   tutabla[10][10];          /* tabla para disparar */
char   barquitos[4];             /* tama;os de los barcos 1 de 4, 2 de 3, etc. */
short  int x,y;                  /* punteros de la tabla mia */
char   bnsup;                    /* usado por bnwrite */

struct usuario 
{
   char server_name[50];             /* server name */
   int  port;                        /* server port */
   char nombre[MAXNAMELEN];          /* user name */
   char names[MAXPLAYER][MAXNAMELEN]; /* other players's name */
   
   int firsthit;                     /* para borrar BN */
   int play;                         /* already playing ? */
   int numjug;                       /* number of player */
   int usrfrom;                      /* player who is owner of enemy window */
   
   char tomsg[MSGMAXLEN];            /* usuadas para mandar mensajes */
   int  towho;
   int  lecho;                       /* local echo */
   
   char status[5][16];
   int  hide;                        /* hide my board new on v0.44 */
} usuario;

/* referente a ncurses */
WINDOW *miboard;                       /* ventana de mis barcos */
WINDOW *tuboard;                       /* ventana de tus barcos */
WINDOW *msgboard;                      /* ventana de los mensajes */
WINDOW *title;
WINDOW *bconnect;
WINDOW *bstart;
WINDOW *bmsg;
WINDOW *bconfig;
WINDOW *babout;
WINDOW *bquit;
WINDOW *bstatus;                        /* new on v0.43 */
WINDOW *wright;
WINDOW *wleft;
WINDOW *myname;
WINDOW *yourname;
WINDOW *about;
WINDOW *sendmesg;
WINDOW *config;

struct ncur 
{
   short int lugarg;              /* lugar grande 0 (izq) 1 (medio) 2(derecha) */
   short int lugarc;              /* lugar chico */
   short int quit;
   short int x,y;
   short int xt,yt;
   char minombre[24];
   char tunombre[24];
   char lefty[40];
   char righty[40];
   
   int  sendx;                    /* coordenadas */
   char sendmsg[MSGMAXLEN];              /* mensaje */
   int  sendto;                   /* a quien */
   int  sendle;                   /* local echo */
   int  sendpos;                  /* internal position */
   
   int  conf[3];                   /* coordenadas del config */
   char confc[3][60];              /* guardar la info */
   int  confpos;
   
   int  flag1;                     /* chequea posible bug */
   int  color;                     /* terminal color new on v0.41 */
} ncur;

char outbuftemp[MSGMAXLEN];

fd_set readfds;                           /* used by proceso() */
#ifndef __linux
struct timeval tout=  { 0,250000 };
#else
struct timeval tout;
#endif

/* 
 *                           FUNCIONES
 */

/* 
 * varias funciones de manejo de pantalla
 */

void wattroncm(WINDOW *win, int attrs) 
{
   if(ncur.color==TRUE) 
     wattrset(win,attrs);
   else 
     {
	switch(attrs) 
	  {
	   case COLOR_PAIR(WHITE):
	   case COLOR_PAIR(BLUCKY):
	   case COLOR_PAIR(GOOD):
	     attrs=A_REVERSE;
	     break;
	   case COLOR_PAIR(NORMAL):
	   case COLOR_PAIR(BLACK):
	   case COLOR_PAIR(VGOOD):
	     attrs=A_BOLD;
	     break;
	   case COLOR_PAIR(RED):
	   case COLOR_PAIR(BLUE):
	   case COLOR_PAIR(GREEN):
	   case COLOR_PAIR(LUCKY):
	   default:
	     attrs=A_NORMAL;
	     break;
	  }   /* switch */
	wattrset(win,attrs);
     }      /* else */
}

void waddchcm(WINDOW *win,chtype ch,int attrs) 
{
   if(ncur.color==TRUE)  
     {
	waddch(win,ch);
	waddch(win,ch);
     }
   else {
      switch(attrs) 
	{        /* NOTA: QUE KILOMBO HICE CON LOS COLORES, TODOS CAMBIADOS */
	 case COLOR_PAIR(GREEN):       /* barco */
	   waddch(win,ACS_BOARD);
	   waddch(win,ACS_BOARD);
	   break;
	 case COLOR_PAIR(BLUE):      /* agua */
	   waddch(win,'.');
	   waddch(win,'.');
	   break;
	 case COLOR_PAIR(RED):        /* tocado */
	   waddch(win,'x');
	   waddch(win,'x');
	   break;
	 case COLOR_PAIR(BLACK):      /* hundido */
	   waddch(win,'*');
	   waddch(win,'*');
	   break;
	}   /* switch */
   }      /* else */
}

/* 2 funciones para escribir mensajes abajo */
void foot_right( char msg[] ) 
{ /* this function is buggy */
   int i,n;
   
   for(i=0;i<40;i++) 
     ncur.righty[i]=' ';
   n=40-strlen(msg);
   for(i=n;i<40;i++)
     ncur.righty[i]=msg[i-n];
   ncur.righty[40]=0;
   mvwprintw(wright,0,0,"%s",ncur.righty);
   wrefresh(wright);
}
void foot_left( char msg[] ) 
{
   int i,n;
   
   for(i=0;i<40;i++) 
     ncur.lefty[i]=' ';
   n=strlen(msg);
   for(i=0;i<n;i++)
     ncur.lefty[i]=msg[i];
   ncur.lefty[40]=0;
   mvwprintw(wleft,0,0,"%s",ncur.lefty);
   wrefresh(wleft);
}
void yname( void ) 
{
   char ch[23]="                     ";
   int n,i,j;
   n=strlen(ncur.tunombre)/2;
   i=10-n;
   for(j=0;j<strlen(ncur.tunombre);j++)
     ch[i+j]=ncur.tunombre[j];
   if(ncur.lugarg==TUTABLA)
     wattroncm(yourname,COLOR_PAIR(VGOOD));
   else
     wattroncm(yourname,COLOR_PAIR(GOOD));
   mvwprintw(yourname,0,0,"%s",ch);
   wrefresh(yourname);
}
void mname( void ) 
{
   char ch[23]="                     ";
   int n,i,j;
   n=strlen(ncur.minombre)/2;
   i=10-n;
   for(j=0;j<strlen(ncur.minombre);j++)
     ch[i+j]=ncur.minombre[j];
   if(ncur.lugarg==MITABLA)
     wattroncm(myname,COLOR_PAIR(VGOOD));
   else
     wattroncm(myname,COLOR_PAIR(GOOD));
   mvwprintw(myname,0,0,"%s",ch);
   wrefresh(myname);
}
void pmicell(int x,int y,int color) 
{
   if(x<10 && x>=0 && y>=0 && y<10)
     {
	if(color==WHITE) 
	  {
	     wattroncm(miboard,COLOR_PAIR(GOOD));
	     wmove(miboard,y+1,x*2+1);
	     waddch(miboard,' ');
	     waddch(miboard,ACS_VLINE);
	     wrefresh(miboard);
	  }
	else  
	  {
	     wattroncm(miboard,COLOR_PAIR(color));
	     wmove(miboard,y+1,x*2+1);
	     waddchcm(miboard,ACS_BOARD,COLOR_PAIR(color));
	     wrefresh(miboard);
	  }
     }
}

void ptucell(int x,int y,int color) 
{
   if(x<10 && x>=0 && y>=0 && y<10)
     {
	if(color==WHITE) 
	  {
	     wattroncm(tuboard,COLOR_PAIR(GOOD));
	     wmove(tuboard,y+1,x*2+1);
	     waddch(tuboard,' ');
	     waddch(tuboard,ACS_VLINE);
	     wrefresh(tuboard);
	  }
	else  
	  {
	     wattroncm(tuboard,COLOR_PAIR(color));
	     wmove(tuboard,y+1,x*2+1);
	     waddchcm(tuboard,ACS_BOARD,COLOR_PAIR(color));
	     wrefresh(tuboard);
	  }
     }
}
/*
 * Cliente inteligente - v0.50
 */
void inteliclient( char *table)
{
   int i,x,y;
   
   x=0;
   y=0;
   
   for(i=0;i<100;i++)
     {
	switch(table[i])
	  {
	   case HUNDIDO:
	   case TOCADO:
	     ptucell(x-1,y-1,BLUE);
	     ptucell(x-1,y+1,BLUE);
	     ptucell(x+1,y-1,BLUE);							
	     ptucell(x+1,y+1,BLUE);
	     break;
	   case NOBARCO:
	     if(x<9)
	       {
		  if(table[i+1]==HUNDIDO)
		    ptucell(x,y,BLUE);
	       }
	     if(x>0)
	       {
		  if(table[i-1]==HUNDIDO)
		    ptucell(x,y,BLUE);
	       }
	     if(y<9)
	       {
		  if(table[i+10]==HUNDIDO)
		    ptucell(x,y,BLUE);
	       }
	     if(y>0)
	       {
		  if(table[i-10]==HUNDIDO)
		    ptucell(x,y,BLUE);
	       }							
	   default:
	     break;
	  }
	x++;
	if(x==10)
	  {
	     x=0;
	     y++;
	  }
     }
}

void iwtable( char *dest) 
{
   int i,x,y;
   x=0;y=0;
   for(i=0;i<100;i++) 
     {
	dest[i]=mitabla[x][y];
	x++;
	if(x>=10) 
	  {
	     x=0;
	     y++;
	  }
     }
}

void repaintmi( void )  
{
   int x,y;
   for(x=0;x<10;x++) 
     {
	for(y=0;y<10;y++)  
	  {
	     switch(mitabla[x][y]) 
	       {
		case BARCO:
		  pmicell(x,y,GREEN);
		  break;
		case NOBARCO:
		  pmicell(x,y,WHITE);
		  break;
		case AGUA:
		  pmicell(x,y,BLUE);
		  break;
		case TOCADO:
		  pmicell(x,y,RED);
		  break;
		case HUNDIDO:
		  pmicell(x,y,BLACK);
		  break;
	       }
	  }
     }
}

void repainttu( char outbuf[] )  
{
   int x,y,i;
   for(x=0,y=0,i=0;i<MSGMAXLEN;i++) 
     {
	if( outbuf[i]==TOCADO)              /* tocado */
	  ptucell(x,y,RED);
	else if(outbuf[i]==HUNDIDO)          /* hundido */
	  ptucell(x,y,BLACK);
	else if(outbuf[i]==AGUA)         /* agua */
	  ptucell(x,y,BLUE);
	else if(outbuf[i]==NOBARCO)          /* nada */
	  ptucell(x,y,WHITE);
	else if(outbuf[i]==BARCO)          /* barco */
	  ptucell(x,y,GREEN);
	x++;
	if(x>=10) 
	  {
	     x=0;y++;
	  }
     }
}

void fillenemyt( char filltable[] ,int a) 
{
   int i,x,y;
   x=y=0;
   for(i=0;i<100;i++) 
     {
	if(a==0)
	  pmicell(x,y,filltable[i]);
	else if(a==1)ptucell(x,y,filltable[i]);
	x++;
	if(x==10) 
	  {
	     x=0;y++;
	  }
     }
}
void fill0( int b,int a ) 
{
   int i;
   int x,y;
   x=y=0;
   
   for(i=0;i<100;i++) 
     {
	if(a==0)
	  pmicell(x,y,b);
	else if(a==1)
	  ptucell(x,y,b);
	x++;
	if(x==10) 
	  {
	     x=0;y++;
	  }
     }
}

void filtermiboard()  
{
   int x,y;
   
   for(x=0;x<10;x++) 
     {
	for(y=0;y<10;y++) 
	  {
	     if(mitabla[x][y]>=BARCO) 
	       mitabla[x][y]=BARCO;
	     else 
	       {
		  if(mitabla[x][y]<=NOBARCO) 
		    mitabla[x][y]=NOBARCO;
	       }
	  }
     }
}

/*
 *             FUNCIONES DE COMUNICACIONES
 */

size_t bnwrite(int fd,char *buf,char tip0,char tip1,char tip2,char jugyo ) 
{
   int i;
   
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   proto.bnptip2=tip2;
   proto.jugador=jugyo;
   for(i=0;i<MSGMAXLEN;i++)
     proto.bnpmsg[i]=buf[i];
   strcpy(proto.bnphead,BNPHEAD);
   proto.bnpver=BNPVER;
   return( write(fd,&proto,MAXLEN) ) ;
}

/*                            ALGORITMO
 *                  CHEQUEAR SI LOS BARCOS ESTAN BIEN
 */

/* points to next pos.return TRUE . FALSE in case there are no more ships */
/* this rutine uses global x,y */
int nextbarco() 
{
   if(x==9) 
     { 
	x=0;
	y++;
     }
   else 
     x++;
   if( y==10 ) 
     return FALSE;
   else 
     return TRUE;
}    	
/* return TRUE if in (x,y) hay barco. else FALSE */
/* this rutine uses local x,y */
int haybarco( int x, int y) 
{
   if( (x < 0) || ( x > 9 ) || (y < 0) || (y >9 ) ) 
     return FALSE;
   if( mitabla[x][y]>=1 )       /* new on v0.45 */
     return TRUE;
   if( mitabla[x][y] == 0 ) 
     return FALSE;
   return TRUE;
}

/* return TRUE if board is OK .else return FALSE  */
int algoritmo( void )  
{
   int i,xx,yy,barcos,subarco;
   
   for(i=0;i<4;i++) 
     barquitos[i]=0;
   
   /* global x,y */
   x=0;
   y=0;
   barcos=0;
   
   for(;;) 
     {
	if(haybarco(x,y)==FALSE) 
	  {
	     if(nextbarco()==FALSE) 
	       {
		  if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
		     return TRUE;         /* tabla bien puesta */
		  }
		  else 
		    {
		       wprintw(msgboard,"\nCheck board: All numbers must be equal to 0. [4]=%i,[3]=%i,[2]=%i,[1]=%i",1-barquitos[3],2-barquitos[2],3-barquitos[1],4-barquitos[0]);
		       return FALSE;
		    }
	       }
	  }
	else 
	  { 
	     if( haybarco(x,y)==TRUE )  
	       {
		  if( (haybarco(x,y-1)==FALSE) && (haybarco(x-1,y)==FALSE) ) 
		    {
		       /* this is the first time i check this */
		       subarco=1;
		       barcos++;
		       xx=x;yy=y;
		       
		       for(;;) 
			 {
			    if( (haybarco(x-1,y+1)==TRUE) || (haybarco(x+1,y+1)==TRUE) ) 
			      {
				 wprintw(msgboard,"\nCheck board: Collision in (x,y)=%i,%c",x,'A'+y);
				 pmicell(x,y,RED); 
				 return FALSE;
			      }
			    if( (haybarco(x+1,y)==FALSE) && (haybarco(x,y+1)==FALSE) ) 
			      {
				 x=xx;y=yy;          /* restauro pos */
				 barquitos[subarco-1]++; /* update cantidad de tama;os de barcos */
				 break;
			      }
			    else if( haybarco(x+1,y) == TRUE ) 
			      {
				 subarco++;
				 barcos++;
				 x++;
			      }          
			    else if( haybarco(x,y+1) == TRUE ) 
			      {
				 y++;
				 barcos++;
				 subarco++;
			      }
			    if( subarco > 4) 
			      {
				 wprintw(msgboard,"\nCheck board: Ship longer than 4 units in (x,y)=%i,%c",x,'A'+y);
				 pmicell(x,y,RED); 
				 return FALSE;
			      }
			 } /* for(;;) */
		    } /* if(haybarco(x,y-1)==FALSE) */
	       } /* if(haybarco(x,y)==TRUE) */
	     if(nextbarco()==FALSE) 
	       {
		  if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) 
		    {
		       return TRUE;         /* tabla bien puesta */
		    }
		  else 
		    {
		       wprintw(msgboard,"\nCheck board: All numbers must be equal to 0. [4]=%i,[3]=%i,[2]=%i,[1]=%i",1-barquitos[3],2-barquitos[2],3-barquitos[1],4-barquitos[0]);
		       return FALSE;
		    }
	       } /* if nextbarco()==FALSE) */
	  } /* else */
     } /* for(;;) */
} /* void algoritomo() */


void aboutw( void ) 
{
   touchwin(about);
   wrefresh(about);
   touchwin(msgboard);
   globaltab();
}
void configw( void ) 
{
   char tempi[8];
   touchwin(config);
   mvwprintw(config,1,10,"                                               ");
   mvwprintw(config,2,10,"                                               ");
   mvwprintw(config,3,10,"                                               ");
   mvwprintw(config,1,10,"%s",usuario.server_name);
   mvwprintw(config,2,10,"%i",usuario.port);
   mvwprintw(config,3,10,"%s",usuario.nombre);
   strcpy(ncur.confc[0],usuario.server_name);
   strcpy(ncur.confc[2],usuario.nombre);
   sprintf(tempi,"%i",usuario.port);
   strcpy(ncur.confc[1],tempi);
   
   wmove(config,ncur.confpos+1,ncur.conf[ncur.confpos]+10);
   wrefresh(config);
   touchwin(msgboard);
}
void sendmsgw( void ) 
{
   int i;
   
   touchwin(sendmesg);
   for(i=0;i<72;i++) mvwaddch(sendmesg,1,8+i,' ');
   ncur.sendpos=0;
   ncur.sendx=0;
   ncur.sendmsg[0]=0;
   wmove(sendmesg,1,8+ncur.sendx);
   wrefresh(sendmesg);
   touchwin(msgboard);
}

void rconnect(void) 
{
   if(ncur.lugarc==0)
     wattroncm(bconnect,COLOR_PAIR(VGOOD));
   else wattroncm(bconnect,COLOR_PAIR(GOOD));
   switch(usuario.play) 
     {
      case 0:
	mvwprintw(bconnect,0,0,"  connEct ");
	break;
      default:
	mvwprintw(bconnect,0,0,"disconnEct");
	break;
     }
   wrefresh(bconnect);
}
void rstart(void) 
{
   if(ncur.lugarc==1)
     wattroncm(bstart,COLOR_PAIR(VGOOD));
   else wattroncm(bstart,COLOR_PAIR(GOOD));
   switch(usuario.play) 
     {
      case 0:
      case 1:
	mvwprintw(bstart,0,0,  "Send board");
	break;
      case 2:
      case 3:
      case 4:
      default:
	mvwprintw(bstart,0,0,  "   Start  ");
	break;
     }
   wrefresh(bstart);
}
void rstatus(void) 
{
   if(ncur.lugarc==2)
     wattroncm(bstatus,COLOR_PAIR(VGOOD));
   else 
     wattroncm(bstatus,COLOR_PAIR(GOOD));
   mvwprintw(bstatus,0,0,  "  statUs  ");
   wrefresh(bstatus);
}
void rmsg(void) 
{
   if(ncur.lugarc==3)
     wattroncm(bmsg,COLOR_PAIR(VGOOD));
   else 
     wattroncm(bmsg,COLOR_PAIR(GOOD));
   mvwprintw(bmsg,0,0,    " send Msg ");
   wrefresh(bmsg);
}
void rconfig(void) 
{
   if(ncur.lugarc==4)
     wattroncm(bconfig,COLOR_PAIR(VGOOD));
   else 
     wattroncm(bconfig,COLOR_PAIR(GOOD));
   mvwprintw(bconfig,0,0, "  Config  ");
   wrefresh(bconfig);
}
void rabout(void) 
{
   if(ncur.lugarc==5)
     wattroncm(babout,COLOR_PAIR(VGOOD));
   else 
     wattroncm(babout,COLOR_PAIR(GOOD));
   mvwprintw(babout,0,0,  "   abOut  ");
   wrefresh(babout);
}
void rquit(void) 
{
   if(ncur.lugarc==6)
     wattroncm(bquit,COLOR_PAIR(VGOOD));
   else 
     wattroncm(bquit,COLOR_PAIR(GOOD));
   mvwprintw(bquit,0,0,   "   Quit   ");
   wrefresh(bquit);
}


void init_screen(void)
{
   int i,x,y;
   
   if(ncur.color==TRUE) 
     {
	init_pair(NORMAL,COLOR_WHITE, COLOR_BLACK);   /* definicion de atributos */
	init_pair(VGOOD, COLOR_BLUE,  COLOR_CYAN);
	init_pair(GOOD,  COLOR_CYAN,  COLOR_BLUE);
	init_pair(WHITE, COLOR_WHITE, COLOR_BLUE);
	init_pair(GREEN, COLOR_GREEN, COLOR_WHITE);
	init_pair(RED,   COLOR_RED,   COLOR_WHITE);
	init_pair(BLACK, COLOR_BLACK, COLOR_WHITE);
	init_pair(BLUE,  COLOR_WHITE, COLOR_BLUE);
	init_pair(BLUCKY,COLOR_BLACK, COLOR_CYAN);
	init_pair(LUCKY, COLOR_CYAN,  COLOR_BLACK);
     }
   
   myname=newwin(1,24,5,5);
   miboard=newwin(12,24,6,5); /* creacion de ventanas */
   yourname=newwin(1,24,5,54);
   tuboard=newwin(12,24,6,54);
   title=newwin(4,80,0,0);
   
   bconnect=newwin(1,10,5,35);
   bstart=newwin(1,10,7,35);
   bstatus=newwin(1,10,9,35);
   bmsg=newwin(1,10,11,35);
   bconfig=newwin(1,10,13,35);
   babout=newwin(1,10,15,35);
   bquit=newwin(1,10,17,35);
		
   wright=newwin(1,40,23,40);
   wleft=newwin(1,40,23,0);
   msgboard=newwin(5,0,18,0);
   about=newwin(5,80,18,0);
   sendmesg=newwin(3,80,20,0);
   config=newwin(4,80,19,0);             /* todo -1 */
   
   wattroncm(myname,COLOR_PAIR(BLUCKY));
   wattroncm(yourname,COLOR_PAIR(BLUCKY));
   
   wattroncm(msgboard,COLOR_PAIR(LUCKY));
   wattroncm(title,COLOR_PAIR(VGOOD));
   wattroncm(wleft,COLOR_PAIR(BLUCKY));
   wattroncm(wright,COLOR_PAIR(BLUCKY));
   
   scrollok(msgboard, TRUE);
   /*   scrollok(myname, FALSE);
    scrollok(yourname, FALSE); */
   scrollok(wleft, FALSE);        /* can these help me  ?*/
   scrollok(wright, FALSE);
   
   wattroncm(tuboard,COLOR_PAIR(VGOOD));
   wattroncm(miboard,COLOR_PAIR(VGOOD));
   mvwprintw(miboard,0,0," 0 1 2 3 4 5 6 7 8 9 ");
   mvwprintw(tuboard,0,0," 0 1 2 3 4 5 6 7 8 9 ");
   for(y=1;y<11;y++) 
     {
	wattroncm(tuboard,COLOR_PAIR(VGOOD));
	wattroncm(miboard,COLOR_PAIR(VGOOD));
	mvwaddch(miboard,y,0,'A'+y-1);
	mvwaddch(tuboard,y,0,'A'+y-1);
	wattroncm(tuboard,COLOR_PAIR(GOOD));
	wattroncm(miboard,COLOR_PAIR(GOOD));
	for(x=1;x<21;) 
	  {
	     mvwaddch(miboard,y,x,' ');
	     mvwaddch(tuboard,y,x,' ');
	     x++;
	     mvwaddch(miboard,y,x,ACS_VLINE);
	     mvwaddch(tuboard,y,x,ACS_VLINE);
	     x++;
	  } 
     }
   
   for(i=0;i<80*5;i++) 
     waddch(msgboard,' ');
   wmove(msgboard,0,0);
   wprintw(msgboard,"\nBatalla Naval ncurses client v"BN_NVER". by Ricardo Quesada (c) 1996,97,98");
   wprintw(msgboard,"\nUse TAB,CURSOR LEFT & CURSOR RIGHT to change between windows");
   
   for(i=0;i<80*4;i++) 
     waddch(title,' ');
   mvwprintw(title,1,25,"Batalla Naval ncurses client v"BN_NVER);
   mvwprintw(title,2,26,"by Ricardo Quesada (c)1996,97,98");
   
   strcpy(ncur.minombre,"Your board");
   mname();
   strcpy(ncur.tunombre,"Enemy board");
   yname();
   
   foot_left("Batalla Naval"); 
   foot_right("(c) 1995,96,97,98 by Riq");
   rconnect();
   rstart();
   rstatus();
   rmsg();
   rconfig();
   rabout();
   rquit();
   
   wattroncm(about,COLOR_PAIR(VGOOD));
   mvwprintw(about,0,0,"    Batalla Naval (c) 1995,96,97,98 by Ricardo Quesada (rquesada@dc.uba.ar)     ");
   wattroncm(about,COLOR_PAIR(GOOD));
   mvwprintw(about,1,0,"Dedicado a: Sebastian Cativa Tolosa que algun dia me dijo que iba a hacer este  ");
   mvwprintw(about,2,0,"            cliente. Espero que la proxima termines lo que empezas :)           ");
   mvwprintw(about,3,0,"Agradezco a: Horacio Pe�a (horape@century.com.ar) por varios bugs resueltos     ");
   mvwprintw(about,4,0,"             y por su cliente Win16. -                                          ");
   
   wattroncm(sendmesg,COLOR_PAIR(VGOOD));
   mvwprintw(sendmesg,0,0,"                               Send Message v0.03                               ");
   wattroncm(sendmesg,COLOR_PAIR(GOOD));
   mvwprintw(sendmesg,1,0,"Message:                                                                        ");
   mvwprintw(sendmesg,2,0,"To player number (0=broadcast):0                  Local Echo:[ ]                ");
   
   wattroncm(config,COLOR_PAIR(VGOOD));
   mvwprintw(config,0,0,"                                The Config v0.01                                ");
   wattroncm(config,COLOR_PAIR(GOOD));
   mvwprintw(config,1,0,"Host name:                                                                      ");
   mvwprintw(config,2,0,"Host port:                                                                      ");
   mvwprintw(config,3,0,"Your name:                                                                      ");
   
   fillenemyt(Bboard,0);
   fillenemyt(Nboard,1);
   
   wrefresh(miboard); 
   wrefresh(tuboard);
   wrefresh(msgboard);
   wrefresh(title);
   wrefresh(bconnect);
   wrefresh(bstatus);
   wrefresh(bmsg);
   wrefresh(bstart);
   wrefresh(bconfig);
   wrefresh(babout);
   wrefresh(bquit);
}
/*
 * Proceso()
 */
void proceso( void ) 
{
   struct protocolo proto;
   char outbuf[MSGMAXLEN];
   char outbufi[MSGMAXLEN];
   int i;
   
   FD_SET(sock,&readfds);
   if(select(FD_SETSIZE,&readfds,NULL,NULL,&tout)) 
     {
	if( read(sock,&proto,MAXLEN) >0) 
	  {
	     ncur.flag1=1; 
	     switch(proto.bnptip0) 
	       {
		case BNTST:
		  break;
		case BNWRI:
		  if(proto.bnpmsg[0]==1) 
		    {
		       usuario.play=2;
		       rstart();
		       wprintw(msgboard,"\nThe board is OK. Press START to start the game.");
		    }
		  else if(proto.bnpmsg[0]==0) 
		    {
		       usuario.play=1;
		       wprintw(msgboard,"\nCheck board: bnserver reports an error.");
		    }
		  wrefresh(msgboard);
		  break;
		case BNJUG:
		  usuario.numjug=proto.bnptip1;
		  sprintf(outbuf,"Connected to bnserver:%s",proto.bnpmsg);
		  foot_right(outbuf);
		  sprintf(outbuf,"Player %i",usuario.numjug);
		  foot_left(outbuf);
		  wprintw(msgboard,"\nConnected to bnserver:%s. You are player number:%i.",proto.bnpmsg,usuario.numjug);
		  wrefresh(msgboard);
		  usuario.play = 1;
		  rconnect();
		  strcpy(ncur.tunombre,"Enemy board");
		  yname();
		  bnwrite(sock,outbuf,BNVER,usuario.usrfrom,bnsup,usuario.numjug);
		  break;
		case BNVER:
		  wprintw(msgboard,"\nbnserver version:%i.%i",proto.bnpmsg[0],proto.bnpmsg[1]);
		  wrefresh(msgboard);
		  break;
		case BNMSG:
		  wprintw(msgboard,"\n%s",proto.bnpmsg);
		  wrefresh(msgboard);
		  break;
		case BNALL:
		  wprintw(msgboard,"\nbnserver is full. Using all the connections.Try later");
		  wrefresh(msgboard);
		  close(sock);
		  break;
		case BNWAI:
		  wprintw(msgboard,"\nYou cant start a game while people are playing.Try later");
		  wrefresh(msgboard);
		  break;
		case BNHIT:
		  if(usuario.play>=3) 
		    {
		       mitabla[(int)proto.bnpmsg[0]][(int)proto.bnpmsg[1]]=(int)proto.bnpmsg[2]; /* new on v0.44 */
		       if(usuario.hide==FALSE)
			 repaintmi();
		    }
		  bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
		  break;
		case BNREA:
		  if(usuario.play>=3)
		    {
		       repainttu(proto.bnpmsg);
		       inteliclient(proto.bnpmsg);
		    }
		  break;
		case BNSTR:
		  foot_right("Please,wait your turn");
		  wprintw(msgboard,"\nUse PAGE UP or 'p' & PAGE DOWN or 'n' to switch between enemy boards");
		  wprintw(msgboard,"\nAt the bottom-right corner you can see if its your turn.");
		  sprintf(outbuf,"Available players:");
		  
		  for(i=0;i<MAXPLAYER;i++) 
		    {
		       if(proto.bnpmsg[i]>=2) 
			 {
			    sprintf(outbufi," %i",i+1);
			    strcat(outbuf,outbufi);
			    strcpy(usuario.names[i],&proto.bnpmsg[5+i*MAXNAMELEN]);
			 }
		    }
		  wprintw(msgboard,"\n%s",outbuf);
		  wrefresh(msgboard);
		  
		  foot_left(usuario.names[usuario.numjug-1]); 
		  
		  if(usuario.play==2) 
		    {
		       usuario.play=3;
		    }
		  for(i=0;i<MAXPLAYER;i++) 
		    {   
		       if(usuario.numjug-1!=i) 
			 {
			    if(strcmp(usuario.names[i],"")!=0) 
			      {
				 usuario.usrfrom=i+1;
				 strcpy(ncur.tunombre,usuario.names[i]); /* new on v0.41 */
				 yname();
			      }
			 }
		    }
		  break;
		case BNSTS:                    /* new on v0.43 */
		  for(i=0;i<MAXPLAYER;i++) 
		    {
		       wprintw(msgboard,"\nPlayer %i (%s), name='%s'",i+1,usuario.status[(int)proto.bnpmsg[i]],&proto.bnpmsg[5+i*MAXNAMELEN]);
		    }
		  wrefresh(msgboard);
		  break;
		case BNCON:
		  wprintw(msgboard,"\nPlayer %i is ready to play",proto.bnptip1);
		  wrefresh(msgboard);
		  break;
		case BNWHO:                      
		  sprintf(outbuf,"It's %s turn.",usuario.names[proto.bnptip1-1]);
		  foot_right(outbuf);
		  break;
		case BNTRN:
		  usuario.play=4;                    /* oh, it is my turn */
		  foot_right("It's my turn.");
		  break;
		case BNEXT:
		  i=proto.bnptip1-1;
		  usuario.names[i][0]=0;
		  strcpy(outbuf,proto.bnpmsg);
		  wprintw(msgboard,"\n%s",outbuf);
		  wrefresh(msgboard);
		  
		  for(i=0;i<MAXPLAYER;i++) 
		    {
		       if(usuario.numjug-1!=i) 
			 {
			    if(strcmp(usuario.names[i],"")!=0) 
			      {
				 usuario.usrfrom=i+1;
				 strcpy(ncur.tunombre,usuario.names[i]); /* new on v0.41 */
				 yname();
			      }
			 }
		    }
		  if(usuario.play>=3) 
		    {
		       bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
		    }
		  break;
		case BNLOS:
		  usuario.play=1;                    /* oh, i lost the game */
		  for(i=0;i<MAXPLAYER;i++)
		    usuario.names[i][0]=0;
		  wprintw(msgboard,"\nYou were killed. To start again wait until the game is over.");
		  wrefresh(msgboard);
		  fillenemyt(looseboard,1);
		  rstart();
		  usuario.firsthit=0;
		  break;
		case BNWIN:
		  usuario.play=1;                    /* oh, i lost the game */
		  for(i=0;i<MAXPLAYER;i++)
		    usuario.names[i][0]=0;
		  wprintw(msgboard,"\nCONGRATULATIONS: You are the winner.");
		  wrefresh(msgboard);
		  fillenemyt(winboard,1);
		  rstart();
		  usuario.firsthit=0;
		  break;
		case BNSOL:
		  wprintw(msgboard,"\nYou are the only one who sent the board.Wait another player.");
		  wrefresh(msgboard);
		  break;
		default:
		  wprintw(msgboard,"\nbnserver sending code %X.  Not available in this nbnclient.",proto.bnptip0);
		  break;
	       }
	  }
     }
}


void init_datos( void ) 
{
   int i,j;
   
   usuario.play = 0;
   usuario.usrfrom=1;
   usuario.lecho=1;
   for(i=0;i<10;i++) 
     {          /* clean tabla */
	for(j=0;j<10;j++)
	  mitabla[i][j]=0;
     } 
   usuario.firsthit=0;
   for(i=0;i<MAXPLAYER;i++)
     usuario.names[i][0]=0;       /* clear all names */
   
   ncur.lugarg=MITABLA;
   ncur.lugarc=1;
   ncur.quit=0;
   ncur.x=0;ncur.y=0;
   
   ncur.sendx=0;
   ncur.sendmsg[0]=0;
   ncur.sendto=0;
   ncur.sendle=0;
   
   ncur.conf[0]=strlen(usuario.server_name);
   ncur.conf[1]=4;
   ncur.conf[2]=strlen(usuario.nombre);
   ncur.confpos=0;
   
   ncur.flag1=0;
   
   strcpy(usuario.status[DISCON],"not connected");
   strcpy(usuario.status[CONNEC],"  connected  ");
   strcpy(usuario.status[BOARD], "ready to play");
   strcpy(usuario.status[PLAY],  "   playing   ");
   strcpy(usuario.status[TURN],  "  playing *  ");
   
   usuario.hide=FALSE;
}

int init_cliente( void ) 
{
   
   /* Create socket */
   sock=socket(AF_INET,SOCK_STREAM,0);
   if(sock <0) 
     {
	wprintw(msgboard,"\nERROR: creating stream socket");
	wrefresh(msgboard); 
	return(1);
     }
   host_info = gethostbyname(usuario.server_name);
   if(host_info==NULL) 
     {
	close(sock);
	wprintw(msgboard,"\nERROR: Unknown host.Please check CONFIG.");
	wrefresh(msgboard); 
	return(2);
     }
   server.sin_family=host_info->h_addrtype;
   memcpy( (char*) &server.sin_addr, host_info->h_addr,host_info->h_length);
   server.sin_port=htons(usuario.port);
   
   if(connect(sock,(struct sockaddr *)&server,sizeof(server))< 0) 
     {
	close(sock);
	wprintw(msgboard,"\nERROR: Perhaps incorrect port number or bnserver not running.Check CONFIG");
	wrefresh(msgboard);
	return 3;
     }
   strcpy(outbuftemp,usuario.nombre);
   bnwrite(sock,outbuftemp,BNJUG,NCLI,BN_NVERH,BN_NVERL);  /* WHO AM I */
   return 0;
}


/* 
 * main_loop
 */
void hideH() 
{
   switch(usuario.hide) 
     {
      case TRUE:
	usuario.hide=FALSE;
	repaintmi();
	break;
      case FALSE:
	usuario.hide=TRUE;
	fillenemyt(Hboard,0);
	break;
     }
}

void globalconfig(int i) 
{
   if(ncur.conf[ncur.confpos]>58)
     ncur.conf[ncur.confpos]=58;
   else 
     {
	mvwaddch(config,ncur.confpos+1,ncur.conf[ncur.confpos]+10,(chtype)i);
	ncur.confc[ncur.confpos][ncur.conf[ncur.confpos]]=(char)i;
	ncur.confc[ncur.confpos][ncur.conf[ncur.confpos]+1]=0;
	ncur.conf[ncur.confpos]++;
     }
   wmove(config,ncur.confpos+1,ncur.conf[ncur.confpos]+10);
   wrefresh(config);
}

void globalsendmsg(int i) 
{
   if(ncur.sendx>70)
     ncur.sendx=70;
   else 
     {
	mvwaddch(sendmesg,1,8+ncur.sendx,(chtype)i);
	ncur.sendmsg[ncur.sendx]=(char)i;
	ncur.sendmsg[ncur.sendx+1]=0;
	ncur.sendx++;
     }
   wmove(sendmesg,1,8+ncur.sendx);
   wrefresh(sendmesg);
}

void globalpageup(void) 
{
   int i,n;
   n=usuario.usrfrom-2;
   
   if(usuario.play>=3) 
     {
	for(i=n;i>=0;i--) 
	  {
	     if(strcmp(usuario.names[i],"")!=0) 
	       {
		  usuario.usrfrom=i+1;
		  strcpy(ncur.tunombre,usuario.names[i]); /* new on v0.41 */
		  yname();
		  break;
	       }
	  }
	if(i<0) 
	  {
	     for(i=MAXPLAYER-1;i>=0;i--) {
		if(strcmp(usuario.names[i],"")!=0) 
		  {
		     usuario.usrfrom=i+1;
		     strcpy(ncur.tunombre,usuario.names[i]); /* new on v0.41 */
		     yname();
		     break;
		  }
	     }
	  }
	bnwrite(sock,outbuftemp,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
     }    /* if(usuario.play) */
}
void globalpagedown(void) 
{
   int i,n;
   n=usuario.usrfrom;
   
   if(usuario.play>=3) 
     {
	for(i=n;i<MAXPLAYER;i++) 
	  {
	     if(strcmp(usuario.names[i],"")!=0) 
	       {
		  usuario.usrfrom=i+1;
		  strcpy(ncur.tunombre,usuario.names[i]); /* new on v0.41 */
		  yname();
		  break;
	       }
	  }
	if(i>=MAXPLAYER) 
	  {
	     for(i=0;i<MAXPLAYER;i++) 
	       {
		  if(strcmp(usuario.names[i],"")!=0) 
		    {
		       usuario.usrfrom=i+1;
		       strcpy(ncur.tunombre,usuario.names[i]); /* new on v0.41 */
		       yname();
		       break;
		    }
	       }
	  }
	bnwrite(sock,outbuftemp,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
     } /* if(usuario.play) */
}

void globaltab(void) 
{         /* llamada desde main_loop */
   switch(ncur.lugarc) 
     {
      case 0:
	wmove(bconnect,0,0);
	wrefresh(bconnect);
	break;
      case 1:
	wmove(bstart,0,0);
	wrefresh(bstart);
	break;
      case 2:
	wmove(bstatus,0,0);
	wrefresh(bstatus);
	break;
      case 3:
	wmove(bmsg,0,0);
	wrefresh(bmsg);
	break;
      case 4:
	wmove(bconfig,0,0);
	wrefresh(bconfig);
	break;
      case 5:
	wmove(babout,0,0);
	wrefresh(babout);
	break;
      case 6:
	wmove(bquit,0,0);
	wrefresh(bquit);
	break;
      default:
	break;
     }
}
void globalbotones(void) 
{         /* llamada desde main_loop */
   rconnect();
   rstart();
   rstatus();
   rmsg();
   rconfig();
   rabout();
   rquit();
   globaltab();                /* posciciona el cursor */
}
void globalbotones2(int i) 
{         /* llamada desde main_loop */
   switch(i) 
     {
      case 'e':case 'E':              /* connEct o disconnEct */
	ncur.lugarc=0;
	globalbotones();
	break;
      case 's':case 'S':              /* Send board o Start */
	ncur.lugarc=1;
	globalbotones();
	break;
      case 'u':case 'U':              /* statUs */
	ncur.lugarc=2;
	globalbotones();
	break;
      case 'm':case 'M':              /* send Msg */
	ncur.lugarc=3;
	globalbotones();
	break;
      case 'c':case 'C':              /* Config */
	ncur.lugarc=4;
	globalbotones();
	break;
      case 'o':case 'O':              /* abOut */
	ncur.lugarc=5;
	globalbotones();
	break;
      case 'q':case 'Q':              /* Quit */
	ncur.lugarc=6;
	globalbotones();
	break;
     }
}
void globalenter(void) 
{         /* llamada desde main_loop */
   int i;
   switch(ncur.lugarc) 
     {
      case 0: /* disconnect */
	if(usuario.play==0) 
	  {
	     init_cliente();
	     globaltab();
	  }
	else 
	  {
	     if(usuario.play!=0) 
	       {
		  usuario.firsthit=0;            /* new on v0.41 */
		  usuario.play=0;
		  foot_right("(c) Ricardo Quesada");
		  foot_left("Batalla Naval");
		  rstart();
		  rconnect();
		  for(i=0;i<MAXPLAYER;i++)
		    usuario.names[i][0]=0;
		  bnwrite(sock,outbuftemp,BNEXT,0,0,usuario.numjug);
		  close(sock);
	       }
	  }
	break;
	
      case 1: /* send board or start */
	switch(usuario.play) 
	  {
	   case 0:
	     filtermiboard();
	     repaintmi();
	     if(algoritmo()==TRUE)  
	       {
		  wprintw(msgboard,"\nFirst establish a connection with CONNECT");
	       }
	     wrefresh(msgboard);
	     break;
	   case 1:
	     filtermiboard();
	     repaintmi();
	     if( algoritmo()==TRUE)
	       {
		  iwtable( outbuftemp );
		  bnwrite(sock,outbuftemp,BNWRI,usuario.numjug,0,usuario.numjug);
	       } 
	     wrefresh(msgboard);
	     break;
	   case 2:
	     bnwrite(sock,outbuftemp,BNSTR,usuario.numjug,0,usuario.numjug);
	     break;
	   case 4:
	   case 3:
	   default:
	     wprintw(msgboard,"\nQue muchacho?.This mean that you are playing!");
	     wrefresh(msgboard);
	     break;
	  }
	break;
      case 2: /* status */
	if(usuario.play>=1) 
	  {
	     bnwrite(sock,outbuftemp,BNSTS,0,0,usuario.numjug);
	  }
	else 
	  {
	     wprintw(msgboard,"\nYou need to be connected");
	     wrefresh(msgboard);
	  }
	break;
      case 3: /* send message */
	if(usuario.play>=1) 
	  {
	     ncur.lugarg=SENDMSG;
	     sendmsgw();
	  }
	else {
	   wprintw(msgboard,"\nFirst establish a connection with CONNECT");
	   wrefresh(msgboard);
	}
	break;
      case 4: /* config */
	ncur.lugarg=THECONF;
	configw();
	break;
      case 5: /* about */
	aboutw();
	break;
      case 6: /* quit */
	if(usuario.play!=0)
	  {             /* if connected then kill child */
	     bnwrite(sock,outbuftemp,BNEXT,0,0,usuario.numjug);
	     close(sock);
	  }
	ncur.quit=1;
	break;
      default:
	break;
     }
}

/*
 * Funcion main_loop
 */
void main_loop(void)
{
   int i,j;
   
   switch(ncur.lugarg) 
     {
      case MITABLA:                         /* main switch */
	wmove(miboard,ncur.y+1,ncur.x*2+1);
	wrefresh(miboard);
	
	switch(i=getch())
	  {
	   case ERR:
	     break;
	   case 'h': case 'H':              /* new on v0.44 */
	     hideH();
	     break;
	   case KEY_LEFT: case 'j':        /* new on v0.41 */
	     ncur.x--;
	     if(ncur.x<0) ncur.x=9;
	     break;
	   case KEY_RIGHT: case 'l':        /* new on v0.41 */
	     ncur.x++;
	     if(ncur.x>9)ncur.x=0;
	     break;
	   case KEY_UP: case 'i':        /* new on v0.41 */
	     ncur.y--;
	     if(ncur.y<0)ncur.y=9;
	     break;
	   case KEY_DOWN: case 'k':        /* new on v0.41 */
	     ncur.y++;
	     if(ncur.y>9)ncur.y=0;
	     break;
	     /*       case KEY_SPACE: */
	   case ' ': case 'x':        /* new on v0.41 */
	     if((usuario.firsthit==0) && (usuario.play<2))  
	       {
		  fill0(WHITE,1);
		  for(i=0;i<10;i++) {          /* clean tabla */
		     for(j=0;j<10;j++)
		       mitabla[i][j]=NOBARCO;
		  }
		  repaintmi(); 
		  usuario.firsthit=1;
		  wmove(miboard,ncur.y+1,ncur.x*2+1);
		  wrefresh(miboard);
	       }
	     switch(usuario.play) 
	       {
		case 0:
		case 1:
		  if(mitabla[ncur.x][ncur.y]==NOBARCO) 
		    {
		       mitabla[ncur.x][ncur.y]=BARCO;
		       wattroncm(miboard,COLOR_PAIR(GREEN));
		       waddchcm(miboard,ACS_BOARD,COLOR_PAIR(GREEN));
		    }
		  else 
		    {
		       mitabla[ncur.x][ncur.y]=NOBARCO;
		       wattroncm(miboard,COLOR_PAIR(GOOD));
		       waddch(miboard,' ');
		       waddch(miboard,ACS_VLINE);
		    }
		  break;
		case 2:
		  wprintw(msgboard,"\nThe board is OK and you cant modify it now.");
		  wrefresh(msgboard);
		  break;
		default:
		  wprintw(msgboard,"\nYou cant modify board while playing!");
		  wrefresh(msgboard);
		  break;
	       }
	     break;
	   case 9:
	   case 't':
	     ncur.lugarg=BOTONES;
	     yname();
	     mname();
	     globaltab();
	     break;
	   case 10:
	   case 13:
	   case KEY_ENTER:
	     globalenter();
	     break;
	   case KEY_NPAGE:
	   case 'n':
	     globalpagedown();
	     break;
	   case KEY_PPAGE:
	   case 'p':
	     globalpageup();
	     break;
	   default:
	     /*
	      * wmove(miboard,ncur.y+1,ncur.x*2+1);
	      * wrefresh(miboard);      
	      */
	     globalbotones2(i);
	     break;
	  }		/* switch(getch()) */
	
	
	break;                         /* se acabo el case MITABLA */
	
      case BOTONES:                    /* main switch */
	switch(i=getch())  
	  {
	   case ERR:
	     break;
	   case KEY_UP: case 'i':        /* new on v0.41 */
	     ncur.lugarc--;
	     if(ncur.lugarc<0) ncur.lugarc=MAXLUGARC;
	     globalbotones();
	     break;
	   case KEY_DOWN: case 'k':        /* new on v0.41 */
	     ncur.lugarc++;
	     if(ncur.lugarc>MAXLUGARC)ncur.lugarc=0;
	     globalbotones();
	     break;
	   case 9: case 't':
	     if(usuario.play>=3)
	       ncur.lugarg=TUTABLA;
	     else ncur.lugarg=MITABLA;
	     yname();
	     mname();
	     break;
	   case KEY_LEFT: case 'j':        /* new on v0.41 */
	     ncur.lugarg=MITABLA;
	     yname();
	     mname();
	     break;
	   case KEY_RIGHT: case 'l':        /* new on v0.41 */
	     ncur.lugarg=TUTABLA;
	     yname();
	     mname();
	     break;
	   case 10:
	   case 13:
	   case KEY_ENTER:
	     globalenter();
	     break;
	   case KEY_NPAGE:
	   case 'n':
	     globalpagedown();
	     break;
	   case KEY_PPAGE:
	   case 'p':
	     globalpageup();
	     break;
	   case 'h': case 'H':              /* new on v0.46 */
	     hideH();
	     break;
	   default:
	     globalbotones2(i);              /* que sera entonces ? new on v0.46 */
	     break;
	  }
	break;
	
      case TUTABLA:             /* main switch */
	wmove(tuboard,ncur.yt+1,ncur.xt*2+1);
	wrefresh(tuboard);
	switch(i=getch())  
	  {
	   case ERR:
	     break;
	   case KEY_LEFT: case 'j':        /* new on v0.41 */
	     ncur.xt--;
	     if(ncur.xt<0) ncur.xt=9;
	     break;
	   case KEY_RIGHT: case 'l':        /* new on v0.41 */
	     ncur.xt++;
	     if(ncur.xt>9)ncur.xt=0;
	     break;
	   case KEY_UP: case 'i':        /* new on v0.41 */
	     ncur.yt--;
	     if(ncur.yt<0)ncur.yt=9;
	     break;
	   case KEY_DOWN: case 'k':        /* new on v0.41 */
	     ncur.yt++;
	     if(ncur.yt>9)ncur.yt=0;
	     break;
	     /*       case KEY_SPACE: */
	   case ' ': case 'x':        /* new on v0.41 */
	     if((usuario.firsthit==0) && (usuario.play<2))  
	       {
		  fill0(WHITE,1);
		  for(i=0;i<10;i++) 
		    {          /* clean tabla */
		       for(j=0;j<10;j++)
			 mitabla[i][j]=NOBARCO;
		    }
		  repaintmi(); 
		  usuario.firsthit=1;
	       }
	     switch(usuario.play) 
	       {
		case 0:
		case 1:
		case 2:
		  wprintw(msgboard,"\nYou cant HIT now. First start a game.");
		  wrefresh(msgboard);
		  break;
		case 3:
		  wprintw(msgboard,"\nIt is not your turn!");
		  wrefresh(msgboard);
		  break;
		default:
		  usuario.play=3;                        /* it is no more your turn */
		  outbuftemp[0]=ncur.xt;
		  outbuftemp[1]=ncur.yt;
		  bnwrite(sock,outbuftemp,BNHIT,0,0,usuario.numjug);
		  bnwrite(sock,outbuftemp,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
		  break;
	       }
	     break;
	   case 9:
	   case 't':
	     ncur.lugarg=BOTONES;
	     yname();
	     mname();
	     globaltab();
	     break;
	   case 10:
	   case 13:
	   case KEY_ENTER:
	     globalenter();
	     break;
	   case KEY_NPAGE:
	   case 'n':
	     globalpagedown();
	     break;
	   case KEY_PPAGE:
	   case 'p':
	     globalpageup();
	     break;
	   case 'h': case 'H':              /* new on v0.46 */
	     hideH();
	     break;
	   default:
	     globalbotones2(i);
	     break;
	     wmove(tuboard,ncur.yt+1,ncur.xt*2+1);
	     wrefresh(tuboard);
	  }  /* switch(getch()) */
	break;                         /* se acabo el case MITABLA */
	
      case SENDMSG:
	if(ncur.flag1!=0) 
	  {
	     touchwin(sendmesg);
	     wrefresh(sendmesg);
	     ncur.flag1=0;
	  }
	switch(i=getch()) 
	  {
	   case '0':
	   case '1':
	   case '2':
	   case '3':
	   case '4':
	   case '5':
	     switch(ncur.sendpos) 
	       {
		case 0:
		  globalsendmsg(i);
		  break;
		case 1:
		  ncur.sendto=atoi((char*)&i);
		  mvwaddch(sendmesg,2,31,i);
		  wmove(sendmesg,2,31);
		  wrefresh(sendmesg);
		  break;
	       }
	     break;
	   case KEY_DOWN:
	   case KEY_UP:
	   case 9:
	     ncur.sendpos++;
	     if(ncur.sendpos>2) ncur.sendpos=0;
	     switch(ncur.sendpos) 
	       {
		case 0:
		  wmove(sendmesg,1,8+ncur.sendx);
		  wrefresh(sendmesg);
		  break;
		case 1:
		  wmove(sendmesg,2,31);
		  wrefresh(sendmesg);
		  break;
		case 2:
		  wmove(sendmesg,2,62);
		  wrefresh(sendmesg);
		  break;
	       }
	     break;
	   case 'x':
	   case 'X':
	   case ' ':
	     switch(ncur.sendpos) 
	       {
		case 0:
		  globalsendmsg(i);
		  break;
		case 1:
		  break;
		case 2:
		  if(ncur.sendle==0) 
		    {
		       ncur.sendle=1;
		       mvwaddch(sendmesg,2,62,'X');
		    }
		  else 
		    {
		       ncur.sendle=0;
		       mvwaddch(sendmesg,2,62,' ');
		    }
		  wmove(sendmesg,2,62);
		  wrefresh(sendmesg);
		  break;
	       }
	     break;
	   case 10:
	   case 13:
	   case KEY_ENTER:
	     bnwrite(sock,ncur.sendmsg,BNMSG,ncur.sendto,ncur.sendle,usuario.numjug);
	     touchwin(msgboard);
	     wrefresh(msgboard);
	     ncur.lugarg=BOTONES;
	     mname();
	     yname();
	     globaltab();
	     break;
	   case KEY_LEFT:
	   case KEY_RIGHT:
	   case ERR:
	     break;
	   case KEY_BACKSPACE: case '/':
	     if(ncur.sendpos==0) 
	       {
		  ncur.sendx--;
		  if(ncur.sendx<0) ncur.sendx=0;
		  ncur.sendmsg[ncur.sendx]=' ';
		  ncur.sendmsg[ncur.sendx+1]=0;
		  mvwaddch(sendmesg,1,8+ncur.sendx,' ');
		  wmove(sendmesg,1,8+ncur.sendx);
		  wrefresh(sendmesg);
	       }
	     break;
	   default:
	     if(ncur.sendpos==0)
	       globalsendmsg(i);
	     else if(ncur.sendpos==1) 
	       {
		  wmove(sendmesg,2,31);
		  wrefresh(sendmesg);
	       }
	     else if(ncur.sendpos==2) 
	       {
		  wmove(sendmesg,2,62);
		  wrefresh(sendmesg);
	       }
	     break;
	  }                     /* switch getch */
	break;
      case THECONF:
	if(ncur.flag1!=0) 
	  {
	     touchwin(config);
	     wrefresh(config);
	     ncur.flag1=0;
	  }
	switch(i=getch()) 
	  {
	   case KEY_UP:
	   case KEY_DOWN:
	   case 9:
	     ncur.confpos++;
	     if(ncur.confpos>2) 
	       ncur.confpos=0;
	     wmove(config,ncur.confpos+1,ncur.conf[ncur.confpos]+10);
	     wrefresh(config);
	     break;
	   case KEY_ENTER:
	   case 10:
	   case 13:
	     strcpy(usuario.server_name,ncur.confc[0]);
	     strcpy(usuario.nombre,ncur.confc[2]);
	     usuario.port=atoi(ncur.confc[1]);
	     
	     ncur.lugarg=BOTONES;
	     yname();
	     mname();
	     globaltab();
	     touchwin(msgboard);
	     wrefresh(msgboard);
	     break;
	   case KEY_BACKSPACE: case '/':
	     ncur.conf[ncur.confpos]--;
	     if(ncur.conf[ncur.confpos]<0)
	       ncur.conf[ncur.confpos]=0;
	     mvwaddch(config,ncur.confpos+1,ncur.conf[ncur.confpos]+10,' ');
	     ncur.confc[ncur.confpos][ncur.conf[ncur.confpos]]=' ';
	     ncur.confc[ncur.confpos][ncur.conf[ncur.confpos]+1]=0;
	     
	     wmove(config,ncur.confpos+1,ncur.conf[ncur.confpos]+10);
	     wrefresh(config);
	     break;
	   case KEY_LEFT:
	   case KEY_RIGHT:
	   case ERR:
	     break;
	   default:
	     globalconfig(i);
	     wmove(config,ncur.confpos+1,ncur.conf[ncur.confpos]+10);
	     wrefresh(config);
	     break;
	  }
	break;
      default:                  /* main switch */
	break;
     }                          /* switch(ncur.lugarg) */
}
/*
 * MAIN
 */
void help() 
{
   printf("\n\nBatalla Naval ncurses client v"BN_NVER"  (c) 1996,97 Ricardo Quesada\n");
   printf("\nsyntax:");
   printf("\nnbnclient [-s hostname] [-p server_port] [-u username]");
   printf("\n");
   printf("\n\t-s host_name:   The name of the host where bnserver is running");
   printf("\n\t                default is: localhost");
   printf("\n\t-p server_port: The number of the port that bnserver is listening to");
   printf("\n\t                default is: 1995");
   printf("\n\t-u username:    Your name or alias");
   printf("\n\t                default is your login name");
   printf("\n\t-c              force color mode");
   printf("\n\t-m              force black & white mode");
   printf("\n\t                VT100 terminal emulation recomended.");
   printf("\n");
   printf("\nKeys: Page up or 'p' shows previous players board");
   printf("\n      Page down or 'n' shows next players board");
   printf("\n      Use TAB,cursor left,cursor right to move between windows");
   printf("\n");
   printf("\nThe program automatically detects if you have a color terminal. ");
   printf("\nSend bugs,doubts,fixes to rquesada@dc.uba.ar\n");
   exit(-1);
}

void main(int argc, char ** argv)
{
   int i;
   
   ncur.color=0;
   strcpy( usuario.server_name,"localhost");
   usuario.port=1995;     
   
   /* 
    * New on v0.50 - modificado ya que la glibc tiene un comportamiento
    * extra�o con getlogin (me devuelve null )
    * Dicen que es por compatibilidad con ipv6
    */
   
   /*
		 * Usar en System-V
    */
   strncpy( usuario.nombre,getenv("LOGNAME"), MAXNAMELEN );
   
   /*
    * Usar en BSD
    */
   
   /* strncpy( usuario.nombre,getenv("USER"), MAXNAMELEN );  */
   
   
   for(i=1;i<argc;i++) 
     {
	if(argv[i][0]=='-') 
	  {
	     switch(argv[i][1]) 
	       {
		case 'M': case 'm': 
		  ncur.color=FALSE+128;
		  printf("\nForcing black & white mode...");
		  break;
		case 'C': case 'c': 
		  ncur.color=TRUE+128; 
		  printf("\nForcing color mode...");
		  break;
		case 'x':
		  bnsup=argv[i][2];
		  break;
		case 's': case 'S':
		  if(argc>i+1) 
		    strcpy( usuario.server_name,argv[i+1]);
		  else 
		    help();
		  i++;
		  break;
		case 'u': case 'U':
		  if(argc>i+1) 
		    strcpy( usuario.nombre,argv[i+1]);
		  else 
		    help();
		  i++;
		  break;
		case 'p': case 'P':
		  if(argc>i+1) 
		    usuario.port=atoi(argv[i+1]);
		  else 
		    help();
		  i++;
		  break;
		case 'h': case 'H':
		case '?': case '-':
		default:
		  help();
		  break;
	       }
	  }
	else 
	  {
	     if(argv[i-1][0]!='-')
	       help();
	  }
     }
   
   /*   use_env(TRUE);   */           /* new on v0.41*/
   initscr();
   keypad(stdscr, TRUE);
   raw();
   noecho();
   /*   scrollok(stdscr, TRUE); */
   timeout(RAPID);
   
   
   if(ncur.color>120) 
     ncur.color-=128;
   else 
     {    
	if(!has_colors()) 
	  {
	     printf("Warning: Color not detected.Black & white mode activated\n\r"
		    "         Remember, you need at least 80x24 terminal\n\r"
		    "         You can force COLOR mode with the -c switch\r\n"
		    );
	     ncur.color=FALSE;
	     sleep(2);
	  }
	else ncur.color=TRUE;
     }
   
   if(ncur.color==TRUE) 
     start_color();
   init_datos();          /* inicializar datos y variables */
   
   /*
    * Tuve que poner este `getch' aca para que no me borre la pantalla
    * Bug en libcurses-3.4 ?
    */
   getch();
   
   init_screen();         /* create and draw screen */
   init_cliente();        /* inicializa el cliente */
   for(;;)
     {
	if(ncur.quit!=0)
	  break;
	main_loop();           /* main loop */
	proceso();
     } 
   
   endwin();
}
